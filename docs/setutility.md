# Set Utility

A collection of methods for creating and manipulating Sets in Apex.

* [Data Factory](datafactory.md)
* [Date Utility](dateutility.md)
* [Integer Utility](integerutility.md)
* [List Utility](listutility.md)
* [Security Utility](securityutility.md)
* Set Utility
* [SObject Utility](sobjectutility.md)

[Back to readme](../README.md)

## Methods

### `difference(Set<Object> setA, Set<Object> setB)`

Returns a new Set containing the values from the first set (setA) but not the second set (setB).

```apex
Set<Object> setA = new Set<Object> { 'a', 'b', 'c' };
Set<Object> setB = new Set<Object> { 'c', 'd', 'e' };
Set<Object> diff = SetUtility.difference(setA, setB);
System.debug(diff);    // {a, b}
```

### `intersection(Set<Object> setA, Set<Object> setB)`

Returns a new Set containing the values that exist in both the first set (setA) and the second set (setB).

```apex
Set<Object> setA = new Set<Object> { 'a', 'b', 'c' };
Set<Object> setB = new Set<Object> { 'c', 'd', 'e' };
Set<Object> intersect = SetUtility.intersection(setA, setB);
System.debug(intersect);    // {c}
```

### `isDisjoint(Set<Object> setA, Set<Object> setB)`

Determines if the two sets have no elements in common.

```apex
Set<Object> setA = new Set<Object> { 'a', 'b', 'c' };
Set<Object> setB = new Set<Object> { 'c', 'd', 'e' };
Set<Object> setC = new Set<Object> { 1, 2, 3 };
Boolean isDisjoint = SetUtility.isDisjoint(setA, setB);
System.debug(isDisjoint);    // false

isDisjoint = SetUtility.isDisjoint(setA, setC);
System.debug(isDisjoint);    // true
```

### `isSuperset(Set<Object> setA, Set<Object> setB)`

Determines if the first Set (setA) is a superset (contains all the values of) the second set (setB).

```apex
Set<Object> setA = new Set<Object> { 'a', 'b', 'c', 'd', 'e' };
Set<Object> setB = new Set<Object> { 'd', 'e' };
Set<Object> setC = new Set<Object> { 1, 2 };
Boolean isSuperset = SetUtility.isSuperset(setA, setB);
System.debug(isSuperset);    // true

isSuperset = SetUtility.isSuperset(setA, setC);
System.debug(isSuperset);    // false
```

### `random(Set<Object> objSet)`

Randomly selects and returns an element from the provided Set.

```apex
Set<Object> setA = new Set<Object> { 'a', 'b', 'c', 'd', 'e' };
Object element = SetUtility.random(setA);
System.debug(element);    // d
```

### `random(Set<Object> objSet, Integer num)`

Returns a new Set populated with the specified number of elements randomly-selected from the provided Set.

```apex
Set<Object> setA = new Set<Object> { 'a', 'b', 'c', 'd', 'e' };
Object element = SetUtility.random(setA, 2);
System.debug(element);    // {c, e}
```

### `symmetricDifference(Set<Object> setA, Set<Object> setB)`

Returns a new Set containing the values contained in the first set (setA) and the second set (setB) but not both.

```apex
Set<Object> setA = new Set<Object> { 'a', 'b', 'c' };
Set<Object> setB = new Set<Object> { 'c', 'd', 'e' };
Set<Object> symDiff = SetUtility.symmetricDifference(setA, setB);
System.debug(symDiff);    // {a, b, d, e}
```

### `union(Set<Object> setA, Set<Object> setB)`

Returns a Set containing all the values from the two sets.

```apex
Set<Object> setA = new Set<Object> { 'a', 'b', 'c' };
Set<Object> setB = new Set<Object> { 'c', 'd', 'e' };
Set<Object> union = SetUtility.union(setA, setB);
System.debug(union);    // {a, b, c, d, e}
```

[Back to readme](../README.md)
