# Integer Utility

A collection of helpful utilities for manipulating and generating Integers in Apex.

* [Data Factory](datafactory.md)
* [Date Utility](dateutility.md)
* Integer Utility
* [List Utility](listutility.md)
* [Security Utility](securityutility.md)
* [Set Utility](setutility.md)
* [SObject Utility](sobjectutility.md)

[Back to readme](../README.md)

## Integer Utility

A collection of methods for Apex Integer manipulation.

### `random()`

Generates a random positive or negative integer.

```apex
Integer[] values = new Integer[] {};
for (Integer i = 0; i < 10; i++) {
    values.add(IntegerUtility.random());
}
System.debug(JSON.serialize(values));
```

Output

```json
[
    -2008534184,
    -202398123,
    1899255236,
    1897009214,
    -551660373,
    23918202,
    -472191124,
    380110378,
    1739072955,
    410350032
]
```

### `random(Integer len)`

Generates a random integer of the specified length.

```apex
Integer value = IntegerUtility.random(1);
System.debug(value);    // 7

value = IntegerUtility.random(2);
System.debug(value);    // 82
```

### `random(Boolean negative)`

Generates a random negative or positive integer.

```apex
Integer value = IntegerUtility.random(false);
System.debug(value);    // 149339668

value = IntegerUtility.random(true);
System.debug(value);    // -1692219848
```

### `random(Integer len, Boolean negative)`

Generates a random negative or positive integer.

```apex
Integer value = IntegerUtility.random(2, false);
System.debug(value);    // 20

value = IntegerUtility.random(2, true);
System.debug(value);    // -74
```

### `random(Integer low, Integer high)`

Selects an integer between the two end points, inclusive.

```apex
Integer value = IntegerUtility.random(0, 9);
System.debug(value);    // 4

value = IntegerUtility.random(100, 200);
System.debug(value);    // 143
```

[Back to readme](../README.md)