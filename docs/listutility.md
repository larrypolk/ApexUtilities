# ListUtility

A collection of methods for manipulating and creating Lists in Apex.

* [Data Factory](datafactory.md)
* [Date Utility](dateutility.md)
* [Integer Utility](integerutility.md)
* List Utility
* [Security Utility](securityutility.md)
* [Set Utility](setutility.md)
* [SObject Utility](sobjectutility.md)

[Back to readme](../README.md)

## Methods

### `average(List<Object> collection)`

Calculates the average of the values in the provided List. The List must be of type Decimal, Double, Integer, or Long.

```apex
Integer[] integers = new Integer[] { 1, 2, 3 };
Double actual = (Double) ListUtility.average(integers);
System.debug(actual);    // 2.0
```

### `chunk(List<Object> items, Integer size)`

Creates a List of Lists from the provided List. The length of the interior Lists is determined by the size argument.

```apex
List<Object> items = new List<Object> { 'a', 'b', 'c', 'd' };
List<List<Object>> chunks = ListUtility.chunk(items, 2);
System.debug(chunks);    // [['a', 'b'], ['c', 'd']]
```

### `compact(List<Object> items)`

Creates a List containing the "truthy" values from the provided List argument.

```apex
List<Object> items = new List<Object> { 'a', 'b', null, 'c', 'd', 0, false };
List<Object> compacted = ListUtility.compact(items);
System.debug(compacted);    // ['a', 'b', 'c', 'd']
```

### `concat(List<List<Object>> lols)`

Combines the elements from the provided List of Lists argument into a single List.

```apex
List<List<Object>> lols = new List<List<Object>> { 
    new List<Object> { 'a', 'b' },
    new List<Object> { 1, 2 },
    new List<Object> { 'c', 'd' },
    new List<Object> { 3, 4 }
};
List<Object> concat = ListUtility.concat(lols);
System.debug(concat);    // [a, b, 1, 2, c, d, 3, 4]
```

### `difference(List<Object> primary, List<Object> secondary)`

Returns a new List containing the elements contained from the first List (primary) and not the second List (secondary).

```apex
List<Object> primary = new List<Object> { 'a', 'b', 'c', 'd' };
List<Object> secondary = new List<Object> { 'c', 'd', 'd', 'e' };
List<Object> diff = ListUtility.difference(primary, secondary);
System.debug(diff);    // ['a', 'b', 'd', 'e']
```

### `collectExternalIds(SObject[] objects)`

Returns a new List containing the values contained in any external ID Fields on the object.

```apex
List<Test_Object__c> objects = [SELECT Id, Name, External_ID__c FROM Test_Object__c];
List<String> externalIds = ListUtility.collectExternalIds(objects);
System.debug(externalIds);    // ['external ID 1', 'external ID 2', 'external ID 3', ...]
```

### `collectExternalIds(SObject[] objects, SObjectField token)`

Returns a new List containing the values contained in the specified Field on the object.

```apex
List<Test_Object__c> objects = [SELECT Id, Name, External_ID__c FROM Test_Object__c];
List<String> externalIds = ListUtility.collectExternalIds(objects, Test_Object__c.External_ID__c);
System.debug(externalIds);    // ['external ID 1', 'external ID 2', 'external ID 3', ...]
```

### `filter(SObject[] objects, SObjectField token, Object value)`

Returns a List of SObjects in the provided List argument where the specified field contains the value.

```apex
SObjectField field = Test_Object__c.External_ID__c;
Object value = null;
List<SObject> objects = [SELECT Id, Name, External_ID__c FROM Test_Object__c];
List<SObject> matches = ListUtility.filter(objects, field, value);
System.debug(matches);    // [TestObject1, TestObject3, TestObject21, ...]
```

### `filter(SObject[] objects, Map<SObjectField, Object> fieldsAndValues)`

Returns a List of SObjects in the provided List argument where **all** the specified fields contains its associated value.

```apex
Map<SObjectField, Object> fieldsAndValues = new Map<SObjectField, Object> { 
    Test_Object__c.Custom_Field__c => null,
    Test_Object__c.External_ID__c => 1234
};
List<SObject> objects = [SELECT Id, Name, Custom_Field__c, External_ID__c FROM Test_Object__c];
List<SObject> matches = ListUtility.filter(objects, fieldsAndValues);
System.debug(matches);    // [TestObject1, TestObject3, TestObject21, ...]
```

### `random(Object[] objects)`

Randomly selects and returns an element from the provided List.

```apex
List<Object> values = new List<Object> { 'a', 'b', 'c', 'd', 'e' };
Object element = ListUtility.random(values);
System.debug(element);    // d
```

### `random(Object[] objects, Integer num)`

Returns a new Set populated with the specified number of elements randomly-selected from the provided Set.

```apex
List<Object> values = new List<Object> { 'a', 'b', 'c', 'd', 'e' };
Object element = ListUtility.random(values, 2);
System.debug(element);    // {b, c}
```

### `unique(Object[] objects)`

Selects the unique items in the provided List. The resulting List maintains the order of the original List.

```apex
Integer[] integers = new Integer[] { 1, 1, 1, 2, 3, 3 };
Object[] values = ListUtility.unique(integers);
System.debug(values);    // (1, 2, 3)
```

### `zip(Object[] listA, Object[] listB)`

```apex
String[] strings = new String[] { 'a', 'b' };
Integer[] integers = new Integer[] { 100, 200 };
List<List<Object>> values = new List<List<Object>>();

values.add(ListUtility.zip(strings, integers));

integers.clear();
integers.add(100);

values.add(ListUtility.zip(strings, integers));
System.debug(JSON.serialize(values));
```

Output

```json
[
    [
        ["a", 100],
        ["b", 200]
    ],
    [
        ["a", 100],
        ["b", null]
    ]
]
```

### `isTruthy(Object value)`

Determines if the provided value is "truthy". A value is not "truthy" if it is null, false, 0, 0.0, or an empty string. Anything else is "truthy".

```apex
Boolean isTruthy = ListUtility.isTruthy(1.0);
System.debug(isTruthy);    // true

isTruthy = ListUtility.isTruthy(0.0);
System.debug(isTruthy);    // false
```

[Back to readme](../README.md)