# SObject Utility

A collection of methods for manipulating and analyzing SObjects in Apex.

* [Data Factory](datafactory.md)
* [Date Utility](dateutility.md)
* [Integer Utility](integerutility.md)
* [List Utility](listutility.md)
* [Security Utility](securityutility.md)
* [Set Utility](setutility.md)
* SObject Utility

[Back to readme](../README.md)

## Methods

### `getFieldTokensForObject(SObjectType objType, Boolean enforceCRUDFLS)`

Returns a List of Field Tokens for the supplied SObject Type.

```apex
SObjectField[] fields = SObjectUtility.getFieldTokensForObject(Account.SObjectType, false);
System.debug(fields);
// (Id, IsDeleted, MasterRecordId, Name, Type, ParentId, BillingStreet, 
//  BillingCity, BillingState, BillingPostalCode, ...)
```

### `getFieldAPINamesForObject(SObjectType objType, Boolean enforceCRUDFLS)`

Returns a List of strings representing the API Names of the fields for the supplied SObject Type.

```apex
String[] fieldNames = SObjectUtility.getFieldAPINamesForObject(Account.SObjectType, false);
System.debug(fieldNames);
// (Id, IsDeleted, MasterRecordId, Name, Type, ParentId, BillingStreet, 
//  BillingCity, BillingState, BillingPostalCode, ...)
```

### `getAllOrgNamespaces()`

Returns a List of strings representing the Namespaces for the org, as well as any packages installed in the org.

```apex
String[] namespaces = SObjectUtility.getAllOrgNamespaces();
System.debug(namespaces);    // ['namespace_1', 'namespace_2', ...]
```

### `getFieldTokensForFieldSet(SObjectType type, FieldSet fs)`

Returns a Set of Field Tokens for the provided FieldSet.

```apex
Map<String, FieldSet> fieldsets = Account.SObjectType.getDescribe().fieldSets.getMap();
FieldSet fs = fieldSets.values()[0];
Set<SObjectField> tokens = SObjectUtility.getFieldTokensForFieldSet(Account.SObjectType, fs);
System.debug(tokens);
```

### `isFieldOnObject(SObjectType type, SObjectField token)`

Determines if the specified Field is contained on the SObject type.

```apex
Boolean isOnObject = SObjectUtility.isFieldOnObject(Account.SObjectType, Account.Name);
System.debug(isOnObject);    // true
```

### `isValidObjectType(SObjectType objType)`

Determines if the provided SObjectType is valid in the org.

```apex
Boolean isValid = SObjectUtility.isValidObjectType(Account.SObjectType);
System.debug(isValid);    // true

isValid = SObjectUtility.isValidObjectType(null);
System.debug(isValid);    // false
```

[Back to readme](../README.md)
