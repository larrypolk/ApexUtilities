# Data Factory

A utility class to create random and dummy data, including random hexadecimal color strings, strings, and more. 

* Data Factory
* [Date Utility](dateutility.md)
* [Integer Utility](integerutility.md)
* [List Utility](listutility.md)
* [Security Utility](securityutility.md)
* [Set Utility](setutility.md)
* [SObject Utility](sobjectutility.md)

[Back to readme](../README.md)

## Methods

### `generateRandomHexColorString(Boolean includeHashSymbol)`

Generate a random string representing a hexadecimal color string.

```apex
String hexString = DataFactory.generateRandomHexColorString(false);
System.debug(hexString);    // 596F98

hexString = DataFactory.generateRandomHexColorString(true);
System.debug(hexString);    // #2BA261
```

### `generateRandomString(Integer len)`

Creates and returns a randomly-generated string of the specified length.

```apex

```

[Back to readme](../README.md)
