# Data Factory

A collection of methods for generating, manipulating, and analyzing Strings in Apex.

* [Data Factory](dateutility.md)
* [Date Utility](dateutility.md)
* [Integer Utility](integerutility.md)
* [List Utility](listutility.md)
* [Security Utility](securityutility.md)
* [Set Utility](setutility.md)
* [SObject Utility](sobjectutility.md)
* String Utility

[Back to readme](../README.md)

## Methods

### `random(Integer len)`

Creates and returns a randomly-generated string of the specified length.

```apex
// Example here...
```

### `random(Integer len, Boolean useNumbers, Boolean useLowercaseLetters, Boolean useUppercaseLetters)`

Creates a randomly-generated string of characters of the specified length.

```apex
// Example here...
```

### `random(Integer len, String dictionary)`

Creates a randomly-generated string of characters, from the supplied string, of the specified length.

```apex
// Example here...
```
