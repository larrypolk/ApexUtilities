# Security Utility

A collection of methods for Security manipulation and analysis in Apex.

* [Data Factory](datafactory.md)
* [Date Utility](dateutility.md)
* [Integer Utility](integerutility.md)
* [List Utility](listutility.md)
* Security Utility
* [Set Utility](setutility.md)
* [SObject Utility](sobjectutility.md)

[Back to readme](../README.md)

## Methods

### `getSanitizedUrlString(String url)`

Returns a sanitized URL string.

```apex
String url = SecurityUtility.getSanitizedUrlString('///www.newurlstring.com');
System.debug(url);    // /www.newurlstring.com
```

### `getSanitizedPageReference(String url)`

Returns a sanitized PageReference, instantiated from the provided URL string.

```apex
PageReference pr = SecurityUtility.getSanitizedPageReference('///www.newurlstring.com');
System.debug(pr.getUrl());    // /www.newurlstring.com
```

[Back to readme](../README.md)
