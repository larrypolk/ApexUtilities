# DateUtility

A collection of methods for Apex Date generation, analysis, and manipulation.

* [Data Factory](datafactory.md)
* Date Utility
* [Integer Utility](integerutility.md)
* [List Utility](listutility.md)
* [Security Utility](securityutility.md)
* [Set Utility](setutility.md)
* [SObject Utility](sobjectutility.md)

[Back to readme](../README.md)

## Methods

### `getFirstDayOfMonth(Date sourceDate)`
    
Determines the date for the first day of the month using the provided date argument.

```apex
Date dateInitial = Date.newInstance(2018, 1, 12);
Date dateStartOfMonth = DateUtility.getFirstDayOfMonth(dateInitial);
System.debug(dateStartOfMonth);    // 2018-01-01 00:00:00
```

### `getFirstDayOfMonth(Datetime sourceDate)`

Determines the datetime for the first day of the month using the provided datetime argument.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 12, 1, 0, 0);
Datetime datetimeStartOfMonth = DateUtility.getFirstDayOfMonth(datetimeInitial);
System.debug(datetimeStartOfMonth);    // 2018-01-01 09:00:00
```
    
### `getLastDayOfMonth(Date sourceDate)`
    
Determines the date for the last day of the month using the provided date argument.

```apex
Date dateInitial = Date.newInstance(2018, 1, 12);
Date dateEndOfMonth = DateUtility.getLastDayOfMonth(dateInitial);
System.debug(dateEndOfMonth);    // 2018-01-31 00:00:00
```

### `getLastDayOfMonth(Datetime sourceDate)`
    
Determines the datetime for the last day of the month using the provided datetime argument.

```apex
Datetime dateInitial = Datetime.newInstance(2018, 1, 12, 1, 0, 0);
Datetime dateEndOfMonth = DateUtility.getLastDayOfMonth(dateInitial);
System.debug(dateEndOfMonth);    // 2018-01-31 09:00:00
```
    
### `getLastDayOfNextMonth(Date sourceDate)`

Determines the date for the last day of the following month using the provided date argument.

```apex
Date dateInitial = Date.newInstance(2018, 1, 12);
Date dateEndOfNextMonth = DateUtility.getLastDayOfNextMonth(dateInitial);
System.debug(dateEndOfNextMonth);    // 2018-02-28 00:00:00
```

### `getLastDayOfNextMonth(Datetime sourceDate)`

Determines the datetime for the last day of the following month using the provided datetime argument.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 12, 1, 0, 0);
Datetime datetimeEndOfNextMonth = DateUtility.getLastDayOfNextMonth(datetimeInitial);
System.debug(datetimeEndOfNextMonth);    // 2018-02-28 09:00:00
```

### `getFirstDayOfNextMonth(Date sourceDate)`

Determines the date for the first day of the following month using the provided date argument.

```apex
Date dateInitial = Date.newInstance(2018, 1, 12);
Date dateFirstDayOfNextMonth = DateUtility.getFirstDayOfNextMonth(dateInitial);
System.debug(dateFirstDayOfNextMonth);    // 2018-02-01 00:00:00
```

### `getFirstDayOfNextMonth(Datetime sourceDate)`

Determines the datetime for the first day of the following month using the provided datetime argument.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 12, 1, 0, 0);
Datetime datetimeFirstDayOfNextMonth = DateUtility.getFirstDayOfNextMonth(datetimeInitial);
System.debug(datetimeFirstDayOfNextMonth);    // 2018-02-01 09:00:00
```

### `getFirstDayOfPreviousMonth(Date sourceDate)`

Determines the date for the first day of the previous month using the provided date argument.

```apex
Date dateInitial = Date.newInstance(2018, 1, 12);
Date dateFirstDayOfPrevMonth = DateUtility.getFirstDayOfPreviousMonth(dateInitial);
System.debug(dateFirstDayOfPrevMonth);    // 2017-12-01 00:00:00
```

### `getFirstDayOfPreviousMonth(Datetime sourceDate)`

Determines the datetime for the first day of the previous month using the provided date argument.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 12, 1, 0, 0);
Datetime dateFirstDayOfPrevMonth = DateUtility.getFirstDayOfPreviousMonth(datetimeInitial);
System.debug(dateFirstDayOfPrevMonth);    // 2017-12-01 09:00:00
```

### `getLastDayOfPreviousMonth(Date sourceDate)`

Determines the date for the last day of the previous month using the provided date argument.

```apex
Date dateInitial = Date.newInstance(2018, 1, 12);
Date dateLastDayOfPrevMonth = DateUtility.getLastDayOfPreviousMonth(dateInitial);
System.debug(dateLastDayOfPrevMonth);    // 2017-12-31 00:00:00
```

### `getLastDayOfPreviousMonth(Datetime sourceDate)`

Determines the datetime for the last day of the previous month using the provided datetime argument.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 12, 1, 0, 0);
Datetime datetimeLastDayOfPrevMonth = DateUtility.getLastDayOfPreviousMonth(datetimeInitial);
System.debug(datetimeLastDayOfPrevMonth);    // 2017-12-31 09:00:00
```

### `subtractYears(Date sourceDate, Integer yearsToSubtract)`

Determines the date created by subtracting the specified number of years from the provided date argument.

```apex
Date dateInitial = Date.newInstance(2018, 1, 12);
Date afterSubtract = DateUtility.subtractYears(dateInitial, 2);
System.debug(afterSubtract);    // 2016-01-12 00:00:00
```

### `subtractYears(Datetime sourceDate, Integer yearsToSubtract)`

Determines the datetime created by subtracting the specified number of years from the provided datetime argument.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 12, 1, 0, 0);
Datetime afterSubtract = DateUtility.subtractYears(datetimeInitial, 2);
System.debug(afterSubtract);    // 2016-01-12 09:00:00
```

### `subtractMonths(Date sourceDate, Integer monthsToSubtract)`

Determines the datetime created by subtracting the specified number of months from the provided datetime argument.

```apex
Date dateInitial = Date.newInstance(2018, 1, 12);
Date afterSubtract = DateUtility.subtractMonths(dateInitial, 4);
System.debug(afterSubtract);    // 2018-09-12 00:00:00
```

### `subtractDays(Date sourceDate, Integer daysToSubtract)`

Determines the date created by subtracting the specified number of days from the provided date argument.

```apex
Date dateInitial = Date.newInstance(2018, 1, 12);
Date afterSubtract = DateUtility.subtractDays(dateInitial, 4);
System.debug(afterSubtract);    // 2018-01-08 00:00:00
```

### `getFirstInstanceOfDayInMonth(Date sourceDate, String dayName)`

Determines the date of the first instance of a given day (Monday, Tuesday, Wednesday, etc.) using the provided date argument.

```apex
Date dateInitial = Date.newInstance(2018, 1, 12);
Date firstSunday = DateUtility.getFirstInstanceOfDayInMonth(dateInitial, 'Sunday');
System.debug(firstSunday);    // 2018-01-07 00:00:00
```

### `getFirstInstanceOfDayInMonth(Datetime sourceDate, String dayName)`

Determines the datetime of the first instance of a given day (Monday, Tuesday, Wednesday, etc.) using the provided datetime argument.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 12, 1, 0, 0);
Datetime firstSunday = DateUtility.getFirstInstanceOfDayInMonth(datetimeInitial, 'Sunday');
System.debug(firstSunday);    // 2018-01-07 09:00:00
```

### `getDaysOfMonthByDayOfWeek(Date sourceDate)`

Creates a Map of dates for the month, keyed on day name using the provided date argument.

```apex
Date dateInitial = Date.newInstance(2018, 1, 12);
Map<String, Date[]> datesByDayName = DateUtility.getDaysOfMonthByDayOfWeek(dateInitial);
System.debug(datesByDayName);
/*
{
    Friday=(2018-01-05 00:00:00, 2018-01-12 00:00:00, 2018-01-19 00:00:00, 2018-01-26 00:00:00), 
    Monday=(2018-01-01 00:00:00, 2018-01-08 00:00:00, 2018-01-15 00:00:00, 2018-01-22 00:00:00, 
            2018-01-29 00:00:00), 
    Saturday=(2018-01-06 00:00:00, 2018-01-13 00:00:00, 2018-01-20 00:00:00, 2018-01-27 00:00:00), 
    Sunday=(2018-01-07 00:00:00, 2018-01-14 00:00:00, 2018-01-21 00:00:00, 2018-01-28 00:00:00), 
    Thursday=(2018-01-04 00:00:00, 2018-01-11 00:00:00, 2018-01-18 00:00:00, 2018-01-25 00:00:00), 
    Tuesday=(2018-01-02 00:00:00, 2018-01-09 00:00:00, 2018-01-16 00:00:00, 2018-01-23 00:00:00, 
             2018-01-30 00:00:00), 
    Wednesday=(2018-01-03 00:00:00, 2018-01-10 00:00:00, 2018-01-17 00:00:00, 2018-01-24 00:00:00, 
               2018-01-31 00:00:00)
}
 */
```

### `getDaysOfMonthByDayOfWeek(Integer month, Integer year)`

Creates a Map of dates for the month, keyed on day name using the provided date argument.

```apex
Map<String, Date[]> datesByDayName = DateUtility.getDaysOfMonthByDayOfWeek(1, 2018);
System.debug(datesByDayName);
/*
{
    Friday=(2018-01-05 00:00:00, 2018-01-12 00:00:00, 2018-01-19 00:00:00, 2018-01-26 00:00:00), 
    Monday=(2018-01-01 00:00:00, 2018-01-08 00:00:00, 2018-01-15 00:00:00, 2018-01-22 00:00:00, 
            2018-01-29 00:00:00), 
    Saturday=(2018-01-06 00:00:00, 2018-01-13 00:00:00, 2018-01-20 00:00:00, 2018-01-27 00:00:00), 
    Sunday=(2018-01-07 00:00:00, 2018-01-14 00:00:00, 2018-01-21 00:00:00, 2018-01-28 00:00:00), 
    Thursday=(2018-01-04 00:00:00, 2018-01-11 00:00:00, 2018-01-18 00:00:00, 2018-01-25 00:00:00), 
    Tuesday=(2018-01-02 00:00:00, 2018-01-09 00:00:00, 2018-01-16 00:00:00, 2018-01-23 00:00:00, 
             2018-01-30 00:00:00), 
    Wednesday=(2018-01-03 00:00:00, 2018-01-10 00:00:00, 2018-01-17 00:00:00, 2018-01-24 00:00:00, 
               2018-01-31 00:00:00)
}
 */
```

### `getDayOfWeekName(Date d)`

Returns the day of the week name for the provided Date.

```apex
Date dateInitial = Date.newInstance(2018, 1, 1);    // Monday
String dayName = DateUtility.getDayOfWeekName(dateInitial);
System.debug(dayName);    // Monday
```

### `getDayOfWeekName(Datetime dt)`

Returns the day of the week name for the provided Datetime.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 1, 1, 0, 0);    // Monday
String dayName = DateUtility.getDayOfWeekName(datetimeInitial);
System.debug(dayName);    // Monday
```

### `getDayOfWeekNumber(Date d)`

Returns a string representing the number of the day of the week for the provided Datetime argument.

```apex
Date dateInitial = Date.newInstance(2018, 1, 1);    // Monday
String dayNum = DateUtility.getDayOfWeekName(dateInitial);
System.debug(dayNum);    // 1
```

### `getDayOfWeekNumber(Datetime dt)`

Returns a string representing the number of the day of the week for the provided Datetime argument.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 1, 1, 0, 0);    // Monday
String dayNum = DateUtility.getDayOfWeekName(datetimeInitial);
System.debug(dayNum);    // 1
```

### `isWeekday(Date d)`

Determines if the provided date falls on a weekday (Monday - Friday).

```apex
Date dateInitial = Date.newInstance(2018, 1, 1);    // Monday
Boolean isWeekday = DateUtility.isWeekday(dateInitial);
System.debug(isWeekday);    // true
```

### `isWeekday(Datetime dt)`

Determines if the provided datetime falls on a weekday (Monday - Friday).

```apex
Datetime datetimeInitial = Date.newInstance(2018, 1, 1);    // Monday
Boolean isWeekday = DateUtility.isWeekday(datetimeInitial);
System.debug(isWeekday);    // true
```

### `isWeekend(Date d)`

Determines if the provided date falls on a weekend (Saturday, Sunday).

```apex
Date dateInitial = Date.newInstance(2018, 1, 1);    // Monday
Boolean isWeekend = DateUtility.isWeekend(dateInitial);
System.debug(isWeekend);    // false
```

### `isWeekend(Datetime dt)`

Determines if the provided datetime falls on a weekend (Saturday, Sunday).

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 1, 1, 0, 0);    // Monday
Boolean isWeekend = DateUtility.isWeekend(datetimeInitial);
System.debug(isWeekend);    // false
```

### `isMonday(Date d)`

Determines if the provided date falls on a Monday.

```apex
Date dateInitial = Date.newInstance(2018, 1, 1);    // Monday
Boolean isMonday = DateUtility.isMonday(dateInitial);
System.debug(isMonday);    // true

dateInitial = dateInitial.addDays(1);    // Sunday
isMonday = DateUtility.isMonday(dateInitial);
System.debug(isMonday);    // false
```

### `isMonday(Datetime dt)`

Determines if the provided datetime falls on a Monday.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 1, 1, 0, 0);
Boolean isMonday = DateUtility.isMonday(datetimeInitial);
System.debug(isMonday);    // true

isMonday = DateUtility.isMonday(datetimeInitial.addDays(1));
System.debug(isMonday);    // false
```

### `isTuesday(Date d)`

Determines if the provided Date falls on a Tuesday.

```apex
Date dateInitial = Date.newInstance(2018, 1, 2);    // Tuesday
Boolean isTuesday = DateUtility.isTuesday(dateInitial);
System.debug(isTuesday);    // true

dateInitial = dateInitial.addDays(1);    // Wednesday
isTuesday = DateUtility.isTuesday(dateInitial);
System.debug(isTuesday);    // false
```

### `isTuesday(Datetime dt)`

Determines if the provided Datetime falls on a Tuesday.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 2, 1, 0, 0);    // Tuesday
Boolean isTuesday = DateUtility.isTuesday(datetimeInitial);
System.debug(isTuesday);    // true

datetimeInitial = datetimeInitial.addDays(1);    // Wednesday
isTuesday = DateUtility.isTuesday(datetimeInitial);
System.debug(isTuesday);    // false
```

### `isWednesday(Date d)`

Determines if the specified Date falls on a Wednesday.

```apex
Date dateInitial = Date.newInstance(2018, 1, 3);    // Wednesday
Boolean isWednesday = DateUtility.isWednesday(dateInitial);
System.debug(isWednesday);    // true

dateInitial = dateInitial.addDays(1);    // Thursday
isWednesday = DateUtility.isWednesday(dateInitial);
System.debug(isWednesday);    // false
```

### `isWednesday(Datetime dt)`

Determines if the specified Datetime falls on a Wednesday.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 3);    // Wednesday
Boolean isWednesday = DateUtility.isWednesday(datetimeInitial);
System.debug(isWednesday);    // true

datetimeInitial = datetimeInitial.addDays(1);    // Thursday
isWednesday = DateUtility.isWednesday(datetimeInitial);
System.debug(isWednesday);    // false
```

### `isThursday(Date d)`

Determines if the provided Date falls on a Thursday.

```apex
Date dateInitial = Date.newInstance(2018, 1, 4);    // Thursday
Boolean isThursday = DateUtility.isThursday(dateInitial);
System.debug(isThursday);    // true

dateInitial = dateInitial.addDays(1);    // Friday
isThursday = DateUtility.isThursday(dateInitial);
System.debug(isThursday);    // false
```

### `isThursday(Datetime dt)`

Determines if the provided Datetime falls on a Thursday.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 4, 1, 0, 0);    // Thursday
Boolean isThursday = DateUtility.isThursday(datetimeInitial);
System.debug(isThursday);    // true

datetimeInitial = datetimeInitial.addDays(1);    // Friday
isThursday = DateUtility.isThursday(datetimeInitial);
System.debug(isThursday);    // false
```

### `isFriday(Date d)`

Determines if the provided Date falls on a Friday.

```apex
Date dateInitial = Date.newInstance(2018, 1, 5);    // Friday
Boolean isFriday = DateUtility.isFriday(dateInitial);
System.debug(isFriday);    // true

dateInitial = dateInitial.addDays(1);    // Saturday
isFriday = DateUtility.isFriday(dateInitial);
System.debug(isFriday);    // false
```

### `isFriday(Datetime dt)`

Determines if the provided Datetime falls on a Friday.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 5, 1, 0, 0);    // Friday
Boolean isFriday = DateUtility.isFriday(datetimeInitial);
System.debug(isFriday);    // true

datetimeInitial = datetimeInitial.addDays(1);    // Saturday
isFriday = DateUtility.isFriday(datetimeInitial);
System.debug(isFriday);    // false
```

### `isSaturday(Date d)`

Determines if the provided Date falls on a Saturday.

```apex
Date dateInitial = Date.newInstance(2018, 1, 6);    // Saturday
Boolean isSaturday = DateUtility.isSaturday(dateInitial);
System.debug(isSaturday);    // true

dateInitial = dateInitial.addDays(1);    // Sunday
isSaturday = DateUtility.isSaturday(dateInitial);
System.debug(isSaturday);    // false
```

### `isSaturday(Datetime dt)`

Determines if the provided Datetime falls on a Saturday.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 6);    // Saturday
Boolean isSaturday = DateUtility.isSaturday(datetimeInitial);
System.debug(isSaturday);    // true

datetimeInitial = datetimeInitial.addDays(1);    // Sunday
isSaturday = DateUtility.isSaturday(datetimeInitial);
System.debug(isSaturday);    // false
```

### `isSunday(Date d)`

Determines if the provided Date falls on a Sunday.

```apex
Date dateInitial = Date.newInstance(2018, 1, 7);    // Sunday
Boolean isSunday = DateUtility.isSunday(dateInitial);
System.debug(isSunday);    // true

dateInitial = dateInitial.addDays(1);    // Monday
isSunday = DateUtility.isSunday(dateInitial);
System.debug(isSunday);    // false
```

### `isSunday(Datetime dt)`

Determines if the provided Datetime falls on a Sunday.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 7);    // Sunday
Boolean isSunday = DateUtility.isSunday(datetimeInitial);
System.debug(isSunday);    // true

datetimeInitial = datetimeInitial.addDays(1);    // Monday
isSunday = DateUtility.isSunday(datetimeInitial);
System.debug(isSunday);    // false
```

### `getMonthDetails(Date d)`

Separates the dates of the days in the month, determined from the provided Date value, into categories.

```apex
Date dateInitial = Date.newInstance(2018, 1, 1);
DateUtility.MonthDetails details = DateUtility.getMonthDetails(dateInitial);
System.debug(JSON.serialize(details));
```

Output:

```json
{
    "weekends": [
        "2018-01-06", "2018-01-07", "2018-01-13", "2018-01-14", "2018-01-20", 
        "2018-01-21", "2018-01-27", "2018-01-28"
    ],
    "weekdays": [
      "2018-01-01", "2018-01-02", "2018-01-03", "2018-01-04", "2018-01-05", 
      "2018-01-08", "2018-01-09", "2018-01-10", "2018-01-11", "2018-01-12", 
      "2018-01-15", "2018-01-16", "2018-01-17", "2018-01-18", "2018-01-19", 
      "2018-01-22", "2018-01-23", "2018-01-24", "2018-01-25", "2018-01-26", 
      "2018-01-29", "2018-01-30", "2018-01-31"
    ],
    "wednesdays": ["2018-01-03", "2018-01-10", "2018-01-17", "2018-01-24", "2018-01-31"],
    "tuesdays": ["2018-01-02", "2018-01-09", "2018-01-16", "2018-01-23", "2018-01-30"],
    "thursdays": ["2018-01-04", "2018-01-11", "2018-01-18", "2018-01-25"],
    "sundays": ["2018-01-07", "2018-01-14", "2018-01-21", "2018-01-28"],
    "saturdays": ["2018-01-06", "2018-01-13", "2018-01-20", "2018-01-27"],
    "mondays": ["2018-01-01", "2018-01-08", "2018-01-15", "2018-01-22", "2018-01-29"],
    "MASK_DAY_OF_WEEK": "u",
    "lastOfMonth": "2018-01-31T08:00:00.000Z",
    "fridays": ["2018-01-05", "2018-01-12", "2018-01-19", "2018-01-26"],
    "fiscalYearStartMonth": 1,
    "fiscalQuarter": 1,
    "firstOfMonth": "2018-01-01T08:00:00.000Z"
}
```

### `getMonthDetails(Datetime dt)`

Separates the dates of the days in the month, determined from the provided Datetime value, into categories.

```apex
Datetime datetimeInitial = Datetime.newInstance(2018, 1, 1, 1, 0, 0);
DateUtility.MonthDetails details = DateUtility.getMonthDetails(datetimeInitial);
System.debug(JSON.serialize(details));
```

Output:

```json
{
    "weekends": [
        "2018-01-06", "2018-01-07", "2018-01-13", "2018-01-14", "2018-01-20", 
        "2018-01-21", "2018-01-27", "2018-01-28"
    ],
    "weekdays": [
        "2018-01-01", "2018-01-02", "2018-01-03", "2018-01-04", "2018-01-05", 
        "2018-01-08", "2018-01-09", "2018-01-10", "2018-01-11", "2018-01-12", 
        "2018-01-15", "2018-01-16", "2018-01-17", "2018-01-18", "2018-01-19", 
        "2018-01-22", "2018-01-23", "2018-01-24", "2018-01-25", "2018-01-26", 
        "2018-01-29", "2018-01-30", "2018-01-31"
    ],
    "wednesdays": ["2018-01-03", "2018-01-10", "2018-01-17", "2018-01-24", "2018-01-31"],
    "tuesdays": ["2018-01-02", "2018-01-09", "2018-01-16", "2018-01-23", "2018-01-30"],
    "thursdays": ["2018-01-04", "2018-01-11", "2018-01-18", "2018-01-25"],
    "sundays": ["2018-01-07", "2018-01-14", "2018-01-21", "2018-01-28"],
    "saturdays": ["2018-01-06", "2018-01-13", "2018-01-20", "2018-01-27"],
    "mondays": ["2018-01-01", "2018-01-08", "2018-01-15", "2018-01-22", "2018-01-29"],
    "MASK_DAY_OF_WEEK": "u",
    "lastOfMonth": "2018-01-31T09:00:00.000Z",
    "fridays": ["2018-01-05", "2018-01-12", "2018-01-19", "2018-01-26"],
    "fiscalYearStartMonth": 1,
    "fiscalQuarter": 1,
    "firstOfMonth": "2018-01-01T09:00:00.000Z"
}
```

### `getMonthDetails(Integer month, Integer year)`

Separates the dates of the days in the month, determined from the provided month and year value, into categories.

```apex
DateUtility.MonthDetails details = DateUtility.getMonthDetails(1, 2018);
System.debug(JSON.serialize(details));
```

Output:

```json
{
    "weekends": [
        "2018-01-06", "2018-01-07", "2018-01-13", "2018-01-14", "2018-01-20", "2018-01-21", 
        "2018-01-27", "2018-01-28"
    ],
    "weekdays": [
        "2018-01-01", "2018-01-02", "2018-01-03", "2018-01-04", "2018-01-05", "2018-01-08", 
        "2018-01-09", "2018-01-10", "2018-01-11", "2018-01-12", "2018-01-15", "2018-01-16", 
        "2018-01-17", "2018-01-18", "2018-01-19", "2018-01-22", "2018-01-23", "2018-01-24", 
        "2018-01-25", "2018-01-26", "2018-01-29", "2018-01-30", "2018-01-31"
    ],
    "wednesdays": ["2018-01-03", "2018-01-10", "2018-01-17", "2018-01-24", "2018-01-31"],
    "tuesdays": ["2018-01-02", "2018-01-09", "2018-01-16", "2018-01-23", "2018-01-30"],
    "thursdays": ["2018-01-04", "2018-01-11", "2018-01-18", "2018-01-25"],
    "sundays": ["2018-01-07", "2018-01-14", "2018-01-21", "2018-01-28"],
    "saturdays": ["2018-01-06", "2018-01-13", "2018-01-20", "2018-01-27"],
    "mondays": ["2018-01-01", "2018-01-08", "2018-01-15", "2018-01-22", "2018-01-29"],
    "MASK_DAY_OF_WEEK": "u",
    "lastOfMonth": "2018-01-31T08:00:00.000Z",
    "fridays": ["2018-01-05", "2018-01-12", "2018-01-19", "2018-01-26"],
    "fiscalYearStartMonth": 1,
    "fiscalQuarter": 1,
    "firstOfMonth": "2018-01-01T08:00:00.000Z"
}
```

[Back to readme](../README.md)