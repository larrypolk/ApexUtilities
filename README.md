# Apex Utilities

A collection of Apex utility classes providing a variety of operations for Data, Dates, Integers, Lists, Security, Sets, and SObjects.

* [Data Factory](docs/datafactory.md)

  A utility to create random and dummy data, including random hexadecimal color strings, strings, and more.

* [Date Utility](docs/dateutility.md)

  A collection of methods for Apex Date generation, analysis, and manipulation.

* [Integer Utility](docs/integerutility.md)

  A collection of helpful utilities for manipulating and generating Integers in Apex.

* [List Utility](docs/listutility.md)

  A collection of methods for manipulating and creating Lists in Apex.

* [Security Utility](docs/securityutility.md)

  A collection of methods for Security manipulation and analysis in Apex.

* [Set Utility](docs/setutility.md)

  A collection of methods for creating and manipulating Sets in Apex.

* [SObject Utility](docs/sobjectutility.md)

  A collection of methods for manipulating and analyzing SObjects in Apex.
  
* [StringUtility](docs/stringutility.md)

  A collection of methods for generating, manipulating, and analyzing Strings in Apex.
  
