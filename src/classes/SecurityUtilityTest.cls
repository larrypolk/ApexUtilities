/*
 * SecurityUtilityTest.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
@IsTest
public with sharing class SecurityUtilityTest {
    @IsTest
    public static void testGetSanitizedUrlString() {
        String url = null;
        String expected = null;
        String actual = SecurityUtility.getSanitizedUrlString(url);

        System.assertEquals(expected, actual, 'The sanitized string does not match the expected value.');
    }

    @IsTest
    public static void testGetSanitizedPageReference() {
        String validUrl;
        PageReference expected;
        for (String url : getTestStrings()) {
            validUrl = '/' + (url.startsWith('/') ? url.replaceFirst('/+', '') : url);
            expected = new PageReference(url);

            System.assertEquals(
                validUrl, SecurityUtility.getSanitizedPageReference(url).getUrl(),
                'The sanitized PageReference does not match the expected value.'
            );
        }
    }

    //
    // Helper methods
    //

    public static String[] getTestStrings() {
        return new String[] {
            'www.dummy-url.com',
            '/www.dummy-url.com',
            '//www.dummy-url.com',
            '///www.dummy-url.com',
            '////www.dummy-url.com'
        };
    }
}
