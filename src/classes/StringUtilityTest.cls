/* 
 * StringUtilityTest.cls
 * 
 * Author: Larry Polk
 * 
 * Copyright 2018. See the file "LICENSE" for the full license governing this code. 
 */
@IsTest
public class StringUtilityTest {
    @TestSetup
    public static void setup() {
        // Setup code
    }

    @IsTest
    public static void testRandomNegativePath() {
        Integer length = -1;
        Boolean useDigits = null;
        Boolean useLowercaseLetters = null;
        Boolean useUppercaseLetters = null;

        Test.startTest();

        // Test with all bad input.
        String actual = StringUtility.random(length, useDigits, useLowercaseLetters, useUppercaseLetters);
        System.assert(actual== StringUtility.EMPTY_STRING);

        // Test with good length, bad Booleans
        length = 100;
        actual = StringUtility.random(length, useDigits, useLowercaseLetters, useUppercaseLetters);
        System.assert(actual== StringUtility.EMPTY_STRING);

        // Test with bad length, good useNumbers, and bad useLowercaseLetters and useUppercaseLetters.
        length = -1;
        useDigits = true;
        actual = StringUtility.random(length, useDigits, useLowercaseLetters, useUppercaseLetters);
        System.assert(actual== StringUtility.EMPTY_STRING);

        // Test with bad length, good useNumbers and useLowercaseLetters, and bad useUppercaseLetters.
        length = -1;
        useDigits = true;
        useLowercaseLetters = true;
        actual = StringUtility.random(length, useDigits, useLowercaseLetters, useUppercaseLetters);
        System.assert(actual== StringUtility.EMPTY_STRING);

        // Test with bad length and good Booleans.
        length = -1;
        useDigits = true;
        useLowercaseLetters = true;
        useUppercaseLetters = true;
        actual = StringUtility.random(length, useDigits, useLowercaseLetters, useUppercaseLetters);
        System.assert(actual== StringUtility.EMPTY_STRING);

        // Test with good length and good Booleans.
        length = 1;
        useDigits = true;
        useLowercaseLetters = false;
        useUppercaseLetters = false;
        Set<String> expected = new Set<String>(StringUtility.DIGITS.split(''));
        actual = StringUtility.random(length, useDigits, useLowercaseLetters, useUppercaseLetters);
        System.assert(expected.contains(actual));

        Test.stopTest();
    }

    @IsTest
    public static void testRandomPositivePath() {
        Integer length = 100;
        String actual = StringUtility.random(length);

        Set<String> lowercase = new Set<String>(StringUtility.ASCII_LOWER.split(''));
        Set<String> uppercase = new Set<String>(StringUtility.ASCII_UPPER.split(''));
        Set<String> numbers = new Set<String>(StringUtility.DIGITS.split(''));

        System.assert(actual.length() == length);

        for (String s : actual.split('')) {
            System.assert(lowercase.contains(s) || uppercase.contains(s) || numbers.contains(s));
        }
    }

    @IsTest
    public static void testRandomOnlyDigits() {
        Integer length = 100;

        // Test only lowercase strings
        Boolean useDigits = true;
        Boolean useLowercaseLetters = false;
        Boolean useUppercaseLetters = false;
        String actual = StringUtility.random(length, useDigits, useLowercaseLetters, useUppercaseLetters);

        Set<String> lowercase = new Set<String>(StringUtility.ASCII_LOWER.split(''));
        Set<String> uppercase = new Set<String>(StringUtility.ASCII_UPPER.split(''));
        Set<String> digits = new Set<String>(StringUtility.DIGITS.split(''));

        System.assert(actual.length() == length);

        for (String s : actual.split('')) {
            System.assert(digits.contains(s));
            System.assert(!lowercase.contains(s));
            System.assert(!uppercase.contains(s));
        }
    }

    @IsTest
    public static void testRandomOnlyLowercase() {
        Integer length = 100;

        // Test only lowercase strings
        Boolean useNumbers = false;
        Boolean useLowercaseLetters = true;
        Boolean useUppercaseLetters = false;
        String actual = StringUtility.random(length, useNumbers, useLowercaseLetters, useUppercaseLetters);

        Set<String> lowercase = new Set<String>(StringUtility.ASCII_LOWER.split(''));
        Set<String> uppercase = new Set<String>(StringUtility.ASCII_UPPER.split(''));
        Set<String> numbers = new Set<String>(StringUtility.DIGITS.split(''));

        System.assert(actual.length() == length);

        for (String s : actual.split('')) {
            System.assert(!numbers.contains(s));
            System.assert(lowercase.contains(s));
            System.assert(!uppercase.contains(s));
        }
    }

    @IsTest
    public static void testRandomOnlyUppercase() {
        Integer length = 100;

        // Test only lowercase strings
        Boolean useNumbers = false;
        Boolean useLowercaseLetters = false;
        Boolean useUppercaseLetters = true;
        String actual = StringUtility.random(length, useNumbers, useLowercaseLetters, useUppercaseLetters);

        Set<String> lowercase = new Set<String>(StringUtility.ASCII_LOWER.split(''));
        Set<String> uppercase = new Set<String>(StringUtility.ASCII_UPPER.split(''));
        Set<String> numbers = new Set<String>(StringUtility.DIGITS.split(''));

        System.assert(actual.length() == length);

        for (String s : actual.split('')) {
            System.assert(!numbers.contains(s));
            System.assert(!lowercase.contains(s));
            System.assert(uppercase.contains(s));
        }
    }
}
