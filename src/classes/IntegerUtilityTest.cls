/* 
 * IntegerUtilityTest.cls
 * 
 * Author: Larry Polk
 * 
 * Copyright 2018. See the file "LICENSE" for the full license governing this code. 
 */
@IsTest
public with sharing class IntegerUtilityTest {
    @IsTest
    public static void testD4() {
        Integer min = 1;
        Integer max = 4;
        Integer actual = IntegerUtility.d4();

        System.assert(actual >= min && actual <= max);
    }

    @IsTest
    public static void testD6() {
        Integer min = 1;
        Integer max = 6;
        Integer actual = IntegerUtility.d6();

        System.assert(actual >= min && actual <= max);
    }

    @IsTest
    public static void testD8() {
        Integer min = 1;
        Integer max = 8;
        Integer actual = IntegerUtility.d8();

        System.assert(actual >= min && actual <= max);
    }

    @IsTest
    public static void testD10() {
        Integer min = 1;
        Integer max = 10;
        Integer actual = IntegerUtility.d10();

        System.assert(actual >= min && actual <= max);
    }

    @IsTest
    public static void testD12() {
        Integer min = 1;
        Integer max = 12;
        Integer actual = IntegerUtility.d12();

        System.assert(actual >= min && actual <= max);
    }

    @IsTest
    public static void testD20() {
        Integer min = 1;
        Integer max = 20;
        Integer actual = IntegerUtility.d20();

        System.assert(actual >= min && actual <= max);
    }

    @IsTest
    public static void testD30() {
        Integer min = 1;
        Integer max = 30;
        Integer actual = IntegerUtility.d30();

        System.assert(actual >= min && actual <= max);
    }

    @IsTest
    public static void testD100() {
        Integer min = 1;
        Integer max = 100;
        Integer actual = IntegerUtility.d100();

        System.assert(actual >= min && actual <= max);
    }

    @IsTest
    public static void testRandomNoArg() {
        Integer[] values = new Integer[] {};

        for (Integer i = 0; i < 10; i++) {
            values.add(IntegerUtility.random());
        }

        for (Integer actual : values) {
            System.assert(actual >= IntegerUtility.MIN_INT_VALUE && actual <= IntegerUtility.MAX_INT_VALUE, actual);
        }
    }

    @IsTest
    public static void testRandomLengthArg() {
        Map<Integer, Integer> tests = new Map<Integer, Integer> {
            1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10
        };

        // Check when length is null or zero.
        Integer len = null;
        System.assertEquals(null, IntegerUtility.random(len));
        System.assertEquals(null, IntegerUtility.random(0));

        for (Integer test : tests.keySet()) {
            System.assertEquals(tests.get(test), String.valueOf(IntegerUtility.random(test)).split('').size());
        }

        // Check when length is longer than max integer length
        Integer actual;
        try {
            actual = IntegerUtility.random(IntegerUtility.MAX_INT_LENGTH + 1);
        } catch (Exception e) {
            String message = Exceptions.ERR_INVALID_PARAMETER;
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(message);

            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(message));
        }
    }

    @IsTest
    public static void testRandomBooleanArg() {
        // Test random positive values
        Integer[] values = new Integer[] {};

        for (Integer i = 0; i < 10; i++) {
            values.add(IntegerUtility.random(false));
        }

        for (Integer actual : values) {
            System.assert(actual <= IntegerUtility.MAX_INT_VALUE);
        }

        // Test random negative values
        values.clear();
        for (Integer i = 0; i < 10; i++) {
            values.add(IntegerUtility.random(true));
        }

        for (Integer actual : values) {
            System.assert(actual >= IntegerUtility.MIN_INT_VALUE, actual);
        }
    }

    @IsTest
    public static void testRandomInRange() {
        Integer low = 0;
        Integer high = 9;

        Integer actual = IntegerUtility.random(low, high);

        // Check with single digit ranges
        System.assert(actual >= low && actual <= high);

        // Check with double digit ranges
        low = 10;
        high = 99;
        actual = IntegerUtility.random(low, high);

        System.assert(actual >= low && actual <= high);

        // Check with triple digit ranges
        low = 100;
        high = 999;
        actual = IntegerUtility.random(low, high);

        System.assert(actual >= low && actual <= high);

        // Check with four digit ranges
        low = 1000;
        high = 9999;
        actual = IntegerUtility.random(low, high);

        System.assert(actual >= low && actual <= high);

        // Check with five digit ranges
        low = 10000;
        high = 99999;
        actual = IntegerUtility.random(low, high);

        System.assert(actual >= low && actual <= high);

        // Check when low and high values are in the wrong order.
        low = 9;
        high = 0;
        actual = IntegerUtility.random(low, high);

        System.assert(actual >= high && actual <= low);

        // Check when low and high values are the same.
        low = 9;
        high = 9;
        Integer expected = 9;
        actual = IntegerUtility.random(low, high);

        System.assertEquals(expected, actual);

        // Test exception generation
        low = null;
        high = null;
        try {
            actual = IntegerUtility.random(low, high);
        } catch (Exception e) {
            Exceptions.InvalidParameterException expExcept = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);

            System.assertEquals(expExcept.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }
    }
}
