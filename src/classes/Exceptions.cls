/*
 * Exceptions.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
public with sharing class Exceptions {
    public static final String ERR_INVALID_FIELD = 'The supplied field does not exist on the object type.';
    public static final String ERR_INVALID_FIELDSET = 'The supplied field set does not exist on the object type.';
    public static final String ERR_INVALID_OBJECT = 'The supplied SObject type does not exist in the org.';
    public static final String ERR_INVALID_PARAMETER = 'One or more of the supplied parameters is invalid.';

    public with sharing class GenericException extends Exception {}

    public with sharing class InvalidFieldException extends Exception {}
    public with sharing class InvalidFieldSetException extends Exception {}
    public with sharing class InvalidObjectTypeException extends Exception {}
    public with sharing class InvalidParameterException extends Exception {}
}
