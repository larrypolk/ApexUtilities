/*
 * SetUtilityTest.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
@IsTest
public with sharing class SetUtilityTest {
    @IsTest
    public static void testDifference() {
        Test.startTest();

        // First set is null.
        Set<Object> first = null;
        Set<Object> second = null;

        Set<Object> expected = new Set<Object>();
        Set<Object> actual = SetUtility.difference(first, second);

        System.assertEquals(expected, actual);

        // Second set is null.
        first = new Set<Object> { 'a', 'b' };
        second = null;

        expected = new Set<Object> { 'a', 'b' };
        actual = SetUtility.difference(first, second);

        System.assertEquals(expected, actual);

        // First and Second set are not null.
        first = new Set<Object> { 'a', 'b' };
        second = new Set<Object> { 'b', 'c' };

        expected = new Set<Object> { 'a' };
        actual = SetUtility.difference(first, second);

        System.assertEquals(expected, actual);

        // First and Second set are not null.
        first = new Set<Object> { 'a', 'b', 'c', 'e' };
        second = new Set<Object> { 'b', 'c' };

        expected = new Set<Object> { 'a', 'e' };
        actual = SetUtility.difference(first, second);

        System.assertEquals(expected, actual);

        // Slightly more extensive test.
        first = new Set<Object> { 1, 2, 3, 4, 5 };
        second = new Set<Object> { 4, 5, 6, 7, 8 };

        expected = new Set<Object> { 1, 2, 3 };
        actual = SetUtility.difference(first, second);

        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testIntersection() {
        Test.startTest();

        // First set is null.
        Set<Object> first = null;
        Set<Object> second = null;

        Set<Object> expected = new Set<Object>();
        Set<Object> actual = SetUtility.difference(first, second);

        System.assertEquals(expected, actual);

        // Second set is null.
        first = new Set<Object> { 'a', 'b' };
        second = null;

        expected = new Set<Object> {};
        actual = SetUtility.intersection(first, second);

        System.assertEquals(expected, actual);

        // First and Second set are not null.
        first = new Set<Object> { 'a', 'b' };
        second = new Set<Object> { 'b', 'c' };

        expected = new Set<Object> { 'b' };
        actual = SetUtility.intersection(first, second);

        System.assertEquals(expected, actual);

        // First and Second set are not null.
        first = new Set<Object> { 'a', 'b', 'c', 'e' };
        second = new Set<Object> { 'b', 'c' };

        expected = new Set<Object> { 'b', 'c' };
        actual = SetUtility.intersection(first, second);

        System.assertEquals(expected, actual);

        // Slightly more extensive test.
        first = new Set<Object> { 1, 2, 3, 4, 5 };
        second = new Set<Object> { 4, 5, 6, 7, 8 };

        expected = new Set<Object> { 4, 5 };
        actual = SetUtility.intersection(first, second);

        System.assertEquals(expected, actual);

        // No Intersection.
        first = new Set<Object> { 1, 2, 3, 4, 5 };
        second = new Set<Object> { 'a', 'b', 'c', 'd' };

        expected = new Set<Object> {};
        actual = SetUtility.intersection(first, second);

        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testIsDisjoint() {
        Test.startTest();

        // Both sets are null.
        Set<Object> first = null;
        Set<Object> second = null;

        Boolean expected = true;
        Boolean actual = SetUtility.isDisjoint(first, second);

        System.assertEquals(expected, actual);

        // Both sets are empty.
        first = new Set<Object>();
        second = new Set<Object>();

        expected = true;
        actual = SetUtility.isDisjoint(first, second);

        System.assertEquals(expected, actual);

        // First set is empty, second set is not.
        first = new Set<Object>();
        second = new Set<Object> { 1, 2, 3 };

        expected = true;
        actual = SetUtility.isDisjoint(first, second);

        System.assertEquals(expected, actual);

        // First set is not empty, second set is.
        first = new Set<Object> { 1, 2, 3 };
        second = new Set<Object>();

        expected = true;
        actual = SetUtility.isDisjoint(first, second);

        System.assertEquals(expected, actual);

        // Both sets are populated, no overlap.
        first = new Set<Object> { 1, 2, 3 };
        second = new Set<Object> { 'a', 'b' };

        expected = true;
        actual = SetUtility.isDisjoint(first, second);

        System.assertEquals(expected, actual);

        // Both sets are populated, with overlap.
        first = new Set<Object> { 1, 2, 3 };
        second = new Set<Object> { 2, 3, 4 };

        expected = false;
        actual = SetUtility.isDisjoint(first, second);

        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testIsSubset() {
        Test.startTest();

        // Both sets are null.
        Set<Object> first = null;
        Set<Object> second = null;

        Boolean expected = false;
        Boolean actual = SetUtility.isSubset(first, second);

        System.assertEquals(expected, actual);

        // Both sets are empty.
        first = new Set<Object>();
        second = new Set<Object>();

        expected = true;
        actual = SetUtility.isSubset(first, second);

        System.assertEquals(expected, actual);

        // First set is empty, second set is not.
        first = new Set<Object>();
        second = new Set<Object> { 1, 2, 3 };

        expected = false;
        actual = SetUtility.isSubset(first, second);

        System.assertEquals(expected, actual);

        // First set is not empty, second set is.
        first = new Set<Object> { 1, 2, 3 };
        second = new Set<Object>();

        expected = true;
        actual = SetUtility.isSubset(first, second);

        System.assertEquals(expected, actual);

        // Both sets are populated, no overlap.
        first = new Set<Object> { 1, 2, 3 };
        second = new Set<Object> { 'a', 'b' };

        expected = false;
        actual = SetUtility.isSubset(first, second);

        System.assertEquals(expected, actual);

        // Both sets are populated, with overlap.
        first = new Set<Object> { 1, 2, 3 };
        second = new Set<Object> { 2, 3, 4 };

        expected = false;
        actual = SetUtility.isSubset(first, second);

        System.assertEquals(expected, actual);

        // Second set is subset
        first = new Set<Object> { 1, 2, 3, 4, 5 };
        second = new Set<Object> { 2, 3, 4 };

        expected = true;
        actual = SetUtility.isSubset(first, second);

        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testIsSuperset() {
        Test.startTest();

        // Both sets are null.
        Set<Object> first = null;
        Set<Object> second = null;

        Boolean expected = false;
        Boolean actual = SetUtility.isSuperset(first, second);

        System.assertEquals(expected, actual);

        // Both sets are empty.
        first = new Set<Object>();
        second = new Set<Object>();

        expected = true;
        actual = SetUtility.isSuperset(first, second);

        System.assertEquals(expected, actual);

        // First set is empty, second set is not.
        first = new Set<Object>();
        second = new Set<Object> { 1, 2, 3 };

        expected = false;
        actual = SetUtility.isSuperset(first, second);

        System.assertEquals(expected, actual);

        // First set is not empty, second set is.
        first = new Set<Object> { 1, 2, 3 };
        second = new Set<Object>();

        expected = true;
        actual = SetUtility.isSuperset(first, second);

        System.assertEquals(expected, actual);

        // Both sets are populated, no overlap.
        first = new Set<Object> { 1, 2, 3 };
        second = new Set<Object> { 'a', 'b' };

        expected = false;
        actual = SetUtility.isSuperset(first, second);

        System.assertEquals(expected, actual);

        // Both sets are populated, with overlap.
        first = new Set<Object> { 1, 2, 3 };
        second = new Set<Object> { 2, 3, 4 };

        expected = false;
        actual = SetUtility.isSuperset(first, second);

        System.assertEquals(expected, actual);

        // Second set is subset
        first = new Set<Object> { 1, 2, 3, 4, 5 };
        second = new Set<Object> { 2, 3, 4 };

        expected = true;
        actual = SetUtility.isSuperset(first, second);

        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testSymmetricDifference() {
        Test.startTest();

        // First set is null.
        Set<Object> first = null;
        Set<Object> second = null;

        Set<Object> expected = new Set<Object>();
        Set<Object> actual = SetUtility.symmetricDifference(first, second);

        System.assertEquals(expected, actual);

        // First set is empty but second is populated.
        first = new Set<Object> {};
        second = new Set<Object> { 'a', 'b' };

        expected = new Set<Object> { 'a', 'b' };
        actual = SetUtility.symmetricDifference(first, second);

        System.assertEquals(expected, actual);

        // Second set is null.
        first = new Set<Object> { 'a', 'b' };
        second = null;

        expected = new Set<Object> { 'a', 'b' };
        actual = SetUtility.symmetricDifference(first, second);

        System.assertEquals(expected, actual);

        // First and Second set are not null.
        first = new Set<Object> { 'a', 'b' };
        second = new Set<Object> { 'b', 'c' };

        expected = new Set<Object> { 'a', 'c' };
        actual = SetUtility.symmetricDifference(first, second);

        System.assertEquals(expected, actual);

        // First and Second set are not null.
        first = new Set<Object> { 'a', 'b', 'c', 'e' };
        second = new Set<Object> { 'b', 'c' };

        expected = new Set<Object> { 'a', 'e' };
        actual = SetUtility.symmetricDifference(first, second);

        System.assertEquals(expected, actual);

        // Slightly more extensive test.
        first = new Set<Object> { 1, 2, 3, 4, 5 };
        second = new Set<Object> { 4, 5, 6, 7, 8 };

        expected = new Set<Object> { 1, 2, 3, 6, 7, 8 };
        actual = SetUtility.symmetricDifference(first, second);

        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testUnion() {
        Test.startTest();

        // Both sets are null.
        Set<Object> first = null;
        Set<Object> second = null;

        Set<Object> expected = new Set<Object>();
        Set<Object> actual = SetUtility.union(first, second);

        System.assertEquals(expected, actual);

        // Second set is null.
        first = new Set<Object> { 1, 2, 3 };
        second = null;

        expected = new Set<Object> { 1, 2, 3 };
        actual = SetUtility.union(first, second);

        System.assertEquals(expected, actual);

        // Both sets are populated
        first = new Set<Object> { 1, 2, 3 };
        second = new Set<Object> { 4, 5, 6 };

        expected = new Set<Object> { 1, 2, 3, 4, 5, 6 };
        actual = SetUtility.union(first, second);

        System.assertEquals(expected, actual);

        // Both sets are populated with some overlap.
        first = new Set<Object> { 1, 2, 3 };
        second = new Set<Object> { 1, 2, 6 };

        expected = new Set<Object> { 1, 2, 3, 6 };
        actual = SetUtility.union(first, second);

        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testRandom() {
        Test.startTest();

        // Test when source set is null.
        Set<Object> testSet = null;
        Object exp = null;
        Object act = SetUtility.random(testSet);

        System.assertEquals(exp, act);

        // Test when source set is empty.
        testSet = new Set<Object>();
        exp = new Set<Object>();
        act = SetUtility.random(testSet);

        System.assertEquals(exp, act);

        // Test when source set is not empty.
        Object val = 'a';
        testSet = new Set<Object> { val };
        act = SetUtility.random(testSet);

        System.assertEquals(val, act);

        testSet = new Set<Object> { 'a', 'b', 'c' };
        act = SetUtility.random(testSet);

        System.assertEquals(true, testSet.contains(act));

        // Test when number of items is greater than the number of items in the set.
        act = SetUtility.random(testSet, testSet.size() + 1);
        System.assertEquals(testSet, act);

        Test.stopTest();
    }
}
