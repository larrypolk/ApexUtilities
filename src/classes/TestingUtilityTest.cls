/*
 * TestingUtilityTest.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
@IsTest
public with sharing class TestingUtilityTest {
    @IsTest
    public static void testConstructor() {
        Test.startTest();

        TestingUtility tu = new TestingUtility();

        System.assertNotEquals(null, tu, 'The TestingUtility constructor did not function correctly.');

        Test.stopTest();
    }

    @IsTest
    public static void testMockTestException() {
        Test.startTest();

        try {
            System.assertEquals(false, TestingUtility.forceError, 'The forceError value in TestingUtility did not match the expected value.');
            TestingUtility.mockTestError();
        } catch (Exception e) {
            System.assertEquals(false, true, '');
        }

        try {
            System.assertEquals(false, TestingUtility.forceError, 'The forceError value in TestingUtility did not match the expected value.');
            TestingUtility.forceError = true;
            System.assertEquals(true, TestingUtility.forceError, 'The forceError value in TestingUtility did not match the expected value.');
            TestingUtility.mockTestError();
        } catch (Exception e) {
            TestingUtility.MockTestException mte = new TestingUtility.MockTestException(TestingUtility.ERR_MOCK_MESSAGE);
            System.assertEquals(mte.getTypeName(), e.getTypeName(), 'The getTypeName() method did not return the expected value.');
            System.assertEquals(mte.getMessage(), e.getMessage(), 'The getMessage() method did not return the expected value.');
        }

        TestingUtility.forceError = false;    // Be sure to reset the flag's state when you're doing multiple tests.

        // Mock a custom exception.
        String customMessage = 'This is a custom error message.';
        try {
            System.assertEquals(false, TestingUtility.forceError, 'The forceError value in TestingUtility did not match the expected value.');
            TestingUtility.forceError = true;
            System.assertEquals(true, TestingUtility.forceError, 'The forceError value in TestingUtility did not match the expected value.');
            TestingUtility.mockTestError(new TestingUtility.GenericException(customMessage));
        } catch (Exception e) {
            TestingUtility.GenericException ge = new TestingUtility.GenericException(customMessage);
            System.assertEquals(ge.getTypeName(), e.getTypeName(), 'The getTypeName() method did not return the expected value.');
            System.assertEquals(ge.getMessage(), e.getMessage(), 'The getMessage() method did not return the expected value.');
        }

        TestingUtility.forceError = false;    // Be sure to reset the flag's state when you're doing multiple tests.

        // Mock a specific type of Exception
        String customDMLMessage = 'This is a custom DML error message.';
        DmlException dmle = new DmlException(customDMLMessage);
        TestingUtility.GenericException ge = new TestingUtility.GenericException(customMessage);
        try {
            System.assertEquals(false, TestingUtility.forceError, 'The forceError value in TestingUtility did not match the expected value.');
            TestingUtility.forceError = true;
            TestingUtility.exceptionToMock = dmle;
            System.assertEquals(true, TestingUtility.forceError, 'The forceError value in TestingUtility did not match the expected value.');
            System.assertEquals(dmle.getTypeName(), TestingUtility.exceptionToMock.getTypeName(), 'The Exception getTypeName() method did not return the expected value.');
            TestingUtility.mockTestError();
        } catch (DmlException dmlExcept) {
            System.assertEquals(dmle.getTypeName(), dmlExcept.getTypeName(), 'The getTypeName() method did not return the expected value.');
            System.assertEquals(dmle.getMessage(), dmlExcept.getMessage(), 'The getMessage() method did not return the expected value.');
        } catch (Exception e) {
            System.assertEquals(ge.getTypeName(), e.getTypeName(), 'The getTypeName() method did not return the expected value.');
            System.assertEquals(ge.getMessage(), e.getMessage(), 'The getMessage() method did not return the expected value.');
        }

        Test.stopTest();
    }
}
