/*
 * SetUtility.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
public with sharing class SetUtility {
    /**
     * The Difference between two sets, A and B, is to find the elements that exist in set A and not set B.
     *
     * @param setA The first set for use in the Difference.
     * @param setB The second set for use in the Difference.
     *
     * @return Set<Object> Returns a new set containing the elements from the first set that are not contained in
     * the second set.
     */
    public static Set<Object> difference(Set<Object> setA, Set<Object> setB) {
        Set<Object> difference = new Set<Object>();

        if (setA == null || setA.isEmpty()) {
            return difference;
        }

        if (setB == null || setB.isEmpty()) {
            return setA;
        }

        for (Object o : setA) {
            if (! setB.contains(o)) {
                difference.add(o);
            }
        }

        return difference;
    }

    /**
     * Finds the Intersection of two sets, A and B. The intersection of two sets is the elements that are common in
     * both sets.
     *
     * @param setA The first set for use in the Intersection.
     * @param setB The second set for use in the Intersection.
     *
     * @return Set<Object> Returns a new set containing the elements that are common both sets.
     */
    public static Set<Object> intersection(Set<Object> setA, Set<Object> setB) {
        Set<Object> union = new Set<Object>();

        if (setA == null || setA.isEmpty() || setB == null || setB.isEmpty()) {
            return union;
        }

        for (Object o : setA) {
            if (setB.contains(o)) {
                union.add(o);
            }
        }

        return union;
    }

    /**
     * Return True if two sets have a null intersection.
     *
     * @param setA
     * @param setB
     *
     * @return Boolean Return True if two sets have a null intersection, False otherwise.
     */
    public static Boolean isDisjoint(Set<Object> setA, Set<Object> setB) {
        return intersection(setA, setB) == new Set<Object>();
    }

    /**
     * Return True if the first set contains all the elements in the second set.
     *
     * @param setA The Set being checked to determine if Set B is a subset.
     * @param setB The Set being checked against Set A to determine if it is a subset.
     *
     * @return Boolean Return True if Set B is a Subset of Set A; False otherwise.
     */
    public static Boolean isSubset(Set<Object> setA, Set<Object> setB) {
        Boolean status = true;

        if (setA == null || setB == null) {
            status = false;
        } else if (setA.isEmpty() && setB.isEmpty()) {
            status = true;
        } else {
            status = setA.containsAll(setB);
        }

        return status;
    }

    /**
     * Return True if the first set contains all the elements of the second set. In other words, is Set B a subset of
     * Set A.
     *
     * @param setA
     * @param setB
     *
     * @return Boolean Returns True if Set A contains all the elements in Set B; False otherwise.
     */
    public static Boolean isSuperset(Set<Object> setA, Set<Object> setB) {
        return isSubset(setA, setB);
    }

    /**
     * Randomly selects a single value from the provided Set.
     *
     * @param Set<Object> The set to use to select a random value.
     *
     * @return Object A random item selected from the members of the provided set.
     */
    public static Object random(Set<Object> objSet) {
        Set<Object> newSet = random(objSet, 1);

        Object value;
        if (newSet == null) {
            value = null;
        } else if (newSet.isEmpty()) {
            value = newSet;
        } else {
            value = (new List<Object>(newSet))[0];
        }

        return value;
    }

    /**
     * Randomly selects the specified number of values from the provided Set.
     *
     * @param Set<Object> objSet The set to use to select random values.
     * @param num The number of random values to select from the provided Set.
     *
     * @return Set<Object> Returns null if the provided set is null or empty, if the number of items to select is null,
     * or if the num of items to select is negative. Otherwise, returns a new Set of randomly-selected values.
     */
    public static Set<Object> random(Set<Object> objSet, Integer num) {
        if (objSet == null || num == null || num < 0) {
            return null;
        }

        Set<Object> newSet = new Set<Object>();
        if (objSet.isEmpty() || num == 0) {
            return newSet;
        }

        if (num > objSet.size()) {
            newSet.addAll(objSet);
            return newSet;
        }

        List<Object> objs = new List<Object>(objSet);
        Integer index;
        do {
            index = IntegerUtility.random(0, objs.size() - 1);

            newSet.add(objs[index]);
        } while (newSet.size() < num);

        return newSet;
    }

    /**
     * Finds the Symmetric Difference of two sets, A and B. The Symmetric Difference is the set of elements from both
     * sets minus the elements that are common to both.
     *
     * @param setA The first set for use in the Symmetric Difference.
     * @param setB The second set for use in the Symmetric Difference.
     *
     * @return Set<Object> Returns a new Set combining the elements from both sets minus the elements that
     * exist in both.
     */
    public static Set<Object> symmetricDifference(Set<Object> setA, Set<Object> setB) {
        Set<Object> sd = new Set<Object>();

        Boolean isFirstGood = setA == null || setA.isEmpty() ? false : true;
        Boolean isSecondGood = setB == null || setB.isEmpty() ? false : true;

        if ((! isFirstGood) && (! isSecondGood)) {
            return sd;
        } else if (isFirstGood && (! isSecondGood)) {
            return setA;
        } else if ((! isFirstGood) && isSecondGood) {
            return setB;
        }

        Set<Object> superSet = union(setA, setB);

        for (Object o : superSet) {
            if (setA.contains(o) && setB.contains(o)) {
                continue;
            }

            sd.add(o);
        }

        return sd;
    }

    /**
     * Finds the Union (combination) of two sets.
     *
     * @param setA The first set for use in the Union.
     * @param setB The second set for use in the Union.
     *
     * @return Set<Object> Returns a new set containing the elements from both supplied sets.
     */
    public static Set<Object> union(Set<Object> setA, Set<Object> setB) {
        Set<Object> union = new Set<Object>();

        if (setA != null) {
            union.addAll(setA);
        }

        if (setB != null) {
            union.addAll(setB);
        }

        return union;
    }
}
