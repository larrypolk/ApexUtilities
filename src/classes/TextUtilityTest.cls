/* 
 * TextUtilityTest.cls
 * 
 * Author: Larry Polk
 * 
 * Copyright 2018. See the file "LICENSE" for the full license governing this code. 
 */
@IsTest
public class TextUtilityTest {
    @TestSetup
    public static void setup() {
        // Setup code
    }

    @IsTest
    public static void testMakeSentence() {
        Test.startTest();

        // Test without leading phrase.
        String actual = TextUtility.makeSentence(false);
        String[] parts = actual.split(' ');

        System.assert(String.isNotBlank(actual));
        System.assert(parts.size() >= TextUtility.MIN_SENTENCE_WORDS && parts.size() <= TextUtility.MAX_SENTENCE_WORDS);

        // Test with leading phrase.
        actual = TextUtility.makeSentence(true);
        parts = actual.split(' ');
        System.assert(String.isNotBlank(actual));

        System.assert(actual.startsWithIgnoreCase(TextUtility.loremStart));
        System.assert(parts.size() >= TextUtility.MIN_SENTENCE_WORDS && parts.size() <= TextUtility.MAX_SENTENCE_WORDS);

        Test.stopTest();
    }
}
