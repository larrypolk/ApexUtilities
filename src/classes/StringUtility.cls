/* 
 * StringUtility.cls
 * 
 * Author: Larry Polk
 * 
 * Copyright 2018. See the file "LICENSE" for the full license governing this code. 
 */
public with sharing class StringUtility {
    public static final String ASCII_LOWER = 'abcdefghijklmnopqrstuvwxyz';
    public static final String ASCII_UPPER = ASCII_LOWER.toUpperCase();
    public static final String ASCII_LETTERS = ASCII_LOWER + ASCII_UPPER;
    public static final String PUNCTUATION = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~';
    public static final String DIGITS = '0123456789';
    public static final String HEX_DIGITS = '0123456789ABCDEF';
    public static final String EMPTY_STRING = '';
    public static final String EOL_WIN = '\r\n';
    public static final String EOL_UNIX = '\n';

    /**
     * Creates and returns a randomly-generated string of the specified length.
     *
     * @param len The length of the randomly-generated string.
     *
     * @return String A randomly-generated string of random uppercase, lowercase, and number characters.
     */
    public static String random(Integer len) {
        return random(len, true, true, true);
    }

    /**
     * Creates a randomly-generated string of characters of the specified length.
     *
     * @param len Specifies the length of the randomly-generated string.
     * @param useNumbers Specifies if the randomly-generated string should contain number characters (digits).
     * @param useLowercaseLetters Specifies if the randomly-generated string should contain lowercase characters.
     * @param useUppercaseLetters Specifies if the randomly-generated string should contain uppercase characters.
     *
     * @return A string of randomly-generated characters of the specified length.
     */
    public static String random(Integer len, Boolean useNumbers, Boolean useLowercaseLetters, Boolean useUppercaseLetters) {
        String characterPool = EMPTY_STRING;

        if (useLowercaseLetters == true) {
            characterPool += ASCII_LOWER;
        }

        if (useNumbers == true) {
            characterPool += DIGITS;
        }

        if (useUppercaseLetters == true) {
            characterPool += ASCII_UPPER;
        }

        return random(len, characterPool);
    }

    /**
     * Creates a randomly-generated string of characters, from the supplied string, of the specified length.
     *
     * @param len Specifies the length of the randomly-generated string.
     * @param dictionary Specifies the characters to use when generating the string.
     *
     * @return A string of randomly-generated characters of the specified length.
     */
    public static String random(Integer len, String dictionary) {
        if (len == null || len < 1 || String.isBlank(dictionary)) {
            return EMPTY_STRING;
        }

        String[] pool = dictionary.split('');
        String[] characters = new String[] {};
        Integer idx;
        while (characters.size() < len) {
            idx = Math.mod(Math.abs(Crypto.getRandomInteger()), dictionary.length());
            characters.add(pool[idx]);    // characters.add(dictionary.substring(idx, idx + 1));
        }

        return String.join(characters, EMPTY_STRING);
    }
}
