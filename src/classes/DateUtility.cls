/*
 * DateUtility.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
public with sharing class DateUtility {
    // Constants
    public static final Integer MIN_YEAR_VALUE = 1;
    public static final Integer MAX_YEAR_VALUE = 9999;
    public static final Integer MIN_MONTH_VALUE = 1;
    public static final Integer MAX_MONTH_VALUE = 12;
    public static final String MASK_DAY_NAME = 'EEEEEEEEE';
    public static final String MASK_DAY_OF_WEEK = 'u';

    //
    // Current Month
    //

    /**
     * Finds the first day of the month based on the Date argument provided.
     *
     * @param sourceDate The Date to use to find the first day of the month.
     *
     * @return Date A new Date representing the first day of the month.
     */
    public static Date getFirstDayOfMonth(Date sourceDate) {
        if (sourceDate == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return sourceDate.toStartOfMonth();
    }

    /**
     * Finds the first day of the month based on the Datetime argument provided.
     *
     * @param sourceDate The Datetime to use to find the first day of the month.
     *
     * @return Datetime A new Datetime representing the first day of the month.
     */
    public static Datetime getFirstDayOfMonth(Datetime sourceDate) {
        if (sourceDate == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return Datetime.newInstance(getFirstDayOfMonth(sourceDate.date()), sourceDate.time());
    }

    /**
     * Finds the last day of the month based on the date argument provided.
     *
     * @param sourceDate The date to use to find the last day of the month.
     *
     * @return Date A new Date representing the last day of the month.
     */
    public static Date getLastDayOfMonth(Date sourceDate) {
        if (sourceDate == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return sourceDate.toStartOfMonth().addMonths(1).addDays(-1);
    }

    /**
     * Finds the last day of the month based on the Datetime argument provided.
     *
     * @param sourceDate The Datetime to use to find the last day of the month.
     *
     * @return Datetime A new Datetime representing the last day of the month.
     */
    public static Datetime getLastDayOfMonth(Datetime sourceDate) {
        if (sourceDate == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return Datetime.newInstance(getLastDayOfMonth(sourceDate.date()), sourceDate.time());
    }

    //
    // Next Month
    //

    /**
     * Finds the last day of the next month based on the date argument provided.
     *
     * @param sourceDate The date to use to find the last day of the next month.
     *
     * @return Date A new Date representing the last day of the next month.
     */
    public static Date getLastDayOfNextMonth(Date sourceDate) {
        if (sourceDate == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return sourceDate.toStartOfMonth().addMonths(2).addDays(-1);
    }

    /**
     * Finds the last day of the previous month based on the Datetime argument provided.
     *
     * @param sourceDate The Datetime to use to find the last day of the next month.
     *
     * @return Datetime A new Datetime representing the last day of the next month.
     */
    public static Datetime getLastDayofNextMonth(Datetime sourceDate) {
        if (sourceDate == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return Datetime.newInstance(getLastDayOfNextMonth(sourceDate.date()), sourceDate.time());
    }

    /**
     * Finds the first day of the following month based on the date argument provided.
     *
     * @param sourceDate The date to use to find the last day of the following month.
     *
     * @return Date A new date representing the first day of the following month.
     */
    public static Date getFirstDayOfNextMonth(Date sourceDate) {
        if (sourceDate == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return sourceDate.toStartOfMonth().addMonths(1);
    }

    /**
     * Finds the first day of the following month based on the Datetime argument provided.
     *
     * @param sourceDate The Datetime to use to find the first day of the following month.
     *
     * @return Datetime A new Datetime representing the first day of the following month.
     */
    public static Datetime getFirstDayOfNextMonth(Datetime sourceDate) {
        if (sourceDate == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return Datetime.newInstance(getFirstDayOfNextMonth(sourceDate.date()), sourceDate.time());
    }

    //
    // Previous Month
    //

    /**
     * Finds the first day of the previous month based on the date argument provided.
     *
     * @param sourceDate The date to use to find the first day of the previous month.
     *
     * @return Date A new date representing the first day of the previous month.
     */
    public static Date getFirstDayOfPreviousMonth(Date sourceDate) {
        if (sourceDate == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return sourceDate.toStartOfMonth().addMonths(-1);
    }

    /**
     * Finds the first day of the previous month based on the datetime argument provided.
     *
     * @param sourceDate The datetime to use to find the first day of the previous month.
     *
     * @return Datetime A new datetime representing the first day of the previous month.
     */
    public static Datetime getFirstDayOfPreviousMonth(Datetime sourceDate) {
        if (sourceDate == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return Datetime.newInstance(getFirstDayOfPreviousMonth(sourceDate.date()), sourceDate.time());
    }

    /**
     * Finds the last day of the previous month based on the date argument provided.
     *
     * @param sourceDate The date to use to find the last day of the previous month.
     *
     * @return Date A new date representing the last day of the previous month.
     */
    public static Date getLastDayOfPreviousMonth(Date sourceDate) {
        if (sourceDate == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return sourceDate.toStartOfMonth().addDays(-1);
    }

    /**
     * Finds the last day of the previous month based on the datetime argument provided.
     *
     * @param sourceDate The datetime to use to find the last day of the previous month.
     *
     * @return Datetime A new datetime representing the last day of the previous month.
     */
    public static Datetime getLastDayOfPreviousMonth(Datetime sourceDate) {
        if (sourceDate == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return Datetime.newInstance(getLastDayOfPreviousMonth(sourceDate.date()), sourceDate.time());
    }

    //
    // Date subtraction operations
    //

    /**
     * Subtracts the number of years from the supplied date.
     *
     * @param sourceDate The date to use to subtract the number of years.
     * @param yearsToSubtract The number of years to subtract from the supplied date.
     *
     * @return Date The new date created from subtracting the number of years from the supplied date.
     */
    public static Date subtractYears(Date sourceDate, Integer yearsToSubtract) {
        if (sourceDate == null || yearsToSubtract == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        Integer years = yearsToSubtract < 0 ? yearsToSubtract : (-1 * yearsToSubtract);

        return sourceDate.addYears(years);
    }

    /**
     * Subtracts the number of years from the supplied datetime.
     *
     * @param sourceDate The datetime to use to subtract the number of years.
     * @param yearsToSubtract The number of years to subtract from the supplied datetime.
     *
     * @return Datetime The new datetime created from subtracting the number of years from the supplied datetime.
     */
    public static Datetime subtractYears(Datetime sourceDate, Integer yearsToSubtract) {
        if (sourceDate == null || yearsToSubtract == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        Integer years = yearsToSubtract < 0 ? yearsToSubtract : (-1 * yearsToSubtract);

        return Datetime.newInstance(subtractYears(sourceDate.date(), years), sourceDate.time());
    }

    /**
     * Subtracts the number of months from the supplied date.
     *
     * @param sourceDate The date to use to subtract the number of months.
     * @param monthsToSubtract The number of months to subtract from the supplied date.
     *
     * @return Date The new date created from subtracting the number of months from the supplied date.
     */
    public static Date subtractMonths(Date sourceDate, Integer monthsToSubtract) {
        if (sourceDate == null || monthsToSubtract == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        Integer months = monthsToSubtract < 0 ? monthsToSubtract : (-1 * monthsToSubtract);

        return sourceDate.addMonths(months);
    }

    /**
     * Subtracts the number of months from the supplied datetime.
     *
     * @param sourceDate The datetime to use to subtract the number of months.
     * @param monthsToSubtract The number of months to subtract from the supplied date.
     *
     * @return Datetime The new datetime created from subtracting the number of months from the supplied datetime.
     */
    public static Datetime subtractMonths(Datetime sourceDate, Integer monthsToSubtract) {
        if (sourceDate == null || monthsToSubtract == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        Integer months = monthsToSubtract < 0 ? monthsToSubtract : (-1 * monthsToSubtract);

        return Datetime.newInstance(subtractMonths(sourceDate.date(), months), sourceDate.time());
    }

    /**
     * Subtracts the number of days from the supplied date.
     *
     * @param sourceDate The date to use to subtract the number of days.
     * @param daysToSubtract The number of days to subtract from the supplied date.
     *
     * @return Date The new date created from subtracting the number of days from the supplied date.
     */
    public static Date subtractDays(Date sourceDate, Integer daysToSubtract) {
        if (sourceDate == null || daysToSubtract == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        Integer days = daysToSubtract < 0 ? daysToSubtract : (-1 * daysToSubtract);

        return sourceDate.addDays(days);
    }

    /**
     * Subtracts the number of days from the supplied datetime.
     *
     * @param sourceDate The datetime to use to subtract the number of days.
     * @param daysToSubtract The number of days to subtract from the supplied datetime.
     *
     * @return Datetime The new datetime created from subtracting the number of days from the supplied datetime.
     */
    public static Datetime subtractDays(Datetime sourceDate, Integer daysToSubtract) {
        if (sourceDate == null || daysToSubtract == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        Integer days = daysToSubtract < 0 ? daysToSubtract : (-1 * daysToSubtract);

        return Datetime.newInstance(subtractDays(sourceDate.date(), days), sourceDate.time());
    }

    //
    // Days of month operations
    //

    /**
     * Finds and returns the first date of the month corresponding to the supplied day of the week name.
     *
     * @param sourceDate The Date to use when finding the first instance fo the supplied day name in the month.
     * @param dayName The name of the day to find.
     *
     * @return Date The date corresponding to the first instance of the supplied day name in the month; Null otherwise.
     */
    public static Date getFirstInstanceOfDayInMonth(Date sourceDate, String dayName) {
        if (sourceDate == null || String.isBlank(dayName)) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        Map<String, Date[]> days = getDaysOfMonthByDayOfWeek(sourceDate);
        Date[] values = days.containsKey(dayName) ? days.get(dayName) : new Date[]{};

        return values.isEmpty() ? null : values[0];
    }

    /**
     * Finds and returns the first date of the month corresponding to the supplied day of the week name.
     *
     * @param sourceDate The Date to use when finding the first instance fo the supplied day name in the month.
     * @param dayName The name of the day to find.
     *
     * @return Date The date corresponding to the first instance of the supplied day name in the month; Null otherwise.
     */
    public static Datetime getFirstInstanceOfDayInMonth(Datetime sourceDate, String dayName) {
        if (sourceDate == null || String.isBlank(dayName)) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return Datetime.newInstance(getFirstInstanceOfDayInMonth(sourceDate.date(), dayName), sourceDate.time());
    }

    /**
     * Creates a collection of Dates for the month based on the provided date, grouped by day of the week name.
     *
     * @param sourceDate The Date to use for calculating the Day of the Week collections.
     *
     * @return Map<String, List<Date>> A map of Date lists, keyed on day of the week name.
     */
    public static Map<String, Date[]> getDaysOfMonthByDayOfWeek(Date sourceDate) {
        if (sourceDate == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        Map<String, Date[]> days = new Map<String, Date[]> {};

        Date startOfMonth = sourceDate.toStartOfMonth();
        Date endOfMonth = startOfMonth.addMonths(1).addDays(-1);

        Date workingDate = sourceDate.toStartOfMonth();
        String name = null;
        do {
            name = getDayOfWeekName(Datetime.newInstance(workingDate, Time.newInstance(0, 0, 0, 0)));

            if (!days.containsKey(name)) {
                days.put(name, new Date[]{});
            }

            days.get(name).add(workingDate);

            workingDate = workingDate.addDays(1);
        } while (workingDate <= endOfMonth);

        return days;
    }

    /**
     * Creates a collection of Dates for the month based on the provided month/day combination, grouped by day of
     * the week name.
     *
     * @param month The integer value representing the desired month.
     * @param year The integer value representing the desired year.
     *
     * @return Map<String, List<Date>> A map of Date lists, keyed on the day of the week name.
     */
    public static Map<String, Date[]> getDaysOfMonthByDayOfWeek(Integer month, Integer year) {
        if (year < MIN_YEAR_VALUE || year > MAX_YEAR_VALUE || month < MIN_MONTH_VALUE || month > MAX_MONTH_VALUE) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return getDaysOfMonthByDayOfWeek(Date.newInstance(year, month, 1));
    }

    /**
     * Determines which day of the week, as a String, the Date falls upon.
     *
     * @param d Date The Date value to use when determining the day of the week name.
     *
     * @return String A string value representing the name of the day in the week.
     */
    public static String getDayOfWeekName(Date d) {
        if (d == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return getDayOfWeekName(Datetime.newInstance(d, Time.newInstance(0, 0, 0, 0)));
    }

    /**
     * Determines which day of the week, as an String, the Datetime falls upon.
     *
     * @param dt DateTime The DateTime value to use when extracting the day of the week name.
     *
     * @return String A string value representing the name of the day in the week.
     */
    public static String getDayOfWeekName(Datetime dt) {
        if (dt == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return dt.format(MASK_DAY_NAME);
    }

    public static String getDayOfWeekNumber(Date d) {
        if (d == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return getDayOfWeekNumber(Datetime.newInstance(d, Time.newInstance(0, 0, 0, 0)));
    }

    /**
     * Determines which day of the week, as an integer, the DateTime falls upon.
     *
     * @param dt Datetime The DateTime value to use when extracting the day of the week integer.
     *
     * @return String A string value representing the position of the day in the week. Values will be between
     * '1' (Monday) and '7' (Sunday).
     */
    public static String getDayOfWeekNumber(Datetime dt) {
        // Monday = 1, Tuesday = 2, Wednesday = 3, Thursday = 4, Friday = 5, Saturday = 6, Sunday = 7
        if (dt == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return dt.format(MASK_DAY_OF_WEEK);
    }

    /**
     * Determines if the provided datetime is a weekday.
     *
     * @param dt The day to check to determine if it is a weekday (Monday, Tuesday, Wednesday, Thursday, Friday).
     *
     * @return Boolean Returns true if the provided date is a weekday (Monday - Friday), false otherwise.
     */
    public static Boolean isWeekday(Date d) {
        if (d == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        Datetime dt = Datetime.newInstance(d, Time.newInstance(0, 0, 0, 0));
        return isWeekday(dt);
    }

    /**
     * Determines if the provided datetime is a weekday.
     *
     * @param dt The day to check to determine if it is a weekday (Monday, Tuesday, Wednesday, Thursday, Friday).
     *
     * @return Boolean Returns true if the provided date is a weekday (Monday - Friday), false otherwise.
     */
    public static Boolean isWeekday(Datetime dt) {
        String dayOfWeekNumber = getDayOfWeekNumber(dt);
        Set<String> weekdayNums = new Set<String>{ '1', '2', '3', '4', '5' };

        return weekdayNums.contains(dayOfWeekNumber);
    }

    /**
     * Determines if the provided date is a weekend (Saturday/Sunday).
     *
     * @param d Date The day to check to determine if it is a Saturday or Sunday.
     *
     * @return Boolean Returns true if the provided date falls on a weekend (Saturday or Sunday), false otherwise.
     */
    public static Boolean isWeekend(Date d) {
        if (d == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        Datetime dt = Datetime.newInstance(d, Time.newInstance(0, 0, 0, 0));
        return isWeekend(dt);
    }

    /**
     * Determines if the provided datetime is a weekend.
     *
     * @param dt Datetime The day to check to determine if it is a Saturday or Sunday.
     *
     * @return Boolean Returns true if the provided datetime falls on a weekend (Saturday or Sunday), false otherwise.
     */
    public static Boolean isWeekend(Datetime dt) {
        String dayOfWeekNumber = getDayOfWeekNumber(dt);
        Set<String> weekendDayNums = new Set<String>{ '6', '7' };

        return weekendDayNums.contains(dayOfWeekNumber);
    }

    /**
     * Determines if the provided Date is a Monday.
     *
     * @param d Date The day to check to determine if it is a Monday.
     *
     * @return Boolean Returns true if the provided date falls on a Monday, false otherwise.
     */
    public static Boolean isMonday(Date d) {
        return isMonday(Datetime.newInstance(d, Time.newInstance(0, 0, 0, 0)));
    }

    /**
     * Determines if the provided Datetime is a Monday.
     *
     * @param dt Datetime The day to check to determine if it is a Monday.
     *
     * @return Boolean Returns true if the provided date falls on a Monday, false otherwise.
     */
    public static Boolean isMonday(Datetime dt) {
        return '1'.equalsIgnoreCase(getDayOfWeekNumber(dt));
    }

    /**
     * Determines if the provided Date is a Tuesday.
     *
     * @param d Date The day to check to determine if it is a Tuesday.
     *
     * @return Boolean Returns true if the provided date falls on a Tuesday, false otherwise.
     */
    public static Boolean isTuesday(Date d) {
        return isTuesday(Datetime.newInstance(d, Time.newInstance(0, 0, 0, 0)));
    }

    /**
     * Determines if the provided Datetime is a Tuesday.
     *
     * @param dt Datetime The day to check to determine if it is a Tuesday.
     *
     * @return Boolean Returns true if the provided date falls on a Tuesday, false otherwise.
     */
    public static Boolean isTuesday(Datetime dt) {
        return '2'.equalsIgnoreCase(getDayOfWeekNumber(dt));
    }

    /**
     * Determines if the provided Date is a Wednesday.
     *
     * @param d Date The day to check to determine if it is a a Wednesday.
     *
     * @return Boolean Returns true if the date falls on a Wednesday, false otherwise.
     */
    public static Boolean isWednesday(Date d) {
        return isWednesday(Datetime.newInstance(d, Time.newInstance(0, 0, 0, 0)));
    }

    /**
     * Determines if the provided Datetime is a Wednesday.
     *
     * @param dt Datetime The day to check to determine if it is a Wednesday.
     *
     * @return Boolean Returns true if the provided Datetime falls on a Tuesday, false otherwise.
     */
    public static Boolean isWednesday(Datetime dt) {
        return '3'.equalsIgnoreCase(getDayOfWeekNumber(dt));
    }

    /**
     * Determines if the provided Date is a Thursday.
     *
     * @param d Date The day to check to determine if it is a Thursday.
     *
     * @return Boolean Returns true if the provided date falls on a Thursday, false otherwise.
     */
    public static Boolean isThursday(Date d) {
        return isThursday(Datetime.newInstance(d, Time.newInstance(0, 0, 0, 0)));
    }

    /**
     * Determines if the provided Datetime is a Thursday.
     *
     * @param dt Datetime The day to check to determine if it is a Thursday.
     *
     * @return Boolean Returns true if the provided date falls on a Thursday, false otherwise.
     */
    public static Boolean isThursday(Datetime dt) {
        return '4'.equalsIgnoreCase(getDayOfWeekNumber(dt));
    }

    /**
     * Determines if the provided Date is a Friday.
     *
     * @param d Date The day to check to determine if it is a Friday.
     *
     * @return Boolean Returns true if the provided date falls on a Friday, false otherwise.
     */
    public static Boolean isFriday(Date d) {
        return isFriday(Datetime.newInstance(d, Time.newInstance(0, 0, 0, 0)));
    }

    /**
     * Determines if the provided Datetime is a Friday.
     *
     * @param dt Datetime The day to check to determine if it is a Friday.
     *
     * @return Boolean Returns true if the provided date falls on Friday, false otherwise.
     */
    public static Boolean isFriday(Datetime dt) {
        return '5'.equalsIgnoreCase(getDayOfWeekNumber(dt));
    }

    /**
     * Determines if the provided Date is a Saturday.
     *
     * @param d Date The day to check to determine if it is a Saturday.
     *
     * @return Boolean Returns true if the provided date falls on Saturday, false otherwise.
     */
    public static Boolean isSaturday(Date d) {
        return isSaturday(Datetime.newInstance(d, Time.newInstance(0, 0, 0, 0)));
    }

    /**
     * Determines if the provided Datetime is a Saturday.
     *
     * @param dt Datetime The day to check to determine if it is a Saturday.
     *
     * @return Boolean Returns true if the provided date falls on Saturday, false otherwise.
     */
    public static Boolean isSaturday(Datetime dt) {
        return '6'.equalsIgnoreCase(getDayOfWeekNumber(dt));
    }

    /**
     * Determines if the provided Date is a Sunday.
     *
     * @param d Date The day to check to determine if it is a Sunday.
     *
     * @return Boolean Returns true if the provided date falls on Sunday, false otherwise.
     */
    public static Boolean isSunday(Date d) {
        return isSunday(Datetime.newInstance(d, Time.newInstance(0, 0, 0, 0)));
    }

    /**
     * Determines if the provided Datetime is a Sunday.
     *
     * @param dt Datetime The day to check to determine if it is a Sunday.
     *
     * @return Boolean Returns true if the provided datetime falls on Sunday, false otherwise.
     */
    public static Boolean isSunday(Datetime dt) {
        return '7'.equalsIgnoreCase(getDayOfWeekNumber(dt));
    }

    /**
     * Generates a variety of details about the month for the provided Date.
     *
     * @param d Date The date to use when generating information about the month.
     *
     * @return MonthDetails A MonthDetails object containing a variety of information about the specified month.
     */
    public static MonthDetails getMonthDetails(Date d) {
        if (d == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return new MonthDetails(d);
    }

    /**
     * Generates a variety of details about the month for the provided Datetime.
     *
     * @param dt Datetime The datetime to use when generating information about the month.
     *
     * @return MonthDetails A MonthDetails object containing a variety of information about the specified month.
     */
    public static MonthDetails getMonthDetails(Datetime dt) {
        if (dt == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return new MonthDetails(dt);
    }

    /**
     * Generates a variety of details about the month for the provided Month and Year.
     *
     * @param month
     * @param year
     *
     * @return MonthDetails A MonthDetails object containing a variety of information about the specified month.
     */
    public static MonthDetails getMonthDetails(Integer month, Integer year) {
        if (month == null || month > MAX_MONTH_VALUE || month < MIN_MONTH_VALUE || year == null || year > MAX_YEAR_VALUE) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return new MonthDetails(Date.newInstance(year, month, 1));
    }

    //
    // Inner classes
    //

    public with sharing class MonthDetails {
        public Datetime firstOfMonth { get; private set; }
        public Datetime lastOfMonth { get; private set; }

        public Date[] mondays { get; private set; }
        public Date[] tuesdays { get; private set; }
        public Date[] wednesdays { get; private set; }
        public Date[] thursdays { get; private set; }
        public Date[] fridays { get; private set; }
        public Date[] saturdays { get; private set; }
        public Date[] sundays { get; private set; }
        public Date[] weekdays { get; private set; }
        public Date[] weekends { get; private set; }

        public Integer fiscalQuarter { get; private set; }
        public Integer fiscalYearStartMonth { get; private set; }

        public final String MASK_DAY_OF_WEEK = 'u';

        public MonthDetails(Date d) {
            this.setup(Datetime.newInstance(d, Time.newInstance(0, 0, 0, 0)));
        }

        public MonthDetails(Datetime dt) {
            this.setup(dt);
        }

        private void setup(Datetime dt) {
            this.firstOfMonth = Datetime.newInstance(dt.date().toStartOfMonth(), dt.time());
            this.lastOfMonth = Datetime.newInstance(dt.date().toStartOfMonth().addMonths(1).addDays(-1), dt.time());

            this.mondays = new Date[] {};
            this.tuesdays = new Date[] {};
            this.wednesdays = new Date[] {};
            this.thursdays = new Date[] {};
            this.fridays = new Date[] {};
            this.saturdays = new Date[] {};
            this.sundays = new Date[] {};

            this.weekdays = new Date[] {};
            this.weekends = new Date[] {};

            this.partitionDays();

            this.fiscalYearStartMonth = this.getOrganizationInfo().FiscalYearStartMonth;
            this.fiscalQuarter = this.getFiscalQuarter();
        }

        private void partitionDays() {
            Datetime dtPointer = this.firstOfMonth;
            String dayNum;
            do {
                dayNum = dtPointer.format(MASK_DAY_OF_WEEK);

                if ('1'.equalsIgnoreCase(dayNum)) {
                    this.mondays.add(dtPointer.date());
                } else if ('2'.equalsIgnoreCase(dayNum)) {
                    this.tuesdays.add(dtPointer.date());
                } else if ('3'.equalsIgnoreCase(dayNum)) {
                    this.wednesdays.add(dtPointer.date());
                } else if ('4'.equalsIgnoreCase(dayNum)) {
                    this.thursdays.add(dtPointer.date());
                } else if ('5'.equalsIgnoreCase(dayNum)) {
                    this.fridays.add(dtPointer.date());
                } else if ('6'.equalsIgnoreCase(dayNum)) {
                    this.saturdays.add(dtPointer.date());
                } else if ('7'.equalsIgnoreCase(dayNum)) {
                    this.sundays.add(dtPointer.date());
                }

                if ('6'.equalsIgnoreCase(dayNum) || '7'.equalsIgnoreCase(dayNum)) {
                    this.weekends.add(dtPointer.date());
                } else {
                    this.weekdays.add(dtPointer.date());
                }

                dtPointer = dtPointer.addDays(1);
            } while(dtPointer <= this.lastOfMonth);
        }

        @TestVisible
        private void setFiscalQuarterStartMonth(Integer fiscalYearStartMonth) {
            this.fiscalYearStartMonth = fiscalYearStartMonth;
            this.fiscalQuarter = this.getFiscalQuarter();
        }

        @TestVisible
        private Integer getFiscalQuarter() {
            Map<Integer, Integer> quarters = new Map<Integer, Integer> {
                1 => 1, 2 => 1, 3 => 1,  4 => 2,  5 => 2,  6 => 2,
                7 => 3, 8 => 3, 9 => 3, 10 => 4, 11 => 4, 12 => 4
            };

            if (this.fiscalYearStartMonth != 1) {
                Integer q1 = this.fiscalYearStartMonth;
                Set<Integer> q1Months = new Set<Integer> {
                    q1, (q1 + 1) > 12 ? (q1 + 1) - 12 : q1 + 1, (q1 + 2) > 12 ? (q1 + 2) - 12 : q1 + 2
                };

                Integer q2 = (q1 + 3) > 12 ? (q1 + 3) - 12 : q1 + 3;
                Set<Integer> q2Months = new Set<Integer> {
                    q2, (q2 + 1) > 12 ? (q2 + 1) - 12 : q2 + 1, (q2 + 2) > 12 ? (q2 + 2) - 12 : q2 + 2
                };

                Integer q3 = (q1 + 6) > 12 ? (q1 + 6) - 12 : q1 + 6;
                Set<Integer> q3Months = new Set<Integer> {
                    q3, (q3 + 1) > 12 ? (q3 + 1) - 12 : q3 + 1, (q3 + 2) > 12 ? (q3 + 2) - 12 : q3 + 2
                };

                Integer q4 = (q1 + 9) > 12 ? (q1 + 9) - 12 : q1 + 9;
                Set<Integer> q4Months = new Set<Integer> {
                    q4, (q4 + 1) > 12 ? (q4 + 1) - 12 : q4 + 1, (q4 + 2) > 12 ? (q4 + 2) - 12 : q4 + 2
                };

                Integer quarter;
                for (Integer k : quarters.keySet()) {
                    if (q1Months.contains(k)) {
                        quarter = 1;
                    } else if (q2Months.contains(k)) {
                        quarter = 2;
                    } else if (q3Months.contains(k)) {
                        quarter = 3;
                    } else if (q4Months.contains(k)) {
                        quarter = 4;
                    }

                    quarters.put(k, quarter);
                }
            }

            return quarters.get(this.firstOfMonth.date().month());
        }

        @TestVisible
        private Organization getOrganizationInfo() {
            Organization orgDetails = [
                SELECT FiscalYearStartMonth
                  FROM Organization
                 WHERE Id = :UserInfo.getOrganizationId()
                 LIMIT 1
            ];

            return orgDetails;
        }
    }
}
