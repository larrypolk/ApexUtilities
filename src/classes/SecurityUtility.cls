/*
 * SecurityUtility.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
public with sharing class SecurityUtility {
    /**
     * Sanitizes a URL string to prevent Open Redirect attacks by making the URL string relative to the Org's root.
     *
     * @param url String A string representing a URL in the user's Org.
     *
     * @return String A sanitized URL string.
     */
    public static String getSanitizedUrlString(String url) {
        if (String.isBlank(url)) {
            return url;
        }

        if (url.startsWith('/')) {
            url = url.replaceFirst('/+', '');    // Removes one or more leading slashes if they exist.
        }

        return '/' + url;    // Re-adds a leading slash to make the URL relative to the root.
    }

    /**
     * Sanitizes a URL string to prevent Open Redirect attacks by creating a PageReference from a sanitized URL
     * string.
     *
     * @param url String A string representing a URL in the user's Org.
     *
     * @return PageReference A PageReference created from a sanitized URL string.
     */
    public static PageReference getSanitizedPageReference(String url) {
        return new PageReference(getSanitizedUrlString(url));
    }
}
