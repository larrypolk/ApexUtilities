/*
 * DataFactoryTest.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
@IsTest
public with sharing class DataFactoryTest {
    @IsTest
    public static void testGenerateRandomHexColorString() {
        Set<String> validCharacters = new Set<String>(StringUtility.HEX_DIGITS.split(''));

        // Test string with no hash symbol
        Boolean includeHashSymbol = false;
        Integer expLen = 6;
        String actual = DataFactory.hexColor(includeHashSymbol);

        // Make sure the value returned only contains valid characters for a hex color string.
        System.assertEquals(expLen, actual.length());
        for (String s : actual.split('')) {
            System.assert(validCharacters.contains(s));
        }

        // Test string with hash symbol
        includeHashSymbol = true;
        expLen = 7;
        actual = DataFactory.hexColor(includeHashSymbol);

        // Make sure the value returned only contains valid characters for a hex color string.
        validCharacters.add('#');    // Add hash symbol to set of valid characters.

        System.assertEquals(expLen, actual.length());

        System.assert(actual.startsWith('#'));

        for (String s : actual.split('')) {
            System.assert(validCharacters.contains(s));
        }
    }

    @IsTest
    public static void testGenerateFakeSSN() {
        Integer expLength = 10;

        // Test with bad Booleans
        Boolean allowInvalid = null;
        Boolean flag = null;
        String actual = DataFactory.socialSecurityNumber(allowInvalid, flag);
        System.assert(actual.length() == expLength);

        Set<String> numbers = new Set<String>(StringUtility.DIGITS.split(''));
        for (String s : actual.split('')) {
            System.assert(numbers.contains(s));
        }

        // Test with good Booleans
        allowInvalid = false;
        flag = true;
        expLength = 12;

        actual = DataFactory.socialSecurityNumber(allowInvalid, flag);
        System.assert(actual.length() == expLength);

        numbers.add('-');
        for (String s : actual.split('')) {
            System.assert(numbers.contains(s));
        }
    }

    @IsTest
    public static void testGenerateFakePhoneNumber() {
        // Test without using fake prefix
        Boolean useFakePrefix = false;
        String actual = DataFactory.phoneNumber(useFakePrefix);
        Integer expLen = 14;

        System.assertEquals(expLen, actual.length());

        // Test with fake prefix
        useFakePrefix = true;
        actual = DataFactory.phoneNumber(useFakePrefix);
        expLen = 14;

        System.assertEquals(expLen, actual.length());
        System.assert(actual.contains(' 555-'));
    }

    @IsTest
    public static void testGenerateFakeCreditCardNumber() {
        // Test without using dashes
        Boolean useDashes = false;
        Integer expLen = 16;
        Set<String> expected = new Set<String>(StringUtility.DIGITS.split(''));
        String actual = DataFactory.creditCardNumber(useDashes);

        System.assertEquals(expLen, actual.length());

        for (String s : actual.split('')) {
            System.assert(expected.contains(s));
        }

        // Test with dashes
        useDashes = true;
        expLen = 19;
        actual = DataFactory.creditCardNumber(useDashes);
        Integer expDashes = 3;
        expected.add('-');

        System.assertEquals(expLen, actual.length());

        System.assertEquals(expDashes, actual.countMatches('-'));

        for (String s : actual.split('')) {
            System.assert(expected.contains(s));
        }
    }

    @IsTest
    public static void testGenerateRandomIPAddress() {
        // Test IPv4 generation
        String actual = DataFactory.ip();
        String[] parts = actual.split('\\' + DataFactory.FORMAT_SEPARATOR_IPV4);

        Integer expLength = 4;
        System.assertEquals(expLength, parts.size(), parts);

        Integer value;
        for (String part : parts) {
            value = Integer.valueOf(part);
            System.assert(0 <= value && value <= 255);
        }

        // Test IPv6 generation
        actual = DataFactory.ip(true);
        parts = actual.split('\\' + DataFactory.FORMAT_SEPARATOR_IPV6);

        expLength = 8;
        System.assertEquals(expLength, parts.size());

        Set<String> validChars = new Set<String>(StringUtility.HEX_DIGITS.split(''));
        String[] chars;
        for (String part : parts) {
            chars = part.split('');
            for (String c : chars) {
                System.assert(validChars.contains(c));
            }
        }
    }

    @IsTest
    public static void testGenerateFakeMACAddress() {
        String actual = DataFactory.macAddress();
        String[] parts = actual.split('\\' + DataFactory.FORMAT_SEPARATOR_MAC);

        Integer expLength = 6;
        System.assertEquals(expLength, parts.size());

        Set<String> validChars = new Set<String>(StringUtility.HEX_DIGITS.split(''));
        String[] chars;
        for (String part : parts) {
            chars = part.split('');
            for (String c : chars) {
                System.assert(validChars.contains(c));
            }
        }
    }

    @IsTest
    public static void testGenerateFemaleFirstName() {
        Set<String> expected = Data.femaleFirstNames;
        String actual = DataFactory.firstNameFemale();

        System.assert(!String.isBlank(actual));
        System.assert(expected.contains(actual));
    }

    @IsTest
    public static void testGenerateMaleFirstName() {
        Set<String> expected = Data.maleFirstNames;
        String actual = DataFactory.firstNameMale();

        System.assert(!String.isBlank(actual));
        System.assert(expected.contains(actual));
    }

    @IsTest
    public static void testGenerateLastName() {
        Set<String> expected = Data.lastNames;
        String actual = DataFactory.lastName();

        System.assert(!String.isBlank(actual));
        System.assert(expected.contains(actual));
    }

    @IsTest
    public static void testGenerateRandomFullNameMale() {
        Boolean maleName = true;
        testFullNameGeneration(maleName);    // Test male name generation
    }

    @IsTest
    public static void testGenerateRandomFullNameFemale() {
        Boolean maleName = false;
        testFullNameGeneration(maleName);    // Test female name generation
    }

    @IsTest
    public static void testCityPrefix() {
        String actual = DataFactory.cityPrefix();
    }

    //
    // Helper methods
    //

    public static void testFullNameGeneration(Boolean maleName) {
        Map<Integer, String> formats = DataFactory.FORMAT_FULL_NAME;

        for (Integer i = 0; i < 20; i++) {
            String actual = DataFactory.fullName(maleName);

            System.assert(!String.isBlank(actual));
            System.assert(actual.length() > 0);

            String[] parts = actual.split(' ');

            System.assert(parts.size() > 0);

            Set<String> prefixes = new Set<String>(Data.genderlessPrefixes);
            prefixes.addAll(maleName == true ? Data.malePrefixes : Data.femalePrefixes);

            Set<String> firstNames = new Set<String>(maleName == true ? Data.maleFirstNames : Data.femaleFirstNames);

            Set<String> suffixes = new Set<String>(Data.genderlessSuffixes);
            suffixes.addAll(maleName == true ? Data.maleSuffixes : new Set<String>());

            if (parts.size() == 2) {
                if (parts[1].contains('-')) {
                    // FirstName LastName-LastName
                    String[] lastNames = parts[1].split('-');

                    System.assert(firstNames.contains(parts[0]), parts);                 // Check first name
                    System.assert(Data.lastNames.contains(lastNames[0]), lastNames);     // Check pre-hyphen Surname
                    System.assert(Data.lastNames.contains(lastNames[1]), lastNames);     // Check post-hyphen Surname
                } else {
                    // FirstName LastName
                    System.assert(firstNames.contains(parts[0]), parts);        // Check first name
                    System.assert(Data.lastNames.contains(parts[1]), parts);    // Check Surname
                }
            } else if (parts.size() == 3) {
                if (parts[1].contains('-')) {
                    // FirstName LastName-LastName Suffix
                    String[] lastNames = parts[1].split('-');

                    System.assert(firstNames.contains(parts[0]), parts);                 // Check first name
                    System.assert(Data.lastNames.contains(lastNames[0]), lastNames);     // Check pre-hyphen Surname
                    System.assert(Data.lastNames.contains(lastNames[1]), lastNames);     // Check post-hyphen Surname
                    System.assert(suffixes.contains(parts[2]), parts);                   // Check suffix
                } else if (parts[2].contains('-')) {
                    // Prefix FirstName LastName-LastName
                    String[] lastNames = parts[2].split('-');

                    System.assert(prefixes.contains(parts[0]), parts);               // Check prefix
                    System.assert(firstNames.contains(parts[1]), parts);             // Check first name
                    System.assert(Data.lastNames.contains(lastNames[0]), parts);     // Check pre-hyphen Surname
                    System.assert(Data.lastNames.contains(lastNames[1]), parts);     // Check post-hyphen Surname
                } else {
                    System.debug(actual);
                    if (prefixes.contains(parts[0])) {
                        // Prefix FirstName LastName
                        System.assert(prefixes.contains(parts[0]), parts);          // Check prefix
                        System.assert(firstNames.contains(parts[1]), parts);        // Check first name
                        System.assert(Data.lastNames.contains(parts[2]), parts);    // Check Surname
                    } else {
                        // FirstName LastName Suffix
                        System.assert(firstNames.contains(parts[0]), parts);        // Check first name
                        System.assert(Data.lastNames.contains(parts[1]), parts);    // Check Surname
                        System.assert(suffixes.contains(parts[2]), parts);          // Check suffix
                    }
                }
            } else if (parts.size() == 4) {
                if (parts[2].contains('-')) {
                    // Prefix FirstName LastName-LastName Suffix
                    String[] lastNames = parts[2].split('-');

                    System.assert(prefixes.contains(parts[0]), parts);                  // Check prefix
                    System.assert(firstNames.contains(parts[1]), parts);                // Check first name
                    System.assert(Data.lastNames.contains(lastNames[0]), lastNames);    // Check pre-hyphen Surname
                    System.assert(Data.lastNames.contains(lastNames[1]), lastNames);    // Check post-hyphen Surname
                    System.assert(suffixes.contains(parts[3]), parts);                  // Check suffix
                } else {
                    // Prefix FirstName LastName Suffix
                    System.assert(prefixes.contains(parts[0]), parts);          // Check prefix
                    System.assert(firstNames.contains(parts[1]), parts);        // Check first name
                    System.assert(Data.lastNames.contains(parts[2]), parts);    // Check pre-hyphen Surname
                    System.assert(suffixes.contains(parts[3]), parts);          // Check suffix
                }
            }
        }
    }
}
