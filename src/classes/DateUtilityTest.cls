/*
 * DateUtilityTest.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
@IsTest
public with sharing class DateUtilityTest {
    @IsTest
    public static void testGetFirstDayOfMonth() {
        Integer year = 2000;
        Integer month = 12;
        Integer day = 10;

        Test.startTest();

        // Check date in middle of the  month
        Date expected = Date.newInstance(year, month, 1);
        Date actual = DateUtility.getFirstDayOfMonth(Date.newInstance(year, month, day));

        System.assertEquals(expected, actual);

        // Check date at start of month
        day = 1;
        actual = DateUtility.getFirstDayOfMonth(Date.newInstance(year, month, day));

        System.assertEquals(expected, actual);

        // Check date at the end of the month
        day = 31;
        actual = DateUtility.getFirstDayOfMonth(Date.newInstance(year, month, day));

        System.assertEquals(expected, actual);

        // Check error message
        try {
            expected = null;
            actual = DateUtility.getFirstDayOfMonth(expected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Check Datetime method
        Datetime dtExpected = null;
        Datetime dtActual;
        try {
            dtActual = DateUtility.getFirstDayOfMonth(dtExpected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        Integer hour = 1;
        Integer minute = 0;
        Integer second = 0;

        dtExpected = Datetime.newInstance(year, month, 1, hour, minute, second);
        dtActual = DateUtility.getFirstDayOfMonth(Datetime.newInstance(year, month, day, hour, minute, second));

        System.assertEquals(dtExpected, dtActual);

        Test.stopTest();
    }

    @IsTest
    public static void testGetLastDayOfMonth() {
        Integer year = 2018;
        Integer month = 1;
        Integer day = 12;

        Test.startTest();

        Date expected = Date.newInstance(year, month, 1).addMonths(1).addDays(-1);
        Date actual = DateUtility.getLastDayOfMonth(Date.newInstance(year, month, day));

        System.assertEquals(expected, actual);

        // Check error message
        try {
            expected = null;
            actual = DateUtility.getLastDayOfMonth(expected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Check Datetime method
        Datetime dtExpected = null;
        Datetime dtActual;
        try {
            dtActual = DateUtility.getLastDayOfMonth(dtExpected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        Date dateValue = Date.newInstance(year, month, day);
        Integer hour = 1;
        Integer minute = 0;
        Integer second = 0;

        dtExpected = Datetime.newInstance(year, month, dateValue.toStartOfMonth().addMonths(1).addDays(-1).day(), hour, minute, second);
        dtActual = DateUtility.getLastDayOfMonth(Datetime.newInstance(year, month, day, hour, minute, second));

        System.assertEquals(dtExpected, dtActual);

        Test.stopTest();
    }

    @IsTest
    public static void testGetLastDayOfNextMonth() {
        Integer year = 2018;
        Integer month = 1;
        Integer day = 12;

        Test.startTest();

        Date expected = Date.newInstance(year, month, 1).addMonths(2).addDays(-1);
        Date actual = DateUtility.getLastDayOfNextMonth(Date.newInstance(year, month, day));

        System.assertEquals(expected, actual);

        // Test error message
        try {
            expected = null;
            actual = DateUtility.getLastDayOfNextMonth(expected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Check Datetime method
        Datetime dtExpected = null;
        Datetime dtActual;
        try {
            dtActual = DateUtility.getLastDayOfNextMonth(dtExpected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        Date dateValue = Date.newInstance(year, month, day);
        Integer hour = 1;
        Integer minute = 0;
        Integer second = 0;

        dtExpected = Datetime.newInstance(year, dateValue.toStartOfMonth().addMonths(1).month(), dateValue.toStartOfMonth().addMonths(2).addDays(-1).day(), hour, minute, second);
        dtActual = DateUtility.getLastDayOfNextMonth(Datetime.newInstance(year, month, day, hour, minute, second));

        System.assertEquals(dtExpected, dtActual);

        Test.stopTest();
    }

    @IsTest
    public static void testGetFirstDayOfNextMonth() {
        Integer year = 2018;
        Integer month = 1;
        Integer day = 12;

        Test.startTest();

        Date expected = Date.newInstance(year, month, 1).addMonths(1);
        Date actual = DateUtility.getFirstDayOfNextMonth(Date.newInstance(year, month, day));

        System.assertEquals(expected, actual);

        // Test error message
        try {
            expected = null;
            actual = DateUtility.getFirstDayOfNextMonth(expected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Check Datetime method
        Datetime dtExpected = null;
        Datetime dtActual;
        try {
            dtActual = DateUtility.getFirstDayOfNextMonth(dtExpected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        Date dateValue = Date.newInstance(year, month, day);
        Integer hour = 1;
        Integer minute = 0;
        Integer second = 0;

        dtExpected = Datetime.newInstance(year, dateValue.toStartOfMonth().addMonths(1).month(), 1, hour, minute, second);
        dtActual = DateUtility.getFirstDayOfNextMonth(Datetime.newInstance(year, month, day, hour, minute, second));

        System.assertEquals(dtExpected, dtActual);

        Test.stopTest();
    }

    @IsTest
    public static void testGetFirstDayOfPreviousMonth() {
        Integer year = 2018;
        Integer month = 1;
        Integer day = 12;

        Test.startTest();

        Date expected = Date.newInstance(year, month, 1).addMonths(-1);
        Date actual = DateUtility.getFirstDayOfPreviousMonth(Date.newInstance(year, month, day));

        System.assertEquals(expected, actual);

        // Test error message
        try {
            expected = null;
            actual = DateUtility.getFirstDayOfPreviousMonth(expected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Check Datetime method
        Datetime dtExpected = null;
        Datetime dtActual;
        try {
            dtActual = DateUtility.getFirstDayOfPreviousMonth(dtExpected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        Date dateValue = Date.newInstance(year, month, day);
        Integer hour = 1;
        Integer minute = 0;
        Integer second = 0;

        dtExpected = Datetime.newInstance(year - 1, dateValue.toStartOfMonth().addMonths(-1).month(), 1, hour, minute, second);
        dtActual = DateUtility.getFirstDayOfPreviousMonth(Datetime.newInstance(year, month, day, hour, minute, second));

        System.assertEquals(dtExpected, dtActual);

        Test.stopTest();
    }

    @IsTest
    public static void testGetLastDayOfPreviousMonth() {
        Integer year = 2018;
        Integer month = 1;
        Integer day = 12;

        Test.startTest();

        Date expected = Date.newInstance(year, month, 1).addMonths(1).addDays(-1).addMonths(-1);
        Date actual = DateUtility.getLastDayOfPreviousMonth(Date.newInstance(year, month, day));

        System.assertEquals(expected, actual);

        // Test error message
        try {
            expected = null;
            actual = DateUtility.getLastDayOfPreviousMonth(expected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Check Datetime method
        Datetime dtExpected = null;
        Datetime dtActual;
        try {
            dtActual = DateUtility.getLastDayOfPreviousMonth(dtExpected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        Date dateValue = Date.newInstance(year, month, day);
        Integer hour = 1;
        Integer minute = 0;
        Integer second = 0;

        dtExpected = Datetime.newInstance(year - 1, dateValue.toStartOfMonth().addMonths(-1).month(), dateValue.toStartOfMonth().addDays(-1).day(), hour, minute, second);
        dtActual = DateUtility.getLastDayOfPreviousMonth(Datetime.newInstance(year, month, day, hour, minute, second));

        System.assertEquals(dtExpected, dtActual);

        Test.stopTest();
    }

    @IsTest
    public static void testSubtractYears() {
        Integer year = 2018;
        Integer month = 1;
        Integer day = 12;

        Test.startTest();

        // Check positive year argument
        Date expected = Date.newInstance(year, month, day).addYears(-1);
        Date actual = DateUtility.subtractYears(Date.newInstance(year, month, day), 1);

        System.assertEquals(expected, actual);

        // Check negative year argument
        actual = DateUtility.subtractYears(Date.newInstance(year, month, day), -1);

        System.assertEquals(expected, actual);

        // Test error message
        expected = null;
        try {
            actual = DateUtility.subtractYears(expected, 1);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Check Datetime method
        Datetime dtExpected = null;
        Datetime dtActual;
        Integer yearsToSubtract = null;
        try {
            dtActual = DateUtility.subtractYears(dtExpected, yearsToSubtract);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test with good values
        Integer hour = 1;
        Integer minute = 0;
        Integer second = 0;
        yearsToSubtract = 1;
        dtExpected = Datetime.newInstance(year - yearsToSubtract, month, day, hour, minute, second);
        dtActual = DateUtility.subtractYears(Datetime.newInstance(year, month, day, hour, minute, second), yearsToSubtract);

        System.assertEquals(dtExpected, dtActual);

        Test.stopTest();
    }

    @IsTest
    public static void testSubtractMonths() {
        Integer year = 2018;
        Integer month = 1;
        Integer day = 12;

        Test.startTest();

        // Check positive month argument
        Date expected = Date.newInstance(year, month, day).addMonths(-1);
        Date actual = DateUtility.subtractMonths(Date.newInstance(year, month, day), 1);

        System.assertEquals(expected, actual);

        // Check negative month argument
        actual = DateUtility.subtractMonths(Date.newInstance(year, month, day), -1);

        System.assertEquals(expected, actual);

        // Test error message
        expected = null;
        try {
            actual = DateUtility.subtractMonths(expected, 1);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Check Datetime method
        Datetime dtExpected = null;
        Datetime dtActual;
        Integer monthsToSubtract = null;
        try {
            dtActual = DateUtility.subtractMonths(dtExpected, monthsToSubtract);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test with good values
        Integer hour = 1;
        Integer minute = 0;
        Integer second = 0;
        monthsToSubtract = 1;
        dtExpected = Datetime.newInstance(year, month - monthsToSubtract, day, hour, minute, second);
        dtActual = DateUtility.subtractMonths(Datetime.newInstance(year, month, day, hour, minute, second), monthsToSubtract);

        System.assertEquals(dtExpected, dtActual);

        Test.stopTest();
    }

    @IsTest
    public static void testSubtractDays() {
        Integer year = 2018;
        Integer month = 1;
        Integer day = 12;
        Integer hour = 9;
        Integer minute = 0;
        Integer second = 0;
        Integer daysToSubtract = 5;

        Test.startTest();

        // Check positive year argument
        Date expected = Date.newInstance(year, month, day).addDays(-1 * daysToSubtract);
        Date actual = DateUtility.subtractDays(Date.newInstance(year, month, day), daysToSubtract);

        System.assertEquals(expected, actual);

        // Check negative year argument
        actual = DateUtility.subtractDays(Date.newInstance(year, month, day), (-1 * daysToSubtract));

        System.assertEquals(expected, actual);

        // Test error message
        try {
            expected = null;
            actual = DateUtility.subtractDays(expected, null);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test Datetime method
        daysToSubtract = null;
        Datetime dtExpected = null;
        Datetime dtActual;
        try {
            dtActual = DateUtility.subtractDays(dtExpected, daysToSubtract);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test with good values
        daysToSubtract = 5;
        dtExpected = Datetime.newInstance(year, month, day - daysToSubtract, hour, minute, second);
        dtActual = DateUtility.subtractDays(Datetime.newInstance(year, month, day, hour, minute, second), daysToSubtract);

        System.assertEquals(dtExpected, dtActual);

        Test.stopTest();
    }

    @IsTest
    public static void testGetFirstInstanceOfDayInMonth() {
        Test.startTest();

        Map<Date, String> testCases = new Map<Date, String> {
            Date.newInstance(2018, 1, 1) => 'Monday',
            Date.newInstance(2018, 1, 2) => 'Tuesday',
            Date.newInstance(2018, 1, 3) => 'Wednesday',
            Date.newInstance(2018, 1, 4) => 'Thursday',
            Date.newInstance(2018, 1, 5) => 'Friday',
            Date.newInstance(2018, 1, 6) => 'Saturday',
            Date.newInstance(2018, 1, 7) => 'Sunday'
        };

        Date actual;
        Date expected;
        for (Date test : testCases.keySet()) {
            actual = DateUtility.getFirstInstanceOfDayInMonth(test, testCases.get(test));
            expected = test;

            System.assertEquals(expected, actual);
        }

        testCases = new Map<Date, String> {
            Date.newInstance(2000, 1, 1) => 'Saturday',
            Date.newInstance(2000, 1, 2) => 'Sunday',
            Date.newInstance(2000, 1, 3) => 'Monday',
            Date.newInstance(2000, 1, 4) => 'Tuesday',
            Date.newInstance(2000, 1, 5) => 'Wednesday',
            Date.newInstance(2000, 1, 6) => 'Thursday',
            Date.newInstance(2000, 1, 7) => 'Friday'
        };

        for (Date test : testCases.keySet()) {
            actual = DateUtility.getFirstInstanceOfDayInMonth(test, testCases.get(test));
            expected = test;

            System.assertEquals(expected, actual);
        }

        // Test error message
        expected = null;
        String dayName = null;
        try {
            actual = DateUtility.getFirstInstanceOfDayInMonth(expected, dayName);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test Datetime method
        Datetime dtExpected = null;
        Datetime dtActual;
        try {
            dtActual = DateUtility.getFirstInstanceOfDayInMonth(dtExpected, dayName);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test with good values
        Map<Datetime, String> testCasesNew = new Map<Datetime, String> {
            Datetime.newInstance(2018, 1, 1, 1, 0, 0) => 'Monday',
            Datetime.newInstance(2018, 1, 2, 1, 0, 0) => 'Tuesday',
            Datetime.newInstance(2018, 1, 3, 1, 0, 0) => 'Wednesday',
            Datetime.newInstance(2018, 1, 4, 1, 0, 0) => 'Thursday',
            Datetime.newInstance(2018, 1, 5, 1, 0, 0) => 'Friday',
            Datetime.newInstance(2018, 1, 6, 1, 0, 0) => 'Saturday',
            Datetime.newInstance(2018, 1, 7, 1, 0, 0) => 'Sunday'
        };

        for (Datetime test : testCasesNew.keySet()) {
            dtExpected = test;
            dtActual = DateUtility.getFirstInstanceOfDayInMonth(test, testCasesNew.get(test));

            System.assertEquals(dtExpected, dtActual);
        }

        Test.stopTest();
    }

    @IsTest
    public static void testGetDaysOfMonthByDayOfWeek() {
        Integer month = 1;
        Integer year = 2018;

        Map<Date, String> testCases = new Map<Date, String> {
            Date.newInstance(2018, 1, 1) => 'Monday',
            Date.newInstance(2018, 1, 2) => 'Tuesday',
            Date.newInstance(2018, 1, 3) => 'Wednesday',
            Date.newInstance(2018, 1, 4) => 'Thursday',
            Date.newInstance(2018, 1, 5) => 'Friday',
            Date.newInstance(2018, 1, 6) => 'Saturday',
            Date.newInstance(2018, 1, 7) => 'Sunday'
        };

        Test.startTest();

        Map<String, Date[]> actual = DateUtility.getDaysOfMonthByDayOfWeek(month, year);
        Date[] dayDates;
        String dayName;
        for (Date d : testCases.keySet()) {
            dayName = testCases.get(d);

            System.assertEquals(d, actual.get(dayName)[0]);
        }

        // Test error message
        Date expected = null;
        try {
            actual = DateUtility.getDaysOfMonthByDayOfWeek(expected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test Integer method error message
        month = -1;
        year = 20000;
        try {
            actual = DateUtility.getDaysOfMonthByDayOfWeek(month, year);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        Test.stopTest();
    }

    @IsTest
    public static void testGetDayOfWeekName() {
        Map<Date, String> testCases = new Map<Date, String> {
            Date.newInstance(2018, 1, 1) => 'Monday',
            Date.newInstance(2018, 1, 2) => 'Tuesday',
            Date.newInstance(2018, 1, 3) => 'Wednesday',
            Date.newInstance(2018, 1, 4) => 'Thursday',
            Date.newInstance(2018, 1, 5) => 'Friday',
            Date.newInstance(2018, 1, 6) => 'Saturday',
            Date.newInstance(2018, 1, 7) => 'Sunday'
        };

        Test.startTest();

        String actual;
        for (Date test : testCases.keySet()) {
            actual = DateUtility.getDayOfWeekName(Datetime.newInstance(test, Time.newInstance(1, 0, 0, 0)));

            System.assertEquals(testCases.get(test), actual);
        }

        // Test error message
        Datetime dtBadValue = null;
        try {
            actual = DateUtility.getDayOfWeekName(dtBadValue);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test Date method - bad values
        Date dBadValue = null;
        try {
            actual = DateUtility.getDayOfWeekName(dBadValue);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test Date method - good values
        actual = null;
        for (Date test : testCases.keySet()) {
            actual = DateUtility.getDayOfWeekName(test);

            System.assertEquals(testCases.get(test), actual);
        }

        Test.stopTest();
    }

    @IsTest
    public static void testGetDayOfWeekNumber() {
        Map<Date, String> testCases = new Map<Date, String> {
            Date.newInstance(2018, 1, 1) => '1',
            Date.newInstance(2018, 1, 2) => '2',
            Date.newInstance(2018, 1, 3) => '3',
            Date.newInstance(2018, 1, 4) => '4',
            Date.newInstance(2018, 1, 5) => '5',
            Date.newInstance(2018, 1, 6) => '6',
            Date.newInstance(2018, 1, 7) => '7'
        };

        Test.startTest();

        String actual;
        for (Date test : testCases.keySet()) {
            actual = DateUtility.getDayOfWeekNumber(Datetime.newInstance(test, Time.newInstance(1, 0, 0, 0)));

            System.assertEquals(testCases.get(test), actual);
        }

        // Test error message
        Datetime dtBadValue = null;
        try {
            actual = DateUtility.getDayOfWeekNumber(dtBadValue);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test Date method - error message
        Date dBadValue = null;
        try {
            actual = DateUtility.getDayOfWeekNumber(dBadValue);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test Date method - good values

        testCases = new Map<Date, String> {
            Date.newInstance(2018, 1, 1) => '1',
            Date.newInstance(2018, 1, 2) => '2',
            Date.newInstance(2018, 1, 3) => '3',
            Date.newInstance(2018, 1, 4) => '4',
            Date.newInstance(2018, 1, 5) => '5',
            Date.newInstance(2018, 1, 6) => '6',
            Date.newInstance(2018, 1, 7) => '7'
        };

        actual = null;
        for (Date test : testCases.keySet()) {
            actual = DateUtility.getDayOfWeekNumber(test);

            System.assertEquals(testCases.get(test), actual);
        }

        Test.stopTest();
    }

    @IsTest
    public static void testIsWeekday() {
        Test.startTest();

        Map<Datetime, Boolean> expectedValues = new Map<Datetime, Boolean> {
            Datetime.newInstance(2018, 1, 1, 0, 0, 0) => true,     // Monday
            Datetime.newInstance(2018, 1, 2, 0, 0, 0) => true,     // Tuesday
            Datetime.newInstance(2018, 1, 3, 0, 0, 0) => true,     // Wednesday
            Datetime.newInstance(2018, 1, 4, 0, 0, 0) => true,     // Thursday
            Datetime.newInstance(2018, 1, 5, 0, 0, 0) => true,     // Friday
            Datetime.newInstance(2018, 1, 6, 0, 0, 0) => false,    // Saturday
            Datetime.newInstance(2018, 1, 7, 0, 0, 0) => false     // Sunday
        };

        Boolean actual;
        Boolean expected;
        for (Datetime dt : expectedValues.keySet()) {
            actual = DateUtility.isWeekday(dt);
            expected = expectedValues.get(dt);

            System.assertEquals(expected, actual);
        }

        // Test Date method - Error message
        Date dBadvalue = null;
        try {
            actual = DateUtility.isWeekday(dBadvalue);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test Date method - good values
        for (Datetime dt : expectedValues.keySet()) {
            actual = DateUtility.isWeekday(dt.date());
            expected = expectedValues.get(dt);

            System.assertEquals(expected, actual);
        }

        Test.stopTest();
    }

    @IsTest
    public static void testIsWeekend() {
        Test.startTest();

        Map<Datetime, Boolean> expectedValues = new Map<Datetime, Boolean> {
            Datetime.newInstance(2018, 1, 1, 0, 0, 0) => false,   // Monday
            Datetime.newInstance(2018, 1, 2, 0, 0, 0) => false,   // Tuesday
            Datetime.newInstance(2018, 1, 3, 0, 0, 0) => false,   // Wednesday
            Datetime.newInstance(2018, 1, 4, 0, 0, 0) => false,   // Thursday
            Datetime.newInstance(2018, 1, 5, 0, 0, 0) => false,   // Friday
            Datetime.newInstance(2018, 1, 6, 0, 0, 0) => true,    // Saturday
            Datetime.newInstance(2018, 1, 7, 0, 0, 0) => true     // Sunday
        };

        Boolean actual;
        Boolean expected;
        for (Datetime dt : expectedValues.keySet()) {
            actual = DateUtility.isWeekend(dt);
            expected = expectedValues.get(dt);

            System.assertEquals(expected, actual);
        }

        // Test Date method - error message
        Date dBadValue = null;
        try {
            actual = DateUtility.isWeekend(dBadValue);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test Date method - good values
        for (Datetime dt : expectedValues.keySet()) {
            actual = DateUtility.isWeekend(dt.date());
            expected = expectedValues.get(dt);

            System.assertEquals(expected, actual);
        }

        Test.stopTest();
    }

    @IsTest
    public static void testIsMonday() {
        Test.startTest();

        Date mon = Date.newInstance(2018, 1, 1);    // Monday
        Date tue = Date.newInstance(2018, 1, 2);    // Tuesday

        System.assertEquals(true, DateUtility.isMonday(mon));
        System.assertEquals(false, DateUtility.isMonday(tue));

        Test.stopTest();
    }

    @IsTest
    public static void testIsTuesday() {
        Test.startTest();

        Date tue = Date.newInstance(2018, 1, 2);    // Tuesday
        Date wed = Date.newInstance(2018, 1, 3);    // Wednesday

        System.assertEquals(true, DateUtility.isTuesday(tue));
        System.assertEquals(false, DateUtility.isTuesday(wed));

        Test.stopTest();
    }

    @IsTest
    public static void testIsWednesday() {
        Test.startTest();

        Date wed = Date.newInstance(2018, 1, 3);     // Wednesday
        Date thur = Date.newInstance(2018, 1, 4);    // Thursday

        System.assertEquals(true, DateUtility.isWednesday(wed));
        System.assertEquals(false, DateUtility.isWednesday(thur));

        Test.stopTest();
    }

    @IsTest
    public static void testIsThursday() {
        Test.startTest();

        Date thur = Date.newInstance(2018, 1, 4);    // Thursday
        Date fri = Date.newInstance(2018, 1, 5);     // Friday

        System.assertEquals(true, DateUtility.isThursday(thur));
        System.assertEquals(false, DateUtility.isThursday(fri));

        Test.stopTest();
    }

    @IsTest
    public static void testIsFriday() {
        Test.startTest();

        Date fri = Date.newInstance(2018, 1, 5);     // Friday
        Date sat = Date.newInstance(2018, 1, 6);     // Saturday

        System.assertEquals(true, DateUtility.isFriday(fri));
        System.assertEquals(false, DateUtility.isFriday(sat));

        Test.stopTest();
    }

    @IsTest
    public static void testIsSaturday() {
        Test.startTest();

        Date sat = Date.newInstance(2018, 1, 6);     // Saturday
        Date sun = Date.newInstance(2018, 1, 7);     // Sunday

        System.assertEquals(true, DateUtility.isSaturday(sat));
        System.assertEquals(false, DateUtility.isSaturday(sun));

        Test.stopTest();
    }

    @IsTest
    public static void testIsSunday() {
        Test.startTest();

        Date sun = Date.newInstance(2018, 1, 7);     // Sunday
        Date mon = Date.newInstance(2018, 1, 1);     // Monday

        System.assertEquals(true, DateUtility.isSunday(sun));
        System.assertEquals(false, DateUtility.isSunday(mon));

        Test.stopTest();
    }

    @IsTest
    public static void testGetMonthDetails() {
        Integer year = 2018;
        Integer month = 1;
        Integer day = 12;
        Integer hour = 0;
        Integer minute = 0;
        Integer second = 0;

        Test.startTest();

        // Test error messages
        Date dExpected = null;
        Datetime dtExpected = null;
        DateUtility.MonthDetails expDetails;
        DateUtility.MonthDetails actDetails;

        // Test error message - Month/Year method
        try {
            expDetails = DateUtility.getMonthDetails(null, null);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        expDetails = DateUtility.getMonthDetails(month, year);

        // Test error message - Date method
        try {
            actDetails = DateUtility.getMonthDetails(dExpected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test error message - Datetime method
        try {
            actDetails = DateUtility.getMonthDetails(dtExpected);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test date method
        dExpected = Date.newInstance(year, month, day);
        actDetails = DateUtility.getMonthDetails(dExpected);
        compareMonthDetails(expDetails, actDetails);

        // Test Datetime method
        dtExpected = Datetime.newInstance(expDetails.firstOfMonth.date(), expDetails.firstOfMonth.time());
        actDetails = DateUtility.getMonthDetails(dtExpected);
        compareMonthDetails(expDetails, actDetails);

        Test.stopTest();
    }

    @IsTest
    public static void testMonthDetails() {
        Test.startTest();

        Integer year = 2018;
        Integer month = 1;
        Integer day = 1;

        // FiscalYearStartMonth => Expected Fiscal Quarter for 2018-01-01 (year-month-day)
        Map<Integer, Integer> testCases = new Map<Integer, Integer> {
                        //     Q1     |     Q2     |     Q3     |     Q4
                        // -----------|------------|------------|------------
             1 => 1,    // 1, 2, 3    | 4, 5, 6    | 7, 8, 9    | 10, 11, 12
             2 => 4,    // 2, 3, 4    | 5, 6, 7    | 8, 9, 10   | 11, 12, 1
             3 => 4,    // 3, 4, 5    | 6, 7, 8    | 9, 10, 11  | 12, 1, 2
             4 => 4,    // 4, 5, 6    | 7, 8, 9    | 10, 11, 12 | 1, 2, 3
             5 => 3,    // 5, 6, 7    | 8, 9, 10   | 11, 12, 1  | 2, 3, 4
             6 => 3,    // 6, 7, 8    | 9, 10, 11  | 12, 1, 2   | 3, 4, 5
             7 => 3,    // 7, 8, 9    | 10, 11, 12 | 1, 2, 3    | 4, 5, 6
             8 => 2,    // 8, 9, 10   | 11, 12, 1  | 2, 3, 4    | 5, 6, 7
             9 => 2,    // 9, 10, 11  | 12, 1, 2   | 3, 4, 5    | 6, 7, 8
            10 => 2,    // 10, 11, 12 | 1, 2, 3    | 4, 5, 6    | 7, 8, 9
            11 => 1,    // 11, 12, 1  | 2, 3, 4    | 5, 6, 7    | 8, 9, 10
            12 => 1     // 12, 1, 2   | 3, 4, 5    | 6, 7, 8    | 9, 10, 11
        };

        DateUtility.MonthDetails details = new DateUtility.MonthDetails(Date.newInstance(year, month, day));

        Integer expQuarter;
        Integer actQuarter;
        for (Integer testCase : testCases.keySet()) {
            details.setFiscalQuarterStartMonth(testCase);
            expQuarter = testCases.get(testCase);
            actQuarter = details.getFiscalQuarter();

            System.assertEquals(expQuarter, actQuarter);
        }

        Datetime dt = Datetime.newInstance(Date.newInstance(year, month, day), Time.newInstance(0, 0, 0, 0));
        details = new DateUtility.MonthDetails(Date.newInstance(year, month, day));
        DateUtility.MonthDetails newDetails = new DateUtility.MonthDetails(dt);

        System.assert(newDetails != null);

        compareMonthDetails(details, newDetails);

        Test.stopTest();
    }

    //
    // Helper methods
    //

    public static void compareMonthDetails(DateUtility.MonthDetails details, DateUtility.MonthDetails newDetails) {
        System.assertEquals(details.firstOfMonth, newDetails.firstOfMonth);
        System.assertEquals(details.lastOfMonth, newDetails.lastOfMonth);

        System.assertEquals(details.mondays, newDetails.mondays);
        System.assertEquals(details.tuesdays, newDetails.tuesdays);
        System.assertEquals(details.wednesdays, newDetails.wednesdays);
        System.assertEquals(details.thursdays, newDetails.thursdays);
        System.assertEquals(details.fridays, newDetails.fridays);
        System.assertEquals(details.saturdays, newDetails.saturdays);
        System.assertEquals(details.sundays, newDetails.sundays);
        System.assertEquals(details.weekdays, newDetails.weekdays);
        System.assertEquals(details.weekends, newDetails.weekends);

        System.assertEquals(details.fiscalQuarter, newDetails.fiscalQuarter);
        System.assertEquals(details.fiscalYearStartMonth, newDetails.fiscalYearStartMonth);
    }
}
