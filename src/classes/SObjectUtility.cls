/*
 * SObjectUtility.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
public with sharing class SObjectUtility {
    public static final Map<String, SObjectType> globalDescribe {
        get {
            if (globalDescribe == null) {
                globalDescribe = Schema.getGlobalDescribe();
            }

            return globalDescribe;
        }

        private set;
    }

    /**
     * Generates a collection of field tokens for the provided SObject type.
     *
     * @param objType SObjectType The SObject type to use when extracting the field tokens.
     * @param enforceCRUDFLS Boolean Determines if the method should access the fields in system context or if it
     * should enforce CRUD and FLS.
     *
     * @return List<SObjectField> A collection of field tokens contained on the provided SObject type.
     */
    public static SObjectField[] getFieldTokensForObject(SObjectType objType, Boolean enforceCRUDFLS) {
        if (! isValidObjectType(objType) || enforceCRUDFLS == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        Map<String, SObjectField> fields = objType.getDescribe().fields.getMap();

        if (! enforceCRUDFLS) {
            return fields.values();
        }

        SObjectField[] tokens = new SObjectField[] {};
        if (! objType.getDescribe().isAccessible()) {
            return tokens;
        }


        for (SObjectField f : fields.values()) {
            if (f.getDescribe().isAccessible()) {
                tokens.add(f);
            }
        }

        return tokens;
    }

    /**
     * Generates a collection of field API names for the provided SObject type.
     *
     * @param objType SObjectType The SObject type to use when extracting the field tokens.
     * @param enforceCRUDFLS Boolean Determines if the method should access the fields in system context or if it
     * should enforce CRUD and FLS.
     *
     * @return List<String> A collection of field API names contained on the provided SObject type.
     */
    public static String[] getFieldAPINamesForObject(SObjectType objType, Boolean enforceCRUDFLS) {
        if (! isValidObjectType(objType) || enforceCRUDFLS == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        String[] fieldNames = new String[] {};

        if (enforceCRUDFLS) {
            if (! objType.getDescribe().isAccessible()) {
                return fieldNames;
            }
        }

        DescribeFieldResult dfr = null;
        for (SObjectField f : getFieldTokensForObject(objType, enforceCRUDFLS)) {
            dfr = f.getDescribe();
            if (enforceCRUDFLS && ! dfr.isAccessible()) {
                continue;    // When enforcing CRUD/FLS, skips fields user does not have permission to access.
            }

            fieldNames.add(dfr.getName());
        }

        return fieldNames;
    }

    /**
     * Generates a collection of Namespace strings installed in the Org.
     *
     * @return List<String> A collection of strings representing all the Namespaces installed in the Org.
     */
    public static String[] getAllOrgNamespaces() {
        String[] prefixes = new String[] {};

        for (PackageLicense license : [SELECT Id, NamespacePrefix, Status, CreatedDate, ExpirationDate
                                         FROM PackageLicense]) {
            prefixes.add(license.NamespacePrefix);
        }

        Organization org = [SELECT Id, NamespacePrefix FROM Organization LIMIT 1];

        if (org.NamespacePrefix != null) {
            prefixes.add(org.NamespacePrefix);
        }

        return prefixes;
    }

    /**
     * Generates a set of field tokens for the provided Field Set.
     *
     * @param type SObjectType The SObject type containing the provided FieldSet.
     * @param fs FieldSet The Field Set to use when extracting the field tokens.
     *
     * @return Set<SObjectField> A set of field tokens for the fields in the provided FieldSet.
     */
    public static Set<SObjectField> getFieldTokensForFieldSet(SObjectType type, FieldSet fs) {
        Set<SObjectField> tokens = new Set<SObjectField>();

        if (type == null || fs == null) {
            return tokens;
        }

        if (!isValidObjectType(type)) {
            throw new Exceptions.InvalidObjectTypeException(Exceptions.ERR_INVALID_OBJECT);
        }

        Map<String, FieldSet> fieldsets = type.getDescribe().fieldSets.getMap();
        String fsName = fs.getName();

        if (!fieldsets.containsKey(fsName)) {
            throw new Exceptions.InvalidFieldSetException(Exceptions.ERR_INVALID_FIELDSET);
        }

        Map<String, SObjectField> fields = type.getDescribe().fields.getMap();

        String path;
        for (FieldSetMember fsm : fs.getFields()) {
            path = fsm.getFieldPath();

            if (fields.containsKey(path)) {
                tokens.add(fields.get(path));
            }
        }

        return tokens;
    }

    //
    // Helper Methods
    //

    /**
     * Determines if the supplied field exists on the SObjectType.
     *
     * @param type SObjectType The SObjectType to check for the field.
     * @param token SObjectField Field token for the field to check.
     *
     * @return Boolean Returns true if the field exists on the SObjectType, false otherwise.
     */
    public static Boolean isFieldOnObject(SObjectType type, SObjectField token) {
        Boolean status = false;

        if (type == null || token == null) {
            return status;
        }

        Map<String, SObjectField> fields = type.getDescribe().fields.getMap();
        Set<SObjectField> tokens = new Set<SObjectField>(fields.values());

        return tokens.contains(token);
    }

    /**
     * Determines if the SObject Type is valid for the Org.
     *
     * @param objType SObjectType The SObject type to analyze.
     *
     * @return Boolean Returns true if the SObjectType is valid in the org, false otherwise.
     */
    public static Boolean isValidObjectType(SObjectType objType) {
        Set<SObjectType> types = new Set<SObjectType>(globalDescribe.values());

        return types.contains(objType);
    }
}
