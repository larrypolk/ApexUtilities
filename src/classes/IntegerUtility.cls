/* 
 * IntegerUtility.cls
 * 
 * Author: Larry Polk
 * 
 * Copyright 2018. See the file "LICENSE" for the full license governing this code. 
 */
public with sharing class IntegerUtility {
    public static final Integer[] DIGITS = new Integer[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    public static final Integer MIN_INT_VALUE = -2147483647; // -2147483646;
    public static final Integer MAX_INT_VALUE = 2147483647; // 2147483646;
    public static final Integer MAX_INT_LENGTH = 10;

    /**
     * Simulates the roll of a four-sided die.
     *
     * @return An integer between 1 and 4, inclusive.
     */
    public static Integer d4() {
        return random(1, 4);
    }

    /**
     * Simulates the roll of a six-sided die.
     *
     * @return An integer between 1 and 6, inclusive.
     */
    public static Integer d6() {
        return random(1, 6);
    }

    /**
     * Simulates the roll of an eight-sided die.
     *
     * @return An integer between 1 and 8, inclusive.
     */
    public static Integer d8() {
        return random(1, 8);
    }

    /**
     * Simulates the roll of a ten-sided die.
     *
     * @return An integer between 1 and 10, inclusive.
     */
    public static Integer d10() {
        return random(1, 10);
    }

    /**
     * Simulates the roll of a twelve-sided die.
     *
     * @return An integer between 1 and 12, inclusive.
     */
    public static Integer d12() {
        return random(1, 12);
    }

    /**
     * Simulates the roll of a twenty-sided die.
     *
     * @return An integer between 1 and 20, inclusive.
     */
    public static Integer d20() {
        return random(1, 20);
    }

    /**
     * Simulates the roll of a thirty-sided die.
     *
     * @return An integer between 1 and 30, inclusive.
     */
    public static Integer d30() {
        return random(1, 30);
    }

    /**
     * Simulates the roll of a one hundred-sided die.
     *
     * @return An integer between 1 and 100, inclusive.
     */
    public static Integer d100() {
        return random(1, 100);
    }

    /**
     * Selects a random Integer value. Value can range between -2,147,483,647 and 2,147,483,647.
     *
     * @return The randomly-selected Integer value.
     */
    public static Integer random() {
        return random(0, 1) == 0 ? random(true) : random(false);
    }

    /**
     * Generates a random positive integer of the specified length.
     *
     * @param len The length of the integer to generate.
     *
     * @return The randomly-selected integer value of the specified length.
     */
    public static Integer random(Integer len) {
        return random(len, false);
    }

    /**
     * Generates a random integer value.
     *
     * @param negative Determines if the value should be negative or positive.
     *
     * @return The randomly-selected integer value.
     */
    public static Integer random(Boolean negative) {
        Integer value;

        if (negative == true) {
            value = random(MIN_INT_VALUE, -1);
        } else {
            value = random(0, MAX_INT_VALUE);
        }

        return value;
    }

    /**
     * Generates a random positive or negative integer of the specified length.
     *
     * @param len The length of the integer to generate.
     * @param negative Specifies if the generated integer value should be negative.
     *
     * @return The randomly-selected positive or negative integer value of the specified length.
     */

    public static Integer random(Integer len, Boolean negative) {
        if (len == null || len < 1) {
            return null;
        }

        if (len > MAX_INT_LENGTH) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        Double[] ubParts = new Double[] {};
        Integer idx = 0;
        do {
            ubParts.add(9 * Math.pow((Double) 10.0, idx));
            idx += 1;
        } while (idx < len);

        Double low = 1.0 * Math.pow((Double) 10.0, (len - 1));
        Double high = 0.0;
        if (len == 10) {
            high = (Double) MAX_INT_VALUE;
        } else {
            for (Double d : ubParts) {
                high += d;
            }
        }

        Integer value = random(low.intValue(), high.intValue());

        return negative == true ? (-1 * value) : value;
    }

    /**
     * Selects a random integer between the two endpoint values.
     *
     * @param low The low endpoint for the possible range from which to select the random integer.
     * @param high The high endpoint for the possible range from which to select the random integer.
     *
     * @return An integer value randomly-selected between the two provided endpoint values.
     */
    public static Integer random(Integer low, Integer high) {
        if (low == null || high == null || low < MIN_INT_VALUE || high > MAX_INT_VALUE) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        // If the endpoints are out of order, swap the values.
        if (low > high) {
            Integer temp = low;
            low = high;
            high = temp;
        }

        // If the low and high are the same, there really isn't a valid range, so just return one of the values.
        if (low == high) {
            return low;
        }

        return Integer.valueOf(Math.floor(Math.random() * ((high - low) + 1) + low));
    }
}
