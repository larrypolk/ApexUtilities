/*
 * TestingUtility.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
public with sharing class TestingUtility {
    // NEVER set these values here; instead, set them in the calling Test code where you
    // want to test one or more exceptions.
    public static Boolean forceError = false;    // !!!! NEVER CHANGE THIS HERE !!!!
    public static Exception exceptionToMock = null;    // !!!! NEVER CHANGE THIS HERE !!!!

    // Message Strings
    public static String ERR_MOCK_MESSAGE = 'This is a Mock Error. It is used for testing purposes only.';

    // Constructor
    public TestingUtility() {}

    public static void mockTestError() {
        if (Test.isRunningTest() && forceError) {
            if (exceptionToMock == null) {
                throw new MockTestException(ERR_MOCK_MESSAGE);
            }

            throw exceptionToMock;
        }
    }

    public static void mockTestError(Exception e) {
        if (Test.isRunningTest() && forceError) {
            throw e;
        }
    }

    public with sharing class GenericException extends Exception {}
    public with sharing class MockTestException extends Exception {}
}
