/*
 * SObjectUtilityTest.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
@IsTest
public with sharing class SObjectUtilityTest {
    @IsTest
    public static void testGetFieldTokensForObject() {
        Test.startTest();

        Boolean enforceSecurity = false;
        SObjectType type = Account.SObjectType;
        SObjectField[] actual = SObjectUtility.getFieldTokensForObject(type, enforceSecurity);

        System.assert(!actual.isEmpty());

        SObjectField[] expected = new SObjectField[] {};
        Map<String, SObjectField> fields = type.getDescribe().fields.getMap();
        for (String fldName : SObjectUtility.getFieldAPINamesForObject(type, enforceSecurity)) {
            if (fields.containsKey(fldName)) {
                expected.add(fields.get(fldName));
            }
        }

        System.assertEquals(expected, actual);

        enforceSecurity = true;

        actual = SObjectUtility.getFieldTokensForObject(type, enforceSecurity);

        expected.clear();
        for (String fldName : SObjectUtility.getFieldAPINamesForObject(type, enforceSecurity)) {
            if (fields.containsKey(fldName)) {
                expected.add(fields.get(fldName));
            }
        }

        System.assertEquals(expected, actual);

        // Test Exception path
        try {
            actual = SObjectUtility.getFieldTokensForObject(null, null);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(e.getTypeName(), ipe.getTypeName());

            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        Test.stopTest();
    }

    @IsTest
    public static void testGetFieldAPINamesForObject() {
        Test.startTest();

        Boolean enforceSecurity = false;
        SObjectType type = Account.SObjectType;
        String[] actual = SObjectUtility.getFieldAPINamesForObject(type, enforceSecurity);

        System.assert(!actual.isEmpty());

        String[] expected = new String[]{};
        for (SObjectField token : SObjectUtility.getFieldTokensForObject(type, enforceSecurity)) {
            expected.add(token.getDescribe().getName());
        }

        System.assertEquals(expected, actual);

        enforceSecurity = true;
        actual = SObjectUtility.getFieldAPINamesForObject(type, enforceSecurity);

        expected.clear();
        for (SObjectField token : SObjectUtility.getFieldTokensForObject(type, enforceSecurity)) {
            expected.add(token.getDescribe().getName());
        }

        System.assertEquals(expected, actual);

        // Test Exception path
        try {
            actual = SObjectUtility.getFieldAPINamesForObject(null, null);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(e.getTypeName(), ipe.getTypeName());

            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        Test.stopTest();
    }

    @IsTest
    public static void testGetAllOrgNamespaces() {
        Test.startTest();

        String[] expected = new String[] {};
        String[] actual = SObjectUtility.getAllOrgNamespaces();

        System.assert(actual != null);

        if (actual.isEmpty()) {
            System.assertEquals(expected, actual);
        }

        Test.stopTest();
    }

    @IsTest
    public static void testGetFieldTokensForFieldSet() {
        Test.startTest();

        SObjectType type = Account.SObjectType;
        Map<String, FieldSet> fieldsets = Account.SObjectType.getDescribe().fieldSets.getMap();

        if (!fieldsets.isEmpty()) {
            Set<SObjectField> actual;
            for (FieldSet fs : fieldsets.values()) {
                actual = SObjectUtility.getFieldTokensForFieldSet(type, fs);
                System.assert(!actual.isEmpty());
            }
        }

        Set<SObjectField> expected = new Set<SObjectField>();
        Set<SObjectField> actual = SObjectUtility.getFieldTokensForFieldSet(type, null);

        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testIsFieldOnObject() {
        Test.startTest();

        SObjectType type = null;
        SObjectField token = null;

        // Test when SObjectType and SObjectField are both null.
        Boolean actual = SObjectUtility.isFieldOnObject(type, token);
        Boolean expected = false;

        System.assertEquals(expected, actual);

        // Test when field is not on the object.
        type = Account.SObjectType;
        token = Contact.Name;
        expected = false;
        actual = SObjectUtility.isFieldOnObject(type, token);

        System.assertEquals(expected, actual);

        // Test when object type and field are both good values.
        token = Account.Name;
        expected = true;
        actual = SObjectUtility.isFieldOnObject(type, token);

        System.assertEquals(expected, actual);

        Test.stopTest();
    }
}
