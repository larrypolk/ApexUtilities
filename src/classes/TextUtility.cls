/* 
 * TextUtility.cls
 * 
 * Author: Larry Polk
 * 
 * Copyright 2018. See the file "LICENSE" for the full license governing this code. 
 */
public with sharing class TextUtility {
    public static final Integer MIN_SENTENCE_WORDS = 8;
    public static final Integer MAX_SENTENCE_WORDS = 30;
    public static final Integer MIN_PARAGRAPH_LINES = 3;
    public static final Integer MAX_PARAGRAPH_LINES = 8;
    public static final String LINE_ENDING = '\n';

    public static final String loremStart = 'Lorem ipsum dolor sit amet';

    public static final String[] loremPunctuation = new String[] { '.', '.', '.', '.', '.', '.', '.', '?', '?', '!' };

    public static final String[] loremWords = new String[] {
        'a', 'ab', 'accusamus', 'accusantium', 'ad', 'adipisci', 'adipiscing', 'alias', 'aliqua', 'aliquam',
        'aliquid', 'aliquip', 'amet', 'anim', 'animi', 'aperiam', 'architecto', 'asperiores', 'aspernatur', 'assumenda',
        'at', 'atque', 'aut', 'aute', 'autem', 'beatae', 'blanditiis', 'cillum', 'commodi', 'commodo',
        'consectetur', 'consequat', 'consequatur', 'consequuntur', 'corporis', 'corrupti', 'culpa', 'cum', 'cumque', 'cupidatat',
        'cupiditate', 'debitis', 'delectus', 'deleniti', 'deserunt', 'dicta', 'dignissimos', 'distinctio', 'do', 'dolor',
        'dolore', 'dolorem', 'doloremque', 'dolores', 'doloribus', 'dolorum', 'ducimus', 'duis', 'ea', 'eaque',
        'earum', 'eius', 'eiusmod', 'eligendi', 'elit', 'enim', 'eos', 'error', 'esse',
        'est', 'et', 'eu', 'eum', 'eveniet', 'ex', 'excepteur', 'excepturi', 'exercitation', 'exercitationem',
        'expedita', 'explicabo', 'facere', 'facilis', 'fuga', 'fugiat', 'fugit', 'harum', 'hic', 'id',
        'illo', 'illum', 'impedit', 'in', 'incididunt', 'incidunt', 'inventore', 'ipsa', 'ipsam', 'ipsum',
        'irure', 'iste', 'itaque', 'iure', 'iusto', 'labore', 'laboriosam', 'laboris', 'laborum', 'laudantium',
        'libero', 'lorem', 'magna', 'magnam', 'magni', 'maiores', 'maxime', 'minim', 'minima', 'minus',
        'modi', 'molestiae', 'molestias', 'mollit', 'mollitia', 'nam', 'natus', 'necessitatibus', 'nemo', 'neque',
        'nesciunt', 'nihil', 'nisi', 'nobis', 'non', 'nostrud', 'nostrum', 'nulla', 'numquam', 'occaecat',
        'occaecati', 'odio', 'odit', 'officia', 'officiis', 'omnis', 'optio', 'pariatur', 'perferendis', 'perspiciatis',
        'placeat', 'porro', 'possimus', 'praesentium', 'proident', 'provident', 'quae', 'quaerat', 'quam', 'quas',
        'quasi', 'qui', 'quia', 'quibusdam', 'quidem', 'quis', 'quisquam', 'quo', 'quod', 'quos',
        'ratione', 'recusandae', 'reiciendis', 'rem', 'repellat', 'repellendus', 'reprehenderit', 'repudiandae', 'rerum', 'saepe',
        'sapiente', 'sed', 'sequi', 'similique', 'sint', 'sit', 'soluta', 'sunt', 'suscipit', 'tempor',
        'tempora', 'tempore', 'temporibus', 'tenetur', 'totam', 'ullam', 'ullamco', 'unde', 'ut', 'vel',
        'velit', 'veniam', 'veritatis', 'vero', 'vitae', 'voluptas', 'voluptate', 'voluptatem', 'voluptates', 'voluptatibus',
        'voluptatum'
    };

    /**
     * Generates a block of Lorem Ipsum text composed of the specified number of paragraphs.
     *
     * @param numParagraphs The number of paragraphs to generate in the block of Lorem Ipsum text.
     * @param startsWith Specifies if the block should begin with the phrase 'Lorem ipsum dolor sit amet'.
     *
     * @return A block of randomly-generated Lorem Ipsum text.
     */
    public static String lorem(Integer numParagraphs, Boolean startsWith) {
        String[] paragraphs = new String[] {};

        for (Integer i = 0; i < numParagraphs; i++) {
            paragraphs.add(makeParagraph(startsWith == true && i == 0 ? true : false));
        }

        return String.join(paragraphs, LINE_ENDING.repeat(2));
    }

    //
    // Private methods
    //

    @TestVisible
    private static String makeParagraph(Boolean startsWith) {
        String[] lines = new String[] {};

        for (Integer i = 0; i < IntegerUtility.random(MIN_PARAGRAPH_LINES, MAX_PARAGRAPH_LINES); i++) {
            lines.add(makeSentence(startsWith));
        }

        return String.join(lines, ' ');
    }

    @TestVisible
    private static String makeSentence(Boolean startWith) {
        Integer numWords = IntegerUtility.random(MIN_SENTENCE_WORDS, MAX_SENTENCE_WORDS);
        String[] words = new String[] {};

        if (startWith == true) {
            words.addAll(loremStart.split(' '));
        }

        Integer counter = 0;
        do {
            words.add((String) ListUtility.random(loremWords));
            counter += 1;
        } while (words.size() < numWords);

        words[0] = words[0].capitalize();
        words[words.size() - 1] = words[words.size() - 1] + (String) ListUtility.random(loremPunctuation);

        return String.join(words, ' ');
    }
}
