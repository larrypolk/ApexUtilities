/*
 * ListUtility.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
public with sharing class ListUtility {
    /**
     * Calculates the average of the values in the provided List. The List must be of type Decimal,
     * Double, Integer, or Long.
     *
     * @param collection A List of Decimal, Double, Integer, or Long values.
     *
     * @return The average of the values in the provided list.
     */
    public static Object average(List<Object> collection) {
        Object value = 0;

        if (collection == null || collection.isEmpty()) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        if (collection instanceof List<Decimal>) {
            Decimal sum = 0.0;

            for (Object o : collection) {
                sum += (Decimal) o;
            }

            value = sum / collection.size();
        } else if (collection instanceof List<Double>) {
            Double sum = 0.0;

            for (Object o : collection) {
                sum += (Double) o;
            }

            value = sum / collection.size();
        } else if (collection instanceof List<Integer>) {
            Integer sum = 0;

            for (Object o : collection) {
                sum += (Integer) o;
            }

            value = sum / collection.size();
        } else if (collection instanceof List<Long>) {
            Long sum = 0L;

            for (Object o : collection) {
                sum += (Long) o;
            }

            value = sum / collection.size();
        } else {
            value = null;
        }

        return value;
    }

    /**
     * Creates a List of elements split into groups the length of `size`. If `items` can't be split evenly, the final
     * chunk will be the remaining elements.
     *
     * @param items The list to process.
     * @param size The length of each chunk.
     *
     * @return List<List> New list of lists containing the chunked elements.
     */
    public static List<List<Object>> chunk(List<Object> items, Integer size) {
        List<List<Object>> chunks = new List<List<Object>>();

        if (items == null || items.isEmpty() || size == null || size <= 0) {
            return null;
        }

        if (size >= items.size()) {
            chunks.add(new List<Object>());
            chunks[0].addAll(items);
            return chunks;
        }

        Integer chunkCount = Integer.valueOf(Math.ceil(items.size() / (1.0 * size)));
        Integer idxPointer = 0;
        for (Integer i = 0; i < chunkCount; i++) {
            chunks.add(new List<String>());

            for (Integer j = idxPointer; j < idxPointer + size; j++) {
                if (j >= items.size()) break;

                chunks[i].add(items[j]);
            }

            idxPointer += size;
        }

        return chunks;
    }

    /**
     * Creates a List with all falsey values removed. The values false, null, 0, 0.0, '' are falsey.
     *
     * @param items The List to compact.
     *
     * @return List<Object> Returns the new List of filtered values.
     */
    public static List<Object> compact(List<Object> items) {
        List<Object> truthyObjects = new List<Object>();

        for (Object item : items) {
            if (ListUtility.isTruthy(item)) {
                truthyObjects.add(item);
            }
        }

        return truthyObjects;
    }

    /**
     * Combines all the elements of a List of Lists into a single list.
     *
     * @return List<Object> Returns a list of objects made up of all the entries in the List of Lists.
     */
    public static List<Object> concat(List<List<Object>> lols) {
        List<Object> newList = new List<Object>();

        if (lols == null) {
            newList = null;
        } else {
            if (!lols.isEmpty()) {
                for (List<Object> loo : lols) {
                    newList.addAll(loo);
                }
            }
        }

        return newList;
    }

    /**
     * Creates a List of values not included in the other given List. The order and references of result values
     * are determined by the first List.
     *
     * @param primary The list to inspect.
     * @param secondary The values to exclude.
     *
     * @return List<Object> Returns the new List of filtered values.
     */
    public static List<Object> difference(List<Object> primary, List<Object> secondary) {
        List<Object> items = new List<Object>();

        if (primary == null || primary.isEmpty()) {
            return items;
        }

        if (secondary == null || secondary.isEmpty()) {
            return primary;
        }

        Set<Object> secondaryUnique = new Set<Object>(secondary);
        for (Object o : primary) {
            if (! secondaryUnique.contains(o)) {
                items.add(o);
            }
        }

        return items;
    }

    //
    // Miscellaneous Methods
    //

    /**
     * Extracts all the values contained in the external ID fields from the objects in the collection.
     *
     * @param objects List<SObject> The collection of SObjects used to extract all the external ID values.
     *
     * @return List<String> A list of strings representing any External ID found on the objects passed into the method.
     */
    public static String[] collectExternalIds(SObject[] objects) {
        String[] values = new String[] {};
        Map<SObjectType, Set<SObjectField>> externalIdFieldsByObject = new Map<SObjectType, Set<SObjectField>>();

        if (objects == null || objects.isEmpty()) {
            return values;
        }

        SObjectType objType = null;
        Set<SObjectField> fields = new Set<SObjectField>();
        for (SObject so : objects) {
            objType = so.Id.getSobjectType();

            if (externalIdFieldsByObject.containsKey(objType)) {
                for (SObjectField f : externalIdFieldsByObject.get(objType)) {
                    values.add((String) so.get(f));
                }
                externalIdFieldsByObject.get(objType).addAll(fields);
            } else {
                externalIdFieldsByObject.put(objType, new Set<SObjectField>());

                DescribeFieldResult dfr = null;
                for (SObjectField fld : objType.getDescribe().fields.getMap().values()) {
                    dfr = fld.getDescribe();
                    if (dfr.isExternalId()) {
                        fields.add(dfr.getSobjectField());    // External ID field can be a String, Number, or Email, INCLUDING AutoNumber.
                        values.add((String) so.get(dfr.getSobjectField()));
                    }
                }
            }
        }

        return values;
    }

    /**
     * Extracts all the values in the specified external ID field from the collection of SObjects.
     *
     * @param objects List<SObject> The list of SObjects to use when extracting the external ID values.
     * @param token SObjectField The field token identifying the external ID field on the SObject.
     *
     * @return List<String> A list of strings representing any External ID found on the objects passed into the method.
     */
    public static String[] collectExternalIds(SObject[] objects, SObjectField token) {
        String[] values = new String[] {};

        if (objects == null || objects.isEmpty() || token == null || !token.getDescribe().isExternalId()) {
            return values;
        }

        if (!SObjectUtility.isFieldOnObject(objects.getSObjectType(), token)) {
            throw new Exceptions.InvalidFieldException(Exceptions.ERR_INVALID_FIELD);
        }

        for (SObject so : objects) {
            values.add((String) so.get(token));
        }

        return values;
    }

    /**
     * Converts the supplied List into a Map keyed on the integer representing the index of the value in the List.
     *
     * @param objects The List of objects to convert into the Map.
     *
     * @return A Map of values from the List, keyed on their index values in the List.
     */
    public static Map<Integer, Object> entries(Object[] objects) {
        Map<Integer, Object> entries = new Map<Integer, Object>();

        if (objects == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        for (Integer i = 0; i < objects.size(); i++) {
            entries.put(i, objects[i]);
        }

        return entries;
    }

    /**
     * Selects items from the list whose specified field contains the value.
     *
     * @param objects List<SObject> The list of SObjects to filter.
     * @param token SObjectField The field to analyze when filtering the objects.
     * @param value Object The value to check for in the specified SObject field.
     *
     * @return List<SObject> List of SObjects which contain the specified value in the field.
     */
    public static SObject[] filter(SObject[] objects, SObjectField token, Object value) {
        SObject[] matches = new SObject[] {};

        if (objects == null || objects.isEmpty() || token == null) {
            return matches;
        }

        if (!SObjectUtility.isFieldOnObject(objects.getSObjectType(), token)) {
            throw new Exceptions.InvalidFieldException(Exceptions.ERR_INVALID_FIELD);
        }

        for (SObject obj : objects) {
            if (obj.get(token) == value) {
                matches.add(obj);
            }
        }

        return matches;
    }

    /**
     * Selects items from the list whose field values match all the specified filter criteria.
     *
     * @param objects List<SObject> The list of SObjects to filter.
     * @param fieldsAndValues Map<SObjectField, Object> A Map keyed on SObjectField which map to the values to filter
     * the objects against.
     *
     * @return List<SObject> A list of SObjects who contain the fields and values supplied in the argument to the method.
     */
    public static SObject[] filter(SObject[] objects, Map<SObjectField, Object> fieldsAndValues) {
        SObject[] matches = new SObject[] {};

        if (objects == null || objects.isEmpty() || fieldsAndValues == null || fieldsAndValues.isEmpty()) {
            return matches;
        }

        SObjectType type = objects.getSObjectType();
        for (SObjectField fld : fieldsAndValues.keySet()) {
            if (fld == null || !SObjectUtility.isFieldOnObject(type, fld)) {
                return matches;
            }
        }

        Set<SObject> selections = new Set<SObject>();
        for (SObject obj : objects) {
            selections.add(obj);
            for (SObjectField fld : fieldsAndValues.keySet()) {
                if (obj.get(fld) != fieldsAndValues.get(fld)) {
                    selections.remove(obj);
                    break;
                }
            }
        }

        if (!selections.isEmpty()) {
            matches.addAll(selections);
        }

        return matches;
    }

    /**
     * Returns a list of all indexes of the target item in the List or an empty List if the target item isn't found.
     *
     * @param objects The List of objects to search for the target item.
     * @param target The item to search for in the List.
     *
     * @return A List of Integers representing the locations of the target object in the List or an empty List if the
     * target item isn't found.
     */
    public static Integer[] allIndexes(Object[] objects, Object target) {
        Integer[] indexes = new Integer[] {};

        if (objects == null || target == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        for (Integer i = 0; i < objects.size(); i++) {
            if (target == objects[i]) {
                indexes.add(i);
            }
        }

        return indexes;
    }

    /**
     * Returns the first index of the target item in the List or -1 if the target item isn't found.
     *
     * @param objects The List of objects to search for the target item.
     * @param target The item to search for in the List.
     *
     * @return The index value for the first instance of the target item in the List or -1 if the item isn't found.
     */
    public static Integer firstIndex(Object[] objects, Object target) {
        if (objects == null || target == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        return objects.indexOf(target);
    }

    /**
     * Returns the last index of the target item in the List or -1 if the target item isn't found.
     *
     * @param objects The List of objects to search for the target item.
     * @param target The item to search for in the List.
     *
     * @return The index value for the last instance of the target item in the List or -1 if the item isn't found.
     */
    public static Integer lastIndex(Object[] objects, Object target) {
        Integer value = -1;

        if (objects == null || target == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        for (Integer i = objects.size() - 1; i >= 0; i--) {
            if (target == objects[i]) {
                value = i;
                break;
            }
        }

        return value;
    }

    /**
     * Selects the largest item in the supplied List.
     *
     * @param objects The list of objects to analyze.
     *
     * @return The object representing the largest item in the List.
     */
    public static Object max(Object[] objects) {
        if (objects == null || objects.isEmpty()) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        objects.sort();

        return objects[objects.size() - 1];
    }

    /**
     * Selects the smallest item in the supplied List.
     *
     * @param objects The list of objects to analyze.
     *
     * @return The object representing the smallest item in the list.
     */
    public static Object min(Object[] objects) {
        if (objects == null || objects.isEmpty()) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        objects.sort();

        return objects[0];
    }

    /**
     * Selects a random item from the provided List.
     *
     * @param objects List<Object> The list of Objects from which to randomly select a value.
     *
     * @return Object The randomly-selected object.
     */
    public static Object random(Object[] objects) {
        Object result = null;

        if (objects == null || objects.isEmpty()) {
            result = null;
        } else {
            Integer index = IntegerUtility.random(0, objects.size() - 1);
            result = objects[index];
        }

        return result;
    }

    /**
     * Randomly selects the specified number of values from the provided List.
     *
     * @param objects List<Object> The list to use when selecting the random values.
     * @param num Integer The number of random values to select from the list.
     *
     * @return List<Object> A new list containing the randomly selected values.
     */
    public static Object[] random(Object[] objects, Integer num) {
        if (objects == null || num == null || num < 0) {
            return null;
        }

        if (objects.isEmpty() || num == 0) {
            return new Object[]{};
        }

        Object[] result = new Object[]{};
        if (num > objects.size()) {
            result.addAll(objects);
        } else {
            Integer index;
            Set<Object> selections = new Set<Object>();
            do {
                index = IntegerUtility.random(0, objects.size() - 1);

                if (! selections.contains(objects[index])) {
                    selections.add(objects[index]);
                }
            } while (selections.size() < num);

            result.addAll(selections);
        }

        return result;
    }

    /**
     * Selects the unique items in the provided List. The resulting List maintains the order of the original List.
     *
     * @param objects The List of items to filter for uniqueness.
     *
     * @return A new List containing the unique items from the supplied List. The order of the items in the returned
     * matches the order from the supplied List.
     */
    public static Object[] unique(Object[] objects) {
        Object[] result = new Object[] {};

        if (objects == null) {
            throw new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
        }

        if (objects.isEmpty()) {
            return result;
        }

        for (Object o : objects) {
            if (!result.contains(o)) {
                result.add(o);
            }
        }

        return result;
    }

    /**
     * Combines the two Lists by grouping corresponding elements from each List.
     *
     * @param listA The primary List in the combination.
     * @param listB The secondary List in the combination.
     *
     * @return A List of Lists containing the elements from the supplied lists, grouped by position.
     */
    public static List<List<Object>> zip(Object[] listA, Object[] listB) {
        List<List<Object>> lol = new List<List<Object>>();

        Integer lenA = listA == null ? 0 : listA.size();
        Integer lenB = listB == null ? 0 : listB.size();
        Integer size = lenA > lenB ? lenA : lenB;
        Boolean bothNull = listA == null && listB == null;
        Boolean isListABad = listA == null || listA.isEmpty();
        Boolean isListBBad = listB == null || listB.isEmpty();

        if (bothNull || (isListABad && isListBBad)) {
            return lol;
        }

        for (Integer i = 0; i < size; i++) {
            lol.add(new Object[] { i >= listA.size() ? null : listA[i], i >= listB.size() ? null : listB[i] });
        }

        return lol;
    }

    //
    // Helper Methods
    //

    /**
     * Determines if a value is "truthy" or "falsey".
     *
     * @param value Object The value used when determining truthy or falsey.
     *
     * @return Boolean Returns true if the value is truthy, false otherwise.
     */
    public static Boolean isTruthy(Object value) {
        Boolean result = true;

        // If the value is null, it's not "truthy".
        if (value == null) {
            result = false;
        }

        // If the value is a Boolean, and it's false, it's not "truthy"
        if (value instanceof Boolean) {
            if (((Boolean) value) == false) {
                result = false;
            }
        }

        // If the value is an Integer, and it's less than or equal to zero, it's not "truthy".
        if (value instanceof Integer) {
            if (((Integer) value) == 0) {
                result = false;
            }
        }

        if (value instanceof Double) {
            if (((Double) value) == 0.0) {
                result = false;
            }
        }

        if (value instanceof String) {
            if (String.isBlank((String) value)) {
                result = false;
            }
        }

        return result;
    }
}
