/*
 * ListUtilityTest.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
@IsTest
public with sharing class ListUtilityTest {
    @TestSetup
    public static void setup() {
        Account[] accts = queryAccounts();

        System.assert(accts.isEmpty());

        accts = new Account[] {
            new Account(Name = 'Test Account 1'),
            new Account(Name = 'Test Account 2'),
            new Account(Name = 'Test Account 3')
        };
        insert accts;

        accts.clear();
        accts = queryAccounts();

        System.assert(!accts.isEmpty());
    }

    @IsTest
    public static void testAverage() {
        Test.startTest();

        // Test collection of Decimals
        Decimal[] decimals = new Decimal[] { 1.0, 2.0, 3.0 };
        Object expected = 2.0;
        Object actual = ListUtility.average(decimals);

        System.assertEquals(expected, actual);

        // Test collection of Doubles
        Double[] doubles = new Double[] { 400.0, 500.0, 600.0 };
        expected = 500.0;
        actual = ListUtility.average(doubles);

        System.assertEquals(expected, actual);

        // Test collection of Integers
        Integer[] integers = new Integer[] { 1, 2, 3 };
        expected = 2;
        actual = ListUtility.average(integers);

        System.assertEquals(expected, actual);

        // Test collection of Longs
        Long[] longs = new Long[] { 1000L, 2000L, 3000L };
        expected = 2000L;
        actual = ListUtility.average(longs);

        System.assertEquals(expected, actual);

        // Test collection of unsupported type
        String[] strings = new String[] { '1', '2', '3' };
        expected = null;
        actual = ListUtility.average(strings);

        System.assertEquals(expected, actual);

        // Test null collection
        integers = new Integer[]{};
        try {
            actual = ListUtility.average(integers);
        } catch (Exception e) {
            String message = Exceptions.ERR_INVALID_PARAMETER;
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(message);

            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(message));
        }

        Test.stopTest();
    }

    @IsTest
    public static void testChunk() {
        Test.startTest();

        String[] values = null;
        Integer size = null;
        List<List<String>> expected = null;

        List<List<String>> actual = (List<List<String>>) ListUtility.chunk(values, size);
        System.assertEquals(expected, actual);

        values = new List<String>();
        size = 0;
        expected = null;
        System.assertEquals(expected, actual);

        values = new String[] { 'a', 'b', 'c', 'd' };

        // Size is smaller than the length of the items array.
        size = 2;
        expected = new List<List<String>> { new String[] { 'a', 'b' }, new String[] { 'c', 'd' } };
        actual = new List<List<String>>();
        for (List<Object> elem : ListUtility.chunk(values, size)) {
            actual.add((List<String>) elem);
        }
        System.assertEquals(expected, actual);

        // Size is smaller than the length of the items array.
        size = 3;
        expected = new List<List<String>> { new String[] { 'a', 'b', 'c' }, new String[] { 'd' } };
        actual = new List<List<String>>();
        for (List<Object> elem : ListUtility.chunk(values, size)) {
            actual.add((List<String>) elem);
        }
        System.assertEquals(expected, actual);

        // Size is equal to the length of the items array.
        size = 4;
        expected = new List<List<String>> { new String[] { 'a', 'b', 'c', 'd' } };
        actual = new List<List<String>>();
        for (List<Object> elem : ListUtility.chunk(values, size)) {
            List<String> items = new List<String>();
            for (Object o : elem) {
                items.add((String) o);
            }
            actual.add(items);
        }
        System.assertEquals(expected, actual);

        // Size is equal to the length of the items array.
        size = 5;
        expected = new List<List<String>> { new String[] { 'a', 'b', 'c', 'd' } };
        actual = new List<List<String>>();
        for (List<Object> elem : ListUtility.chunk(values, size)) {
            List<String> items = new List<String>();
            for (Object o : elem) {
                items.add((String) o);
            }
            actual.add(items);
        }
        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testCompact() {
        Test.startTest();

        List<Object> values = new List<Object> { 0, 1, false, 2, '', 3, null, true, 0.0, 1.0, -1, -1.0 };

        List<Object> actual = ListUtility.compact(values);
        List<Object> expected = new List<Object> { 1, 2, 3, true, 1.0, -1, -1.0 };

        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testConcat() {
        List<List<Object>> lols = new List<Object[]>{
            new Object[] { 'a', 'b', 'c' },
            new Object[] { 'x', 'y', 'z' },
            new Object[] { 1, 2 },
            new Object[] { '1', '2', '3', '4' }
        };

        Test.startTest();

        List<Object> expected = new List<Object> { 'a', 'b', 'c', 'x', 'y', 'z', 1, 2, '1', '2', '3', '4' };
        List<Object> actual = ListUtility.concat(lols);

        System.assertEquals(expected, actual);

        // Test when List of List is null
        expected = null;
        actual = ListUtility.concat(null);

        System.assertEquals(expected, actual);

        // Test when List of List is empty
        expected = new List<Object>();
        actual = ListUtility.concat(new List<List<Object>>{});

        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testDifference() {
        Test.startTest();

        // First List is null
        List<Object> first = null;
        List<Object> second = null;

        List<Object> expected = new List<Object>();
        List<Object> actual = ListUtility.difference(first, second);

        System.assertEquals(actual, expected);

        // First list is empty
        first = new List<Object>();
        second = null;

        expected = new List<Object>();
        actual = ListUtility.difference(first, second);

        System.assertEquals(actual, expected);

        // Second List is null
        first = new List<Object> { 2, 1 };
        second = null;

        expected = new List<Object> { 2, 1 };
        actual = ListUtility.difference(first, second);

        System.assertEquals(actual, expected);

        // Second List is empty
        first = new List<Object> { 2, 1 };
        second = new List<Object>();

        expected = new List<Object> { 2, 1 };
        actual = ListUtility.difference(first, second);

        System.assertEquals(actual, expected);

        // First and Second Lists are populated.
        first = new List<Object> { 2, 1 };
        second = new List<Object> { 2, 3 };

        expected = new List<Object> { 1 };
        actual = ListUtility.difference(first, second);
        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testCollectExternalIds() {
        Test.startTest();

        SObject[] objects = null;

        // Test when object list is null;
        String[] expected = new String[] {};
        String[] actual = ListUtility.collectExternalIds(objects);

        System.assertEquals(expected, actual);

        // Test when object list is empty.
        objects = new SObject[] {};
        expected = new String[] {};
        actual = ListUtility.collectExternalIds(objects);

        System.assertEquals(expected, actual);

        // Test positive path.
        objects = queryAccounts();
        actual = ListUtility.collectExternalIds(objects);

        System.assert(actual != null);

        // Test - secondary method
        SObjectField fld = Account.Name;
        expected = new String[]{};
        actual = ListUtility.collectExternalIds(objects, fld);

        System.assertEquals(expected, actual);

        // Test - secondary method with field not on object
        fld = Contact.Name;

        try {
            actual = ListUtility.collectExternalIds(objects, fld);
        } catch (Exception e) {
            Exceptions.InvalidFieldException ife = new Exceptions.InvalidFieldException(Exceptions.ERR_INVALID_FIELD);
            System.assertEquals(ife.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_FIELD));
        }

        Test.stopTest();
    }

    @IsTest
    public static void testEntries() {
        Test.startTest();

        Object[] objects = new Object[] { 'a', 'b', 'c' };

        // Make sure we have objects to test.
        System.assert(!objects.isEmpty());

        // Test positive path
        Map<Integer, Object> actual = ListUtility.entries(objects);

        System.assertEquals(objects.size(), actual.size());

        for (Integer i : actual.keySet()) {
            System.assertEquals(objects[i], actual.get(i));
        }

        // Check with SObject Types
        objects = queryAccounts();
        actual = ListUtility.entries(objects);

        System.assert(!objects.isEmpty());

        for (Integer i : actual.keySet()) {
            System.assertEquals(objects[i], (Account) actual.get(i));
        }

        // Test when List is null
        try {
            actual = ListUtility.entries(null);
        } catch (Exception e) {
            String message = Exceptions.ERR_INVALID_PARAMETER;
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(message);

            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(ipe.getMessage().contains(message));
        }

        // Test when List is empty
        actual = ListUtility.entries(new Object[] {});
        System.assert(actual.isEmpty());

        Test.stopTest();
    }

    @IsTest
    public static void testFilter() {
        Test.startTest();

        Account[] objects = queryAccounts();

        System.assert(!objects.isEmpty());

        // Test where collection is null/empty and field is null
        SObjectField fld = null;
        String val = '';

        SObject[] expected = new SObject[] {};
        SObject[] actual = ListUtility.filter(objects, fld, val);

        System.assertEquals(expected, actual);

        // Test with good values
        fld = Account.Name;
        val = objects[0].Name;

        expected = new SObject[] { objects[0] };
        actual = ListUtility.filter(objects, fld, val);

        System.assertEquals(expected, actual);

        // Test when field doesn't exist on object.
        fld = Contact.Name;
        try {
            actual = ListUtility.filter(objects, fld, val);
        } catch (Exception e) {
            Exceptions.InvalidFieldException ife = new Exceptions.InvalidFieldException(Exceptions.ERR_INVALID_FIELD);
            System.assertEquals(ife.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_FIELD));
        }

        // Test alternate method with good values.

        Map<SObjectField, Object> filters = new Map<SObjectField, Object> {
            Account.Id => objects[0].Id,
            Account.Name => objects[0].Name
        };

        actual = ListUtility.filter(objects, filters);
        expected = new SObject[] { objects[0] };

        System.assertEquals(actual, expected);

        // Test alternate method with bad values.
        filters = null;
        actual = ListUtility.filter(objects, filters);
        expected = new SObject[]{};

        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testAllIndexes() {
        Object[] objects = new Object[] { 'a', 'b', 'c', 'a', 'b', 'c', 'd' };
        Map<Object, Integer[]> tests = new Map<Object, Integer[]> {
            'a' => new Integer[] { 0, 3 },
            'b' => new Integer[] { 1, 4 },
            'c' => new Integer[] { 2, 5 },
            'd' => new Integer[] { 6 },
            'e' => new Integer[] {}
        };

        Test.startTest();

        // Test Positive path
        for (Object obj : tests.keySet()) {
            System.assertEquals(tests.get(obj), ListUtility.allIndexes(objects, obj));
        }

        objects = queryAccounts();

        System.assert(!objects.isEmpty());
        System.assertEquals(3, objects.size());

        Account newAccount = new Account(Name = 'Test Account - Not In List');

        Map<Object, Integer[]> accts = new Map<Object, Integer[]> {
            objects[0] => new Integer[] { 0 },
            objects[1] => new Integer[] { 1 },
            objects[2] => new Integer[] { 2 },
            newAccount => new Integer[] {}
        };

        for (Object acct : accts.keySet()) {
            System.assertEquals(accts.get(acct), ListUtility.allIndexes(objects, acct));
        }

        Integer[] actual;

        // Test when List is null
        try {
            actual = ListUtility.allIndexes(null, 'a');
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test when target item is null
        try {
            actual = ListUtility.allIndexes(objects, null);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test when List is empty
        Integer[] expected = new Integer[] {};
        actual = ListUtility.allIndexes(new Object[] {}, 'a');
        System.assertEquals(expected, actual);

        // Test when List is a single value
        expected = new Integer[] { 0 };
        actual = ListUtility.allIndexes(new Object[] { 'a' }, 'a');    // Test when value exists in List.
        System.assertEquals(expected, actual);

        actual = ListUtility.allIndexes(new Object[] { 'a' }, 'b');    // Test when value does not exist in List.

        Test.stopTest();
    }

    @IsTest
    public static void testFirstIndex() {
        Object[] objects = new Object[] { 'a', 'b', 'c' };
        Map<Object, Integer> tests = new Map<Object, Integer> {
            'a' => 0,
            'b' => 1,
            'c' => 2,
            'd' => -1
        };

        Test.startTest();

        // Test positive path
        for (Object obj : tests.keySet()) {
            System.assertEquals(tests.get(obj), ListUtility.firstIndex(objects, obj));
        }

        objects = queryAccounts();

        System.assert(!objects.isEmpty());

        for (Integer i = 0; i < objects.size(); i++) {
            System.assertEquals(i, ListUtility.firstIndex(objects, objects[i]));
        }

        Account newAccount = new Account(Name = 'Test Account - Not In List');

        Integer expected = -1;
        Integer actual = ListUtility.firstIndex(objects, newAccount);
        System.assertEquals(expected, actual);

        // Test when List is null
        try {
            actual = ListUtility.firstIndex(null, 'a');
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test when target item is null
        try {
            actual = ListUtility.firstIndex(objects, null);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test when List is empty
        actual = ListUtility.firstIndex(new Object[] {}, 'a');
        System.assertEquals(expected, actual);

        // Test when List is a single value
        expected = 0;
        actual = ListUtility.firstIndex(new Object[] { 'a' }, 'a');    // Test when value exists in List.
        System.assertEquals(expected, actual);

        actual = ListUtility.firstIndex(new Object[] { 'a' }, 'b');    // Test when value does not exist in List.

        Test.stopTest();
    }

    @IsTest
    public static void testLastIndex() {
        Object[] objects = new Object[] { 'a', 'b', 'c', 'd', 'd' };
        Map<Object, Integer> tests = new Map<Object, Integer> {
            'a' => 0,
            'b' => 1,
            'c' => 2,
            'd' => 4,
            'e' => -1
        };

        Test.startTest();

        // Test positive path
        for (Object obj : tests.keySet()) {
            System.assertEquals(tests.get(obj), ListUtility.lastIndex(objects, obj));
        }

        objects = queryAccounts();

        for (Integer i = 0; i < objects.size(); i++) {
            System.assertEquals(i, ListUtility.lastIndex(objects, objects[i]));
        }

        Account newAccount = new Account(Name = 'Test Account - Not In List');

        Integer expected = -1;
        Integer actual = ListUtility.lastIndex(objects, newAccount);
        System.assertEquals(expected, actual);

        // Test when List is null
        try {
            actual = ListUtility.lastIndex(null, 'a');
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test when target item is null
        try {
            actual = ListUtility.lastIndex(objects, null);
        } catch (Exception e) {
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(Exceptions.ERR_INVALID_PARAMETER);
            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(Exceptions.ERR_INVALID_PARAMETER));
        }

        // Test when List is empty
        expected = -1;
        actual = ListUtility.lastIndex(new Object[] {}, 'a');
        System.assertEquals(expected, actual);

        // Test when List is a single value
        expected = 0;
        actual = ListUtility.lastIndex(new Object[] { 'a' }, 'a');
        System.assertEquals(expected, actual);

        expected = -1;
        actual = ListUtility.lastIndex(new Object[] { 'a' }, 'b');

        Test.stopTest();
    }

    @IsTest
    public static void testMax() {
        Test.startTest();

        // Test with Integer collection
        Integer[] integers = new Integer[] { 5, 4, 3, 2, 1 };
        Object expected = 5;
        Object actual = ListUtility.max(integers);

        System.assertEquals((Integer) expected, (Integer) actual);

        // Test with String collection
        String[] strings = new String[] { 'bc', 'bb', 'ab', 'aa' };
        expected = 'bc';
        actual = ListUtility.max(strings);

        System.assertEquals((String) expected, (String) actual);

        // Test with SObject collection
        Account[] accounts = queryAccounts();

        System.assert(!accounts.isEmpty());

        accounts.sort();
        expected = accounts[accounts.size() - 1];
        actual = ListUtility.max(accounts);

        System.assertEquals((Account) expected, (Account) actual);

        // Test with Object collection
        Object[] objects = new Object[] { 3.0, 2.0, 1.0 };
        expected = 3.0;
        actual = ListUtility.max(objects);

        System.assertEquals(expected, actual);

        // Test with null collection
        integers = null;
        try {
            actual = ListUtility.max(integers);
        } catch (Exception e) {
            String message = Exceptions.ERR_INVALID_PARAMETER;
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(message);

            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(message));
        }

        // Test with empty collection
        integers = new Integer[] {};
        try {
            actual = ListUtility.max(integers);
        } catch (Exception e) {
            String message = Exceptions.ERR_INVALID_PARAMETER;
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(message);

            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(message));
        }

        Test.stopTest();
    }

    @IsTest
    public static void testMin() {
        Test.startTest();

        // Test with Integer collection
        Integer[] integers = new Integer[] { 5, 4, 3, 2, 1 };
        Object expected = 1;
        Object actual = ListUtility.min(integers);

        System.assertEquals((Integer) expected, (Integer) actual);

        // Test with String collection
        String[] strings = new String[] { 'bc', 'bb', 'ab', 'aa' };
        expected = 'aa';
        actual = ListUtility.min(strings);

        System.assertEquals((String) expected, (String) actual);

        // Test with SObject collection
        Account[] accounts = queryAccounts();

        System.assert(!accounts.isEmpty());

        accounts.sort();
        expected = accounts[0];
        actual = ListUtility.min(accounts);

        System.assertEquals((Account) expected, (Account) actual);

        // Test with Object collection
        Object[] objects = new Object[] { 3.0, 2.0, 1.0 };
        expected = 1.0;
        actual = ListUtility.min(objects);

        System.assertEquals(expected, actual);

        // Test with null collection
        integers = null;
        try {
            actual = ListUtility.min(integers);
        } catch (Exception e) {
            String message = Exceptions.ERR_INVALID_PARAMETER;
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(message);

            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(message));
        }

        // Test with empty collection
        integers = new Integer[] {};
        try {
            actual = ListUtility.min(integers);
        } catch (Exception e) {
            String message = Exceptions.ERR_INVALID_PARAMETER;
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(message);

            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(message));
        }

        Test.stopTest();
    }

    @IsTest
    public static void testRandom() {
        Test.startTest();

        // Test when Object list is null
        Object[] objects = null;

        Object expected = null;
        Object actual = ListUtility.random(objects);

        System.assertEquals(expected, actual);

        // Test when Object list is empty
        objects = new Object[] {};
        expected = null;
        actual = ListUtility.random(objects);

        System.assertEquals(expected, actual);

        // Test when Object list is not empty
        objects = new Object[] { 'a', 'b', 'c' };
        actual = ListUtility.random(objects);

        System.assert((new Set<Object>(objects)).contains(actual));

        // Test with SObjects
        objects = queryAccounts();
        actual = ListUtility.random(objects);

        System.assert((new Set<Object>(objects)).contains(actual));

        Test.stopTest();
    }

    @IsTest
    public static void testRandomAlternate() {
        Test.startTest();

        Integer count = null;

        // Test when Object list is null and count is null
        Object[] objects = null;

        Object expected = null;
        Object[] actual = ListUtility.random(objects, count);

        System.assertEquals(expected, actual);

        // Test when object list is null and count is negative
        count = -1;
        expected = null;
        actual = ListUtility.random(objects, count);

        System.assertEquals(expected, actual);

        // Test when Object list is empty
        objects = new Object[] {};
        expected = null;
        actual = ListUtility.random(objects, count);

        System.assertEquals(expected, actual);

        // Test when Object list is not empty and count is zero
        count = 0;
        objects = new Object[] { 'a', 'b', 'c' };
        expected = new Object[]{};
        actual = ListUtility.random(objects, count);

        System.assertEquals(expected, actual);

        // Test when Object list is not empty and count is positive
        count = 2;
        actual = ListUtility.random(objects, count);

        System.assert((new Set<Object>(objects)).containsAll(actual));

        // Test with SObjects
        objects = queryAccounts();
        actual = ListUtility.random(objects, count);

        System.assert((new Set<Object>(objects)).containsAll(actual));

        // Test when count is larger than the list size.
        expected = new List<Object>(objects);
        actual = ListUtility.random(objects, objects.size() + 1);

        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    @IsTest
    public static void testUnique() {
        Test.startTest();

        // Test with collection of Integers
        Integer[] integers = new Integer[] { 1, 1, 1, 2, 3, 3 };
        Object[] expected = new Object[] { 1, 2, 3 };
        Object[] actual = ListUtility.unique(integers);

        System.assertEquals(expected, actual);

        // Test with empty collection
        integers = new Integer[] {};
        expected = new Integer[] {};
        actual = ListUtility.unique(integers);

        System.assertEquals(expected, actual);

        // Test with null collection
        integers = null;
        try {
            actual = ListUtility.unique(integers);
        } catch (Exception e) {
            String message = Exceptions.ERR_INVALID_PARAMETER;
            Exceptions.InvalidParameterException ipe = new Exceptions.InvalidParameterException(message);

            System.assertEquals(ipe.getTypeName(), e.getTypeName());
            System.assert(e.getMessage().contains(message));
        }

        Test.stopTest();
    }

    @IsTest
    public static void testZip() {
        Test.startTest();

        // Test with valid input.
        String[] strings = new String[] { 'a', 'b' };
        Integer[] integers = new Integer[] { 100, 200 };
        List<List<Object>> expected = new List<List<Object>> {
            new List<Object> { 'a', 100 },
            new List<Object> { 'b', 200 }
        };
        List<List<Object>> actual = ListUtility.zip(strings, integers);

        System.assertEquals(expected, actual);

        // Test with valid initial list, empty second list
        strings = new String[] { 'a', 'b' };
        integers = new Integer[] {};
        expected = new List<List<Object>> {
            new List<Object> { 'a', null },
            new List<Object> { 'b', null }
        };
        actual = ListUtility.zip(strings, integers);

        System.assertEquals(expected, actual);

        // Test with empty initial list, valid second list
        strings = new String[] {};
        integers = new Integer[] { 100, 200 };
        expected = new List<List<Object>> {
            new List<Object> { null, 100 },
            new List<Object> { null, 200 }
        };
        actual = ListUtility.zip(strings, integers);

        System.assertEquals(expected, actual);

        // Test with null input
        strings = null;
        integers = null;
        expected = new List<List<Object>>();
        actual = ListUtility.zip(strings, integers);

        System.assertEquals(expected, actual);

        // Test with empty input
        strings = new String[] {};
        integers = new Integer[] {};
        expected = new List<List<Object>>();
        actual = ListUtility.zip(strings, integers);

        System.assertEquals(expected, actual);

        Test.stopTest();
    }

    //
    // Helper methods
    //

    public static Account[] queryAccounts() {
        Account[] accounts = [
            SELECT Id, Name
              FROM Account
        ];

        return accounts;
    }
}
