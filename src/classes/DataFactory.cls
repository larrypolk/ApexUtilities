/*
 * DataFactory.cls
 *
 * Author: Larry Polk
 *
 * Copyright 2018. See the file "LICENSE" for the full license governing this code.
 */
public with sharing class DataFactory {
    public static final String FORMAT_PHONE = '({0}) {1}-{2}';
    public static final String FORMAT_SEPARATOR_IPV4 = '.';
    public static final String FORMAT_SEPARATOR_IPV6 = ':';
    public static final String FORMAT_SEPARATOR_MAC = ':';
    public static final String GUID_CHARACTERS = 'abcdef1234567890';
    public static final String GUID_VARIANT_CHARACTERS = 'ab89';
    public static final String GUID_FORMAT = '{0}-{1}-{2}{3}-{4}{5}-{6}';

    public static final Map<Integer, String> FORMAT_FULL_NAME = new Map<Integer, String>{
        1 => '{0} {1}',               // FirstName LastName
        2 => '{0} {1} {2}',           // Prefix FirstName LastName
        3 => '{0} {1} {2}',           // FirstName LastName Suffix
        4 => '{0} {1} {2} {3}',       // Prefix FirstName LastName Suffix
        5 => '{0} {1}-{2}',           // FirstName LastName-LastName
        6 => '{0} {1} {2}-{3}',       // Prefix FirstName LastName-LastName
        7 => '{0} {1}-{2} {3}',       // FirstName LastName-LastName Suffix
        8 => '{0} {1} {2}-{3} {4}'    // Prefix FirstName LastName-LastName Suffix
    };  // DO NOT change order; doing so will break unit tests. Add items to the end if you need additional formats.

    //
    // Methods for generating data
    //

    /**
     * Generates a random hex color string.
     *
     * @param includeHashSymbol Determines if the returned string should include the hash sign (octothorpe) at the start.
     *
     * @return A string representing a randomly-generated hexadecimal color.
     */
    public static String hexColor(Boolean includeHashSymbol) {
        String template = '{0}{1}{2}{3}';

        String[] parts = new String[]{
            includeHashSymbol == true ? '#' : '',    // Hash symbol for hex color
            StringUtility.random(2, StringUtility.HEX_DIGITS),     // Red component
            StringUtility.random(2, StringUtility.HEX_DIGITS),     // Green component
            StringUtility.random(2, StringUtility.HEX_DIGITS)      // Blue component
        };

        return String.format(template, parts);
    }

    /**
     * Creates a new fake credit card number using randomly generated numbers.
     *
     * @param includeDashes Determines if the parts of the number should be separated with dashes (0000-0000-0000-0000).
     *
     * @return A randomly-generated string of numbers representing a credit card number.
     */
    public static String creditCardNumber(Boolean includeDashes) {
        // TODO: Update this method so that it generates fake, but valid, Credit Card numbers.
        String[] parts = new String[]{
            StringUtility.random(4, true, false, false),    // First four
            StringUtility.random(4, true, false, false),    // Second four
            StringUtility.random(4, true, false, false),    // Third four
            StringUtility.random(4, true, false, false)     // Fourth four
        };

        return includeDashes == true ? String.join(parts, '-') : String.join(parts, '');
    }

    /**
     * Creates a new fake phone number using randomly generated numbers.
     *
     * @param useFakePrefix Determines if the phone number should use the fake prefix (555).
     *
     * @return A randomly-generated telephone number.
     */
    public static String phoneNumber(Boolean useFakePrefix) {
        String[] parts = new String[]{
            StringUtility.random(3, true, false, false),    // Area Code
            useFakePrefix == true ? '555' : StringUtility.random(3, true, false, false),    // Prefix
            StringUtility.random(4, true, false, false)
        };

        return String.format(FORMAT_PHONE, parts);
    }

    /**
     * Creates a new fake Social Security Number (SSN) using randomly generated numbers.
     *
     * @param includeDashes Determines if the fake SSN should include the dashes (000-000-0000) or just the numeric
     *                      values (0000000000).
     *
     * @return A string of 10 randomly-generated digits to act as a Social Security Number.
     */
    public static String socialSecurityNumber(Boolean allowInvalid, Boolean includeDashes) {
        String ssnArea = StringUtility.random(3, true, false, false);      // First three digits; can't be 666 or 900-999 for US SSN.
        String ssnGroup = StringUtility.random(3, true, false, false);     // Middle two digits; can't be 00.
        String ssnSerial = StringUtility.random(4, true, false, false);    // Last four digits; can't be 0000.

        // Make sure the values are valid.
        if (allowInvalid != true) {
            Integer ssnAreaValue = Integer.valueOf(ssnArea);
            if (ssnAreaValue == 666 || (ssnAreaValue >= 900 && ssnAreaValue <= 999)) {
                ssnArea = ssnAreaValue == 666 ? '667' : '899';
            }

            if ('00'.equalsIgnoreCase(ssnGroup)) {
                ssnGroup = '01';
            }

            if ('0000'.equalsIgnoreCase(ssnSerial)) {
                ssnSerial = '0001';
            }
        }

        String[] parts = new String[]{
            ssnArea, ssnGroup, ssnSerial
        };

        return includeDashes == true ? String.join(parts, '-') : String.join(parts, '');
    }

    /**
     * Creates a random IPv4 address.
     *
     * @return A string representing a randomly-generated IPv4 address in the 000.000.000.000 format.
     */
    public static String ip() {
        return ip(false);
    }

    /**
     * Creates a random IP address.
     *
     * @param useIPV6 A flag which determines if the generated address should be IPv4 or IPv6.
     *
     * @return A string representing a randomly-generated IPv4 or IPv6 address.
     */
    public static String ip(Boolean useIPV6) {
        // TODO: Update this method so that it generates fake, but valid, IP addresses.
        String[] parts;
        String sep;
        if (useIPV6 == true) {
            sep = FORMAT_SEPARATOR_IPV6;

            parts = new String[]{
                StringUtility.random(4, StringUtility.HEX_DIGITS),
                StringUtility.random(4, StringUtility.HEX_DIGITS),
                StringUtility.random(4, StringUtility.HEX_DIGITS),
                StringUtility.random(4, StringUtility.HEX_DIGITS),
                StringUtility.random(4, StringUtility.HEX_DIGITS),
                StringUtility.random(4, StringUtility.HEX_DIGITS),
                StringUtility.random(4, StringUtility.HEX_DIGITS),
                StringUtility.random(4, StringUtility.HEX_DIGITS)
            };
        } else {
            sep = FORMAT_SEPARATOR_IPV4;

            parts = new String[]{
                String.valueOf(IntegerUtility.random(1, 254)),
                String.valueOf(IntegerUtility.random(0, 255)),
                String.valueOf(IntegerUtility.random(0, 255)),
                String.valueOf(IntegerUtility.random(1, 254))
            };
        }

        return String.join(parts, sep);
    }

    /**
     * Creates a random MAC address.
     *
     * @return A randomly-generated string in the format of a MAC address (0000:0000:0000:0000:0000:0000).
     */
    public static String macAddress() {
        // TODO: Update this method so that it generates fake, but valid, MAC addresses.
        String[] parts = new String[]{
            StringUtility.random(4, StringUtility.HEX_DIGITS),
            StringUtility.random(4, StringUtility.HEX_DIGITS),
            StringUtility.random(4, StringUtility.HEX_DIGITS),
            StringUtility.random(4, StringUtility.HEX_DIGITS),
            StringUtility.random(4, StringUtility.HEX_DIGITS),
            StringUtility.random(4, StringUtility.HEX_DIGITS)
        };

        return String.join(parts, FORMAT_SEPARATOR_MAC);
    }

    /**
     * Randomly selects a female name from the pool of possible female names.
     *
     * @return Returns a randomly-selected female first name.
     */
    public static String firstNameFemale() {
        return firstName(false);
    }

    /**
     * Randomly selects a male name from the pool of male names.
     *
     * @return A randomly-selected male name the pool of male names.
     */
    public static String firstNameMale() {
        return firstName(true);
    }

    /**
     * Randomly selects a male or female name from the pool of possible names.
     *
     * @param maleName Determines if the generated name should be male.
     *
     * @return A randomly-selected male or female first name.
     */
    public static String firstName(Boolean maleName) {
        List<String> firstNames = new List<String>();

        if (maleName == true) {
            firstNames.addAll(Data.maleFirstNames);
        } else {
            firstNames.addAll(Data.femaleFirstNames);
        }

        firstNames.sort();    // Put the names in ascending alphabetical order.

        Integer idx = IntegerUtility.random(0, firstNames.size() - 1);

        return firstNames[idx];
    }

    /**
     * Randomly selects a surname from the pool of possible names.
     *
     * @return A randomly-selected surname.
     */
    public static String lastName() {
        List<String> lastNames = new List<String>(Data.lastNames);
        lastNames.sort();

        Integer idx = IntegerUtility.random(0, lastNames.size() - 1);

        return lastNames[idx];
    }

    /**
     * Randomly generates a full name using a randomly-selected format as well as randomly selected component data.
     *
     * @pram maleName Determines if the generated name is male or female.
     *
     * @return A randomly-generated female-specific full name.
     */
    public static String fullName(Boolean maleName) {
        maleName = maleName == true;
        Integer key = getRandomNameFormat();
        String name = '';

        if (key == 1) {
            name = getRandomNameFirstLast(maleName);
        } else if (key == 2) {
            name = getRandomNamePrefixFirstLast(maleName);
        } else if (key == 3) {
            name = getRandomNameFirstLastSuffix(maleName);
        } else if (key == 4) {
            name = getRandomPrefixFirstLastSuffix(maleName);
        } else if (key == 5) {
            name = getRandomFirstLastLast(maleName);
        } else if (key == 6) {
            name = getRandomPrefixFirstLastLast(maleName);
        } else if (key == 7) {
            name = getRandomFirstLastLastSuffix(maleName);
        } else if (key == 8) {
            name = getRandomPrefixFirstLastLastSuffix(maleName);
        }

        return name;
    }

    public static String guid() {
        String[] args = new String[] {
            StringUtility.random(8, GUID_CHARACTERS),
            StringUtility.random(4, GUID_CHARACTERS),
            '5',    // Version
            StringUtility.random(3, GUID_CHARACTERS),
            StringUtility.random(1, GUID_VARIANT_CHARACTERS),
            StringUtility.random(3, GUID_CHARACTERS),
            StringUtility.random(12, GUID_CHARACTERS)
        };

        return String.format(GUID_FORMAT, args);
    }

    public static String primaryAddressLine() {
        return null;
    }

    public static String secondaryAddressLine() {
        return null;
    }

    public static String generateFakeCityName() {
        return cityName();
    }

    public static String zipCode(String stateId, Boolean includePlusFour) {
        String format = Data.FORMAT_ZIP;
        String[] args = new String[] {
            stateId + StringUtility.random(3, StringUtility.DIGITS)
        };

        if (includePlusFour == true) {
            format = Data.FORMAT_ZIP_PLUS4;
            args.add(StringUtility.random(4, StringUtility.DIGITS));
        }

        return String.format(format, args);
    }

    //
    // Private methods
    //

    @TestVisible
    private static Integer getRandomNameFormat() {
        return IntegerUtility.random(1, FORMAT_FULL_NAME.keySet().size());
    }

    @TestVisible
    private static String getRandomNameFirstLast(Boolean maleName) {
        String format = FORMAT_FULL_NAME.get(1);

        String[] args = new String[]{
            firstName(maleName),    // FirstName
            lastName()              // LastName
        };

        return String.format(format, args);
    }

    @TestVisible
    private static String getRandomNamePrefixFirstLast(Boolean maleName) {
        String format = FORMAT_FULL_NAME.get(2);

        String[] args = new String[] {
            namePrefix(maleName),    // Prefix
            firstName(maleName),     // FirstName
            lastName()               // LastName
        };

        return String.format(format, args);
    }

    @TestVisible
    private static String getRandomNameFirstLastSuffix(Boolean maleName) {
        String format = FORMAT_FULL_NAME.get(3);

        String[] args = new String[] {
            firstName(maleName),    // FirstName
            lastName(),             // LastName
            nameSuffix(maleName)    // Suffix
        };

        return String.format(format, args);
    }

    @TestVisible
    private static String getRandomPrefixFirstLastSuffix(Boolean maleName) {
        String format = FORMAT_FULL_NAME.get(4);

        String[] args = new String[] {
            namePrefix(maleName),   // Prefix
            firstName(maleName),    // FirstName
            lastName(),             // LastName
            nameSuffix(maleName)    // Suffix
        };

        return String.format(format, args);
    }

    @TestVisible
    private static String getRandomFirstLastLast(Boolean maleName) {
        String format = FORMAT_FULL_NAME.get(5);

        String[] args = new String[] {
            firstName(maleName),    // FirstName
            lastName(),             // LastName
            lastName()              // LastName
        };

        return String.format(format, args);
    }

    @TestVisible
    private static String getRandomPrefixFirstLastLast(Boolean maleName) {
        String format = FORMAT_FULL_NAME.get(6);

        String[] args = new String[] {
            namePrefix(maleName),    // Prefix
            firstName(maleName),     // FirstName
            lastName(),              // LastName
            lastName()               // LastName
        };

        return String.format(format, args);
    }

    @TestVisible
    private static String getRandomFirstLastLastSuffix(Boolean maleName) {
        String format = FORMAT_FULL_NAME.get(7);

        String[] args = new String[] {
            firstName(maleName),    // FirstName
            lastName(),             // LastName
            lastName(),             // LastName
            nameSuffix(maleName)    // Suffix
        };

        return String.format(format, args);
    }

    @TestVisible
    private static String getRandomPrefixFirstLastLastSuffix(Boolean maleName) {
        String format = FORMAT_FULL_NAME.get(8);

        String[] args = new String[] {
            namePrefix(maleName),    // Prefix
            firstName(maleName),     // FirstName
            lastName(),              // LastName
            lastName(),              // LastName
            nameSuffix(maleName)     // Suffix
        };

        return String.format(format, args);
    }

    @TestVisible
    private static String namePrefix(Boolean maleName) {
        List<String> prefixes = new List<String>(Data.genderlessPrefixes);

        if (maleName == true) {
            prefixes.addAll(Data.malePrefixes);
        } else {
            prefixes.addAll(Data.femalePrefixes);
        }

        prefixes.sort();

        Integer idx = IntegerUtility.random(0, prefixes.size() - 1);

        return prefixes[idx];
    }

    @TestVisible
    private static String nameSuffix(Boolean maleName) {
        List<String> suffixes = new List<String>(Data.genderlessSuffixes);

        if (maleName == true) {
            suffixes.addAll(Data.maleSuffixes);
        }

        suffixes.sort();

        Integer idx = IntegerUtility.random(0, suffixes.size() - 1);

        return suffixes[idx];
    }

    @TestVisible
    private static String cityPrefix() {
        List<String> prefixes = new List<String>(Data.cityPrefixes);
        Integer idx = IntegerUtility.random(0, prefixes.size() - 1);

        prefixes.sort();

        return prefixes[idx];
    }

    @TestVisible
    private static String cityFirstName() {
        List<String> names = new List<String>(Data.firstNames);
        Integer idx = IntegerUtility.random(0, names.size() - 1);

        names.sort();

        return names[idx];
    }

    @TestVisible
    private static String citySuffix() {
        List<String> suffixes = new List<String>(Data.citySuffixes);
        Integer idx = IntegerUtility.random(0, suffixes.size() - 1);

        suffixes.sort();

        return suffixes[idx];
    }

    @TestVisible
    private static String cityName() {
        Integer formatId = IntegerUtility.random(0, Data.FORMAT_CITY.size() - 1);
        String cityName;

        if (formatId == 1) {
            cityName = getRandomCityPrefixFirstNameSuffix();
        } else if (formatId == 2) {
            cityName = getRandomCityPrefixFirstName();
        } else if (formatId == 3) {
            cityName = getRandomCityFirstNameCityPrefix();
        } else if (formatId == 4) {
            cityName = getRandomCityLastNameCitySuffix();
        }

        return cityName;
    }

    @TestVisible
    private static String getRandomCityPrefixFirstNameSuffix() {
        String format = Data.FORMAT_CITY.get(1);

        String[] args = new String[] {
            cityPrefix(),
            cityFirstName(),
            citySuffix()
        };

        return String.format(format, args);
    }

    @TestVisible
    private static String getRandomCityPrefixFirstName() {
        String format = Data.FORMAT_CITY.get(2);

        String[] args = new String[] {
            cityPrefix(),
            cityFirstName()
        };

        return String.format(format, args);
    }

    @TestVisible
    private static String getRandomCityFirstNameCityPrefix() {
        String format = Data.FORMAT_CITY.get(3);

        String[] args = new String[] {
            cityFirstName(),
            citySuffix()
        };

        return String.format(format, args);
    }

    @TestVisible
    private static String getRandomCityLastNameCitySuffix() {
        String format = Data.FORMAT_CITY.get(4);

        String[] args = new String[] {
            lastName(),
            citySuffix()
        };

        return String.format(format, args);
    }
}
